<?php
// Middleware 
// Usuarios invitados tendran accesos a estas rutas
Route::group(['middleware' => ['guest']], function () {
    
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::get('/login','Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('login');

});

// Usuarios Autenticado tendran acceso a las siguientes:
Route::group(['middleware' => ['auth']], function () {
    // Dashboard 
    Route::get('/dashboard', 'DashboardController');
    // Notficaciones
    Route::get('/notification/get', 'NotificationController@verVencidos');

    Route::get('/main', function () {
        return view('contenido.contenido');
    })->name('main');

    // Para cerrar Session
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


    // A su vez tendra otros grupos de Rutas Admnistrador
    Route::group(['middleware' => ['Administrador']], function () {
        
        
        //************Rutas de Usuarios******************/
        Route::get('/user', 'UserController@index'); // para tomar datos de la bd
        Route::post('/user/registrar', 'UserController@store'); // para registrar en la bd
        Route::put('/user/actualizar', 'UserController@update'); // para actualizar en la bd
        Route::put('/user/desactivar', 'UserController@desactivar'); // para desactivar en la bd
        Route::put('/user/activar', 'UserController@activar'); // para actualizar en la bd
        Route::get('/user/validar', 'UserController@validar'); // para tomar datos de la bd
        
        // Roles
        Route::get('/roles', 'RolController@index'); // Listar
        Route::post('/roles/registrar', 'RolController@store'); // Registrar
        Route::put('/roles/actualizar', 'RolController@update'); // Actualizar
        Route::put('/roles/eliminar', 'RolController@destroy'); // Eliminar

        // Zonas estas rutas las voy administrar desde el controlador de Roles
        Route::get('/zonas', 'RolController@indexzonas'); // Listar
        Route::post('/zonas/registrar', 'RolController@storezona'); // Registrar
        Route::put('/zonas/actualizar', 'RolController@updatezona'); // Actualizar
        Route::put('/zonas/eliminar', 'RolController@destroyzona'); // Eliminar
        
        
        // Users
        Route::get('/users', 'UserController@index'); // Listar
        Route::post('/users/registrar', 'UserController@store'); // Registrar
        Route::put('/users/actualizar', 'UserController@update'); // Actualizar
        Route::put('/users/eliminar', 'UserController@destroy'); // Eliminar
        Route::get('/users/validar', 'UserController@validar'); // Eliminar
        
        // Formacosto
        Route::get('/formasc', 'DepartamentoController@indexforma'); // Listar
        Route::post('/formasc/registrar', 'DepartamentoController@storeforma'); // Registrar
        Route::put('/formasc/actualizar', 'DepartamentoController@updateforma'); // Actualizar
        Route::put('/formasc/eliminar', 'DepartamentoController@destroyforma'); // Eliminar
        
        // Departamentos
        Route::get('/departamentos', 'DepartamentoController@index'); // Listar
        Route::post('/departamentos/registrar', 'DepartamentoController@store'); // Registrar
        Route::put('/departamentos/actualizar', 'DepartamentoController@update'); // Actualizar
        Route::put('/departamentos/eliminar', 'DepartamentoController@destroy'); // Eliminar
        
        //Servicios
        Route::get('/servicios', 'ServicioController@index'); // Listar
        Route::post('/servicios/registrar', 'ServicioController@store'); // Registrar
        Route::put('/servicios/actualizar', 'ServicioController@update'); // Actualizar
        Route::put('/servicios/eliminar', 'ServicioController@destroy'); // Eliminar
        Route::get('/servicios/listarTodo', 'ServicioController@listarServicios'); // Listar para asiganarlos a los camiones
        Route::get('servicios/paraModi', 'ServicioController@obtenerServicios'); // Listar para editarlos a los camiones
        
        //Empresas
        Route::get('/empresas/codigo', 'EmpresaController@codigo'); // Generar Codigo
        Route::get('/empresas', 'EmpresaController@index'); // Listar
        Route::post('/empresas/registrar', 'EmpresaController@create'); // Registrar
        Route::put('/empresas/actualizar', 'EmpresaController@update'); // Actualizar
        Route::put('/empresas/eliminar', 'EmpresaController@destroy'); // Eliminar
        
        //Camiones
        Route::get('/camiones/codigo', 'CamionController@codigo'); // Generar Codigo
        Route::get('/camiones', 'CamionController@index'); // Listar
        Route::post('/camiones/registrar', 'CamionController@store'); // Registrar
        Route::put('/camiones/actualizar', 'CamionController@update'); // Actualizar
        Route::put('/camiones/eliminar', 'CamionController@destroy'); // Eliminar
        Route::get('/empresa/selectem', 'EmpresaController@index'); // esta peticion se usa para la seleccion de empresa
        
        //Trabajadores
        Route::get('/personal/codigo', 'PersonalController@codigo'); // Generar Codigo
        Route::get('/personal', 'PersonalController@index'); // Listar
        Route::post('/personal/registrar', 'PersonalController@store'); // Registrar
        Route::put('/personal/actualizar', 'PersonalController@update'); // Actualizar
        Route::put('/personal/eliminar', 'PersonalController@destroy'); // Eliminar
        
        //Gestion Contratos
        Route::get('/gestionp/codcontrato', 'PersonalController@codcontrato'); // Generar Codigo
        Route::get('/gestionp/contratos', 'PersonalController@contratos'); // Listar
        Route::post('/gestionp/regcontrato', 'PersonalController@storecontrato'); // Registrar
        Route::put('/gestionp/actualizar', 'PersonalController@upcontrato'); // Actualizar
        Route::put('/gestionp/eliminar', 'PersonalController@descontrato'); // Eliminar
        Route::post('/gestionp/anexar', 'PersonalController@anexar'); // Eliminar
        
        Route::get('gestionp/selectTrabau', 'PersonalController@selectTrabaUsu'); //para cargar el select de usuarios

        Route::get('gestionp/selectTraba', 'PersonalController@selectTraba'); //para cargar el select de Trabajo con clientes
        Route::get('gestionp/vercontratos', 'PersonalController@vercontratos'); //para cargar el select de Trabajo con clientes 
        Route::get('gestionp/verfiniquito', 'PersonalController@verfiniquito'); //para ver los finiquito

        //Gestion de Finiquitos
        Route::get('/finiquitos', 'PersonalController@indexfini'); // Listar
        Route::post('/finiquitos/registrar', 'PersonalController@storefini'); // Registrar
        Route::put('/finiquitos/actualizar', 'PersonalController@updatefini'); // Actualizar
        
        // Gestion de Clientes Empresa
        Route::get('/clientempresa', 'ClienteController@index'); // Listar
        Route::post('/clientempresa/registrar', 'ClienteController@store'); // Registrar
        Route::put('/clientempresa/actualizar', 'ClienteController@update'); // Actualizar
        Route::put('/clientempresa/eliminar', 'ClienteController@destroy'); // Actualizar
        
        // Todos los clientes existentes
        Route::get('/clientempresa/todos', 'ClienteController@todosClientes'); // Listar

        // Gestion de Clientes Particulares
        Route::get('/clienteparti', 'ClienteController@indexParti'); // Listar
        Route::post('/clienteparti/registrar', 'ClienteController@storeParti'); // Registrar
        Route::put('/clienteparti/actualizar', 'ClienteController@updateParti'); // Actualizar
        Route::put('/clienteparti/eliminar', 'ClienteController@destroyParti'); // Actualizar
        
        //Gestiones de Solicitudes Empresas
        Route::get('/gestionsoli', 'SolicitudController@index'); // Listar
        Route::get('/gestionsoli/codigo', 'SolicitudController@codigo'); // Generar Codigo
        Route::get('gestionsoli/selectEmpre', 'SolicitudController@selectEm'); //para cargar el select de clientes empresa
        Route::post('/gestionsoli/registrar', 'SolicitudController@store'); // Registrar
        Route::put('/gestionsoli/actualizar', 'SolicitudController@update'); // Actualizar
        Route::put('/gestionsoli/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('gestionsoli/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        // Gestion de Solicitudes Aprobados
        Route::get('/solicitudes/apro', 'SolicitudController@aprobados'); // prueba
        
        //Gestiones de Solicitudes Particulares
        Route::get('/gestisoliparti', 'SolicitudController@indexParti'); // Listar
        Route::get('/gestisoliparti/codigo', 'SolicitudController@codigoParti'); // Generar Codigo
        Route::get('gestisoliparti/selectParti', 'SolicitudController@selectParti'); //para cargar el select de clientes empresa
        Route::post('/gestisoliparti/registrar', 'SolicitudController@storeParti'); // Registrar
        Route::put('/gestisoliparti/actualizar', 'SolicitudController@updateParti'); // Actualizar
        Route::put('/gestisoliparti/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('/gestisoliparti/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        //Presupuestos
        Route::get('/presupuesto', 'PresupuestoController@index'); // Listar
        Route::get('/presupuesto/total', 'PresupuestoController@indextotal'); // ListarTotal Cotizaciones
        Route::get('/presupuesto/codigo', 'PresupuestoController@codigo'); // Generar Codigo
        Route::post('/presupuesto/registrar', 'PresupuestoController@store'); // Registrar
        Route::get('/presupuesto/obtenerDetalles', 'PresupuestoController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::put('/presupuesto/eliminar', 'PresupuestoController@destroy'); // Eliminar Presupuesto
        Route::put('/presupuesto/aprobar', 'PresupuestoController@aprobar'); // Aprobar Presupuesto
        Route::put('/presupuesto/rechazar', 'PresupuestoController@rechazar'); // Rechazar Presupuesto
        Route::get('/cotizacion/pdf/{id}', 'PresupuestoController@mostrarPdf'); //Pdf del Presupuesto
        // 
        
        // Asignaciones
        Route::get('/asignacion/codigo', 'AsignacionController@codigo'); // Generar Codigo
        Route::get('/asignacion/total', 'AsignacionController@indextotal'); // Listar Total Asignaciones
        Route::get('/asignacion', 'AsignacionController@index'); // Listar
        Route::get('/asignacion/cordi', 'AsignacionController@indexCordi'); // Listar
        Route::get('/asignaciones/obtenerDetalles', 'AsignacionController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::post('/asignacion/registrar', 'AsignacionController@store'); // Registrar
        Route::get('/asignacion/pdf/{id}', 'AsignacionController@mostrarPdf'); //Pdf del Presupuesto
        // 
        
        // ** Rutas adicionales usadas en presupuestos**
        Route::get('/solicitudes', 'SolicitudController@solicitudesTodas'); // Listar Todas las solicitudes
        Route::get('/solicitudes/asignar', 'SolicitudController@solicitudesAsignar'); // Listar Todas las solicitudes
        Route::get('/presupuesto_apro', 'PresupuestoController@aprobados'); // Listar
        //
        
        //Coordinaciones
        Route::get('/coordinacion', 'CoordinacionesController@index'); // Listar
        Route::get('/coordinacion/codigo', 'CoordinacionesController@codigo'); // Generar Codigo
        Route::post('/coordinacion/registrar', 'CoordinacionesController@store'); // Registrar
        Route::get('/coordinacion/obtenerDetalles', 'CoordinacionesController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/personal/coordinacion', 'PersonalController@indexTraCordi'); // Listar Todos los trabajadores hausar en la coordinacion
        Route::get('/camion/validar', 'CoordinacionesController@obtenerValidar'); // Validar camiones para la coordinacion
        Route::get('/trabajador/validar', 'CoordinacionesController@validarTraba'); // Validar camiones para la coordinacion
        Route::put('/coordinacion/actualizarcamion', 'CoordinacionesController@cambiarCamion'); //Modificar camion en la coordinacion
        Route::put('/coordinacion/actualizartraba', 'CoordinacionesController@cambiarTrabajador'); //Modificar camion en la coordinacion
        
        
        //Ordenes
        Route::get('/ordenes', 'OrdenController@index'); // Listar
        Route::get('/ordenes/codigo', 'OrdenController@codigo'); // Generar Codigo
        Route::post('/ordenes/registrar', 'OrdenController@store'); // Registrar
        Route::get('/ordenes/obtenerDetalles', 'OrdenController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/orden/pdf/{id}', 'OrdenController@mostrarPdf'); //Pdf del Presupuesto
        Route::put('/orden/cobrar', 'OrdenController@cobrar'); //Pdf del Presupuesto
        
        // Coordinaciones realizadas para usar en el modulo OT
        Route::get('/coordinacion/ot', 'CoordinacionesController@indexListas'); // Listar
        Route::get('/coordinacion/ot2', 'CoordinacionesController@indexListas2'); // Listar
        //
        
        //Guia
        Route::get('/guias', 'GuiaController@index'); // Listar
        Route::get('/guias/codigo', 'GuiaController@codigo'); // Generar Codigo
        Route::post('/guias/registrar', 'GuiaController@store'); // Registrar
        Route::get('/guias/obtenerDetalles', 'GuiaController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/guia/pdf/{id}', 'GuiaController@mostrarPdf'); //Pdf de la Guia
        
        // Evento Coordinacion Calendario //
        Route::post('Evento/create','CalendarController@create');
        // Calendario Con Evento
        Route::get('Evento/index','CalendarController@indexEvento');
        Route::get('Evento/index/{month}','CalendarController@index_month_event');
        Route::get('Evento/details/{id}','CalendarController@details');
        // Route::get('coordinacion/pdf/{id}','CalendarController@pdf');
        
        // Contratos con Empresas y Particular
        Route::get('/contratos', 'ContratosController@index'); // Listar
        Route::get('/contratos/codigo', 'ContratosController@codigo'); // Generar Codigo
        Route::get('contratos/selectcli', 'ContratosController@selectCli'); //para cargar el select de clientes empresa
        Route::post('/contratos/registrar', 'ContratosController@store'); //para cargar el select de clientes empresa
        Route::post('/contratos/serviregistrar', 'ContratosController@store_servi'); //para cargar el select de clientes empresa
        Route::get('/contratos/servicioscli', 'ContratosController@verServicios'); //para cargar el select de clientes empresa
        Route::put('/contratos/actualizarprecio', 'ContratosController@cambiarPrecio'); // para Actualizar en las prendas de empresas
        Route::put('/contratos/eliminarprecio', 'ContratosController@eliminarPrecio'); // para Actualizar en las prendas de empresas
        Route::get('/contratos/validarcli', 'ContratosController@validarCli'); //para cargar el select de clientes empresa
        

        // Estados de Pagos de Las Guias de Despacho
        Route::get('/estadoguias', 'EstadoController@index'); // Listar
        Route::get('/estadoguias/codigo', 'EstadoController@codigo'); // Listar
        Route::post('/estadoguias/registrar', 'EstadoController@storegui'); // Registrar
        Route::get('/estadoguias/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{contra}/{folio}/{fechao}','EstadoController@mostrarPdf'); // para tomar datos de la bd
        Route::get('/estadoguias2/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{folio}/{fechao}','EstadoController@mostrarPdf2'); // para tomar datos de la bd
        //*********************Resumenes de Guias***********************
        Route::get('/resumen_esguia', 'EstadoController@resumen'); // para tomar datos de la bd
        
        // Estados de Pagos de Las Ordenes de Trabajo
        Route::get('/estadosot', 'EstadoController@indexot'); // Listar
        Route::get('/estadosot/codigo', 'EstadoController@codigot'); // Listar
        Route::post('/estadosot/registrar', 'EstadoController@storeot'); // Registrar
        Route::get('/estadosot/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{folio}/{fechao}','EstadoController@mostrarPdfot'); // para tomar datos de la bd
        Route::get('/estadosot2/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{contra}/{folio}/{fechao}','EstadoController@mostrarPdfot2'); // para tomar datos de la bd
        //*********************Resumenes de Guias***********************
        Route::get('/resumen_estadosot', 'EstadoController@resumenot'); // para tomar datos de la bd

        // Listar todos los contratos de una empresa
        Route::get('/contratos/cliente', 'ContratosController@verContratos'); //para cargar el select de clientes empresa
        Route::get('/contratos/clientempresa', 'ContratosController@verContratosEmpresa'); //para cargar el select de clientes empresa
        // 
        
        // Asistencias
        Route::get('/asistencias', 'AsistenciaController@index'); // Listar
        Route::post('/asistencia/registrar', 'AsistenciaController@store'); // Registrar
        Route::put('/asistencia/actualizar', 'AsistenciaController@update'); // Actualizar
        Route::put('/asistencia/eliminar', 'AsistenciaController@destroy'); // Eliminar
        Route::get('/asistencia/trabajadores', 'AsistenciaController@indexTrasi'); // Actualizar
        Route::get('/asistencia/tipos', 'AsistenciaController@indexTipos'); // Actualizar
        Route::get('/asistencia/verasis', 'AsistenciaController@validarAsis'); // Actualizar
        Route::get('/asistencia/tracontra', 'AsistenciaController@indexTracontra'); // Listado de Trabajadores

        // Calendario de Asistencias
        Route::get('Asistencia/indexAsis','CalendarController@indexAsis');
        Route::get('Asistencia/indexAsis/{month}','CalendarController@index_month_asis');
        
        // Calendario de Asistencia por Trabajador
        Route::get('Asistencia/indexAsis/personal/{id}','CalendarController@indexAsis');
        Route::get('Asistencia/indexAsis/{month}/{id}','CalendarController@index_month_asis');
        // 

        // Horas Extras
        Route::get('/horas', 'AsistenciaController@indexHoras'); // Listar
        Route::post('/hora/registrar', 'AsistenciaController@storeHoras'); // Registrar
        Route::put('/hora/actualizar', 'AsistenciaController@updateHoras'); // Actualizar
        Route::put('/hora/eliminar', 'AsistenciaController@destroyHoras'); // Eliminar
        // 

        // Turnos Extras
        Route::get('/turnos', 'AsistenciaController@indexTurnos'); // Listar
        Route::post('/turnos/registrar', 'AsistenciaController@storeTurnos'); // Registrar
        Route::put('/turnos/actualizar', 'AsistenciaController@updateTurnos'); // Actualizar
        Route::put('/turnos/eliminar', 'AsistenciaController@destroyTurnos'); // Eliminar
        //

        // Resumenes de Asistencias, Horas y Turnos
        Route::get('/turnos/fecha', 'AsistenciaController@indexTurnosFecha'); // Listar
        Route::get('/hora/fecha', 'AsistenciaController@indexHorasFecha'); // Listar
        // 

        // Cuentas
        Route::get('/cuentas/codigo', 'CuentaController@codigo'); // Listar
        Route::get('/cuentas', 'CuentaController@index'); // Listar
        Route::post('/cuentas/registrar', 'CuentaController@store'); // Registrar
        Route::get('/cuentas/creditos', 'CuentaController@verCreditos'); // Listar
        Route::put('/cuentas/actucreditos', 'CuentaController@cambiarMonto'); // para Actualizar en las prendas de empresas
        Route::put('/cuentas/actualizar', 'CuentaController@update'); // para Actualizar en las prendas de empresas

        // Pagar Guia
        Route::post('/cuentas/pagarGuia', 'CuentaController@pagoGuia'); // Registrar
        Route::post('/cuentas/pagarOt', 'CuentaController@pagoOt'); // Registrar
        // 

        // Cartolas
        Route::get('/cartolas', 'CuentaController@indexCartolas'); // Listar
        //

        // Facturas
        // EPPOT
        Route::get('/facturas', 'FacturaController@index'); // Listar
        Route::get('/facturas/codigo', 'FacturaController@codigo'); // Listar
        Route::get('/facturas/resumenot', 'FacturaController@listarEstados'); // Listar
        Route::post('/facturas/registrar', 'FacturaController@store'); // Listar
        Route::get('/facturas/eporden', 'FacturaController@detallesFactura'); // Listar
        Route::post('/facturas/pagarot', 'FacturaController@pagarFactuOt');
        Route::get('/facturas/pagosOT', 'FacturaController@pagosFactura');
        
        // EPPGUIA
        Route::get('/facturas2', 'FacturaController@index2'); // Listar
        Route::get('/facturas2/resumengui', 'FacturaController@listarEstadosGui'); // Listar
        Route::post('/facturas2/registrar', 'FacturaController@storeGuia'); // Listar
        Route::get('/facturas2/epguia', 'FacturaController@detallesFacturaGui'); // Listar
        Route::post('/facturas/pagarguia', 'FacturaController@pagarFactuGui');
        Route::get('/facturas/pagosGuia', 'FacturaController@pagosFacturaGui');
        // 

        // Cheques
        Route::get('/cheques', 'ChequeController@index'); // Listar
        Route::post('/cheques/registrar', 'ChequeController@store'); // Listar
        Route::post('/cheques/pagar', 'ChequeController@procesoFinal'); // Listar
        // 



        // Envio de Email
        Route::get('/form', 'MailController@index'); // Vista de Email
        Route::post('/send', 'MailController@contact');
        // 
    });

    // A su vez tendra otros grupos de Rutas Recursos Humanos
    Route::group(['middleware' => ['Recursos']], function () {
        // Departamentos
        Route::get('/departamentos', 'DepartamentoController@index'); // Listar
        Route::post('/departamentos/registrar', 'DepartamentoController@store'); // Registrar
        Route::put('/departamentos/actualizar', 'DepartamentoController@update'); // Actualizar
        Route::put('/departamentos/eliminar', 'DepartamentoController@destroy'); // Eliminar

        //Gestion Contratos
        Route::get('/gestionp/codcontrato', 'PersonalController@codcontrato'); // Generar Codigo
        Route::get('/gestionp/contratos', 'PersonalController@contratos'); // Listar
        Route::post('/gestionp/regcontrato', 'PersonalController@storecontrato'); // Registrar
        Route::put('/gestionp/actualizar', 'PersonalController@upcontrato'); // Actualizar
        Route::put('/gestionp/eliminar', 'PersonalController@descontrato'); // Eliminar
        Route::post('/gestionp/anexar', 'PersonalController@anexar'); // Eliminar
        
        Route::get('gestionp/selectTraba', 'PersonalController@selectTraba'); //para cargar el select de Trabajo con clientes
        Route::get('gestionp/vercontratos', 'PersonalController@vercontratos'); //para cargar el select de Trabajo con clientes
        Route::get('gestionp/vercontratos2', 'PersonalController@vercontratos2'); //para cargar el select de Trabajo con clientes
        
        //Gestion de Finiquitos
        Route::get('/finiquitos', 'PersonalController@indexfini'); // Listar
        Route::post('/finiquitos/registrar', 'PersonalController@storefini'); // Registrar
        Route::put('/finiquitos/actualizar', 'PersonalController@updatefini'); // Actualizar

         // Asistencias
         Route::get('/asistencias', 'AsistenciaController@index'); // Listar
         Route::post('/asistencia/registrar', 'AsistenciaController@store'); // Registrar
         Route::put('/asistencia/actualizar', 'AsistenciaController@update'); // Actualizar
         Route::put('/asistencia/eliminar', 'AsistenciaController@destroy'); // Eliminar
         Route::get('/asistencia/trabajadores', 'AsistenciaController@indexTrasi'); // Actualizar
         Route::get('/asistencia/tipos', 'AsistenciaController@indexTipos'); // Actualizar
         Route::get('/asistencia/verasis', 'AsistenciaController@validarAsis'); // Actualizar
         // 
 
 
         // Horas Extras
         Route::get('/horas', 'AsistenciaController@indexHoras'); // Listar
         Route::post('/hora/registrar', 'AsistenciaController@storeHoras'); // Registrar
         Route::put('/hora/actualizar', 'AsistenciaController@updateHoras'); // Actualizar
         Route::put('/hora/eliminar', 'AsistenciaController@destroyHoras'); // Eliminar
         // 
 
         // Turnos Extras
         Route::get('/turnos', 'AsistenciaController@indexTurnos'); // Listar
         Route::post('/turnos/registrar', 'AsistenciaController@storeTurnos'); // Registrar
         Route::put('/turnos/actualizar', 'AsistenciaController@updateTurnos'); // Actualizar
         Route::put('/turnos/eliminar', 'AsistenciaController@destroyTurnos'); // Eliminar
         //

         // Calendario de Asistencias
        Route::get('Asistencia/indexAsis','CalendarController@indexAsis');
        Route::get('Asistencia/indexAsis/{month}','CalendarController@index_month_asis');
        
        // Calendario de Asistencia por Trabajador
        Route::get('Asistencia/indexAsis/personal/{id}','CalendarController@indexAsis_tra');
        Route::get('Asistencia/indexAsis/personal/{month}/{id}','CalendarController@index_month_asis_tra');
        // 


    });

    // A su vez tendra otros grupos de Rutas Admnistrador
    Route::group(['middleware' => ['Operador']], function () {
        //Servicios
        Route::get('/servicios', 'ServicioController@index'); // Listar
        Route::post('/servicios/registrar', 'ServicioController@store'); // Registrar
        Route::put('/servicios/actualizar', 'ServicioController@update'); // Actualizar
        Route::put('/servicios/eliminar', 'ServicioController@destroy'); // Eliminar
        Route::get('/servicios/listarTodo', 'ServicioController@listarServicios'); // Listar para asiganarlos a los camiones
        Route::get('servicios/paraModi', 'ServicioController@obtenerServicios'); // Listar para editarlos a los camiones
        
        //Empresas
        Route::get('/empresas/codigo', 'EmpresaController@codigo'); // Generar Codigo
        Route::get('/empresas', 'EmpresaController@index'); // Listar
        Route::post('/empresas/registrar', 'EmpresaController@create'); // Registrar
        Route::put('/empresas/actualizar', 'EmpresaController@update'); // Actualizar
        Route::put('/empresas/eliminar', 'EmpresaController@destroy'); // Eliminar
        
        //Camiones
        Route::get('/camiones/codigo', 'CamionController@codigo'); // Generar Codigo
        Route::get('/camiones', 'CamionController@index'); // Listar
        Route::post('/camiones/registrar', 'CamionController@store'); // Registrar
        Route::put('/camiones/actualizar', 'CamionController@update'); // Actualizar
        Route::put('/camiones/eliminar', 'CamionController@destroy'); // Eliminar
        Route::get('/empresa/selectem', 'EmpresaController@index'); // esta peticion se usa para la seleccion de empresa
        
        //Trabajadores
        Route::get('/personal/codigo', 'PersonalController@codigo'); // Generar Codigo
        Route::get('/personal', 'PersonalController@index'); // Listar
        Route::post('/personal/registrar', 'PersonalController@store'); // Registrar
        Route::put('/personal/actualizar', 'PersonalController@update'); // Actualizar
        Route::put('/personal/eliminar', 'PersonalController@destroy'); // Eliminar
        
        // Gestion de Clientes Empresa
        Route::get('/clientempresa', 'ClienteController@index'); // Listar
        Route::post('/clientempresa/registrar', 'ClienteController@store'); // Registrar
        Route::put('/clientempresa/actualizar', 'ClienteController@update'); // Actualizar
        Route::put('/clientempresa/eliminar', 'ClienteController@destroy'); // Actualizar
        
        // Todos los clientes existentes
        Route::get('/clientempresa/todos', 'ClienteController@todosClientes'); // Listar

        // Gestion de Clientes Particulares
        Route::get('/clienteparti', 'ClienteController@indexParti'); // Listar
        Route::post('/clienteparti/registrar', 'ClienteController@storeParti'); // Registrar
        Route::put('/clienteparti/actualizar', 'ClienteController@updateParti'); // Actualizar
        Route::put('/clienteparti/eliminar', 'ClienteController@destroyParti'); // Actualizar
        
        //Gestiones de Solicitudes Empresas
        Route::get('/gestionsoli', 'SolicitudController@index'); // Listar
        Route::get('/gestionsoli/codigo', 'SolicitudController@codigo'); // Generar Codigo
        Route::get('gestionsoli/selectEmpre', 'SolicitudController@selectEm'); //para cargar el select de clientes empresa
        Route::post('/gestionsoli/registrar', 'SolicitudController@store'); // Registrar
        Route::put('/gestionsoli/actualizar', 'SolicitudController@update'); // Actualizar
        Route::put('/gestionsoli/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('gestionsoli/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        // Gestion de Solicitudes Aprobados
        Route::get('/solicitudes/apro', 'SolicitudController@aprobados'); // prueba
        
        //Gestiones de Solicitudes Particulares
        Route::get('/gestisoliparti', 'SolicitudController@indexParti'); // Listar
        Route::get('/gestisoliparti/codigo', 'SolicitudController@codigoParti'); // Generar Codigo
        Route::get('gestisoliparti/selectParti', 'SolicitudController@selectParti'); //para cargar el select de clientes empresa
        Route::post('/gestisoliparti/registrar', 'SolicitudController@storeParti'); // Registrar
        Route::put('/gestisoliparti/actualizar', 'SolicitudController@updateParti'); // Actualizar
        Route::put('/gestisoliparti/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('/gestisoliparti/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        //Presupuestos
        Route::get('/presupuesto', 'PresupuestoController@index'); // Listar
        Route::get('/presupuesto/total', 'PresupuestoController@indextotal'); // ListarTotal Cotizaciones
        Route::get('/presupuesto/codigo', 'PresupuestoController@codigo'); // Generar Codigo
        Route::post('/presupuesto/registrar', 'PresupuestoController@store'); // Registrar
        Route::get('/presupuesto/obtenerDetalles', 'PresupuestoController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::put('/presupuesto/eliminar', 'PresupuestoController@destroy'); // Eliminar Presupuesto
        Route::put('/presupuesto/aprobar', 'PresupuestoController@aprobar'); // Aprobar Presupuesto
        Route::put('/presupuesto/rechazar', 'PresupuestoController@rechazar'); // Rechazar Presupuesto
        Route::get('/cotizacion/pdf/{id}', 'PresupuestoController@mostrarPdf'); //Pdf del Presupuesto
        // 
        
        // Asignaciones
        Route::get('/asignacion/codigo', 'AsignacionController@codigo'); // Generar Codigo
        Route::get('/asignacion/total', 'AsignacionController@indextotal'); // Listar Total Asignaciones
        Route::get('/asignacion', 'AsignacionController@index'); // Listar
        Route::get('/asignacion/cordi', 'AsignacionController@indexCordi'); // Listar
        Route::get('/asignaciones/obtenerDetalles', 'AsignacionController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::post('/asignacion/registrar', 'AsignacionController@store'); // Registrar
        Route::get('/asignacion/pdf/{id}', 'AsignacionController@mostrarPdf'); //Pdf del Presupuesto
        // 
        
        // ** Rutas adicionales usadas en presupuestos**
        Route::get('/solicitudes', 'SolicitudController@solicitudesTodas'); // Listar Todas las solicitudes
        Route::get('/solicitudes/asignar', 'SolicitudController@solicitudesAsignar'); // Listar Todas las solicitudes
        Route::get('/presupuesto_apro', 'PresupuestoController@aprobados'); // Listar
        //
        
        //Coordinaciones
        Route::get('/coordinacion', 'CoordinacionesController@index'); // Listar
        Route::get('/coordinacion/codigo', 'CoordinacionesController@codigo'); // Generar Codigo
        Route::post('/coordinacion/registrar', 'CoordinacionesController@store'); // Registrar
        Route::get('/coordinacion/obtenerDetalles', 'CoordinacionesController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/personal/coordinacion', 'PersonalController@indexTraCordi'); // Listar Todos los trabajadores hausar en la coordinacion
        Route::get('/camion/validar', 'CoordinacionesController@obtenerValidar'); // Validar camiones para la coordinacion
        Route::get('/trabajador/validar', 'CoordinacionesController@validarTraba'); // Validar camiones para la coordinacion
        
        //Ordenes
        Route::get('/ordenes', 'OrdenController@index'); // Listar
        Route::get('/ordenes/codigo', 'OrdenController@codigo'); // Generar Codigo
        Route::post('/ordenes/registrar', 'OrdenController@store'); // Registrar
        Route::get('/ordenes/obtenerDetalles', 'OrdenController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/orden/pdf/{id}', 'OrdenController@mostrarPdf'); //Pdf del Presupuesto
        Route::put('/orden/cobrar', 'OrdenController@cobrar'); //Pdf del Presupuesto
        
        // Coordinaciones realizadas para usar en el modulo OT
        Route::get('/coordinacion/ot', 'CoordinacionesController@indexListas'); // Listar
        Route::get('/coordinacion/ot2', 'CoordinacionesController@indexListas2'); // Listar
        //
        
        //Guia
        Route::get('/guias', 'GuiaController@index'); // Listar
        Route::get('/guias/codigo', 'GuiaController@codigo'); // Generar Codigo
        Route::post('/guias/registrar', 'GuiaController@store'); // Registrar
        Route::get('/guias/obtenerDetalles', 'GuiaController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::get('/guia/pdf/{id}', 'GuiaController@mostrarPdf'); //Pdf de la Guia
        
        // Evento Coordinacion Calendario //
        Route::post('Evento/create','CalendarController@create');
        // Calendario Con Evento
        Route::get('Evento/index','CalendarController@indexEvento');
        Route::get('Evento/index/{month}','CalendarController@index_month_event');
        Route::get('Evento/details/{id}','CalendarController@details');
        // Route::get('coordinacion/pdf/{id}','CalendarController@pdf');
        
        // Contratos con Empresas y Particular
        Route::get('/contratos', 'ContratosController@index'); // Listar
        Route::get('/contratos/codigo', 'ContratosController@codigo'); // Generar Codigo
        Route::get('contratos/selectcli', 'ContratosController@selectCli'); //para cargar el select de clientes empresa
        Route::post('/contratos/registrar', 'ContratosController@store'); //para cargar el select de clientes empresa
        Route::post('/contratos/serviregistrar', 'ContratosController@store_servi'); //para cargar el select de clientes empresa
        Route::get('/contratos/servicioscli', 'ContratosController@verServicios'); //para cargar el select de clientes empresa
        Route::put('/contratos/actualizarprecio', 'ContratosController@cambiarPrecio'); // para Actualizar en las prendas de empresas
        Route::put('/contratos/eliminarprecio', 'ContratosController@eliminarPrecio'); // para Actualizar en las prendas de empresas
        Route::get('/contratos/validarcli', 'ContratosController@validarCli'); //para cargar el select de clientes empresa

        // Estados de Pagos de Las Guias de Despacho
        Route::get('/estadoguias', 'EstadoController@index'); // Listar
        Route::get('/estadoguias/codigo', 'EstadoController@codigo'); // Listar
        Route::post('/estadoguias/registrar', 'EstadoController@storegui'); // Registrar
        Route::get('/estadoguias/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{contra}/{folio}/{fechao}','EstadoController@mostrarPdf'); // para tomar datos de la bd
        //*********************Resumenes de Guias***********************
        Route::get('/resumen_esguia', 'EstadoController@resumen'); // para tomar datos de la bd
        
        // Estados de Pagos de Las Ordenes de Trabajo
        Route::get('/estadosot', 'EstadoController@indexot'); // Listar
        Route::get('/estadosot/codigo', 'EstadoController@codigot'); // Listar
        Route::post('/estadosot/registrar', 'EstadoController@storeot'); // Registrar
        Route::get('/estadosot/pdf/{id}/{idem}/{ini}/{fin}/{empre}/{folio}/{fechao}','EstadoController@mostrarPdfot'); // para tomar datos de la bd
        //*********************Resumenes de Guias***********************
        Route::get('/resumen_estadosot', 'EstadoController@resumenot'); // para tomar datos de la bd

        // Listar todos los contratos de una empresa
        Route::get('/contratos/cliente', 'ContratosController@verContratos'); //para cargar el select de clientes empresa
        Route::get('/contratos/clientempresa', 'ContratosController@verContratosEmpresa'); //para cargar el select de clientes empresa
        // 
        
        // Dashboard 
        Route::get('/dashboard', 'DashboardController');
        
    });
    

    // A su vez tendra otros grupos de Rutas Admnistrador Adjustar a Secretaria
    Route::group(['middleware' => ['Operador']], function () {
        //Servicios
        Route::get('/servicios', 'ServicioController@index'); // Listar
        Route::post('/servicios/registrar', 'ServicioController@store'); // Registrar
        Route::put('/servicios/actualizar', 'ServicioController@update'); // Actualizar
        Route::put('/servicios/eliminar', 'ServicioController@destroy'); // Eliminar
        Route::get('/servicios/listarTodo', 'ServicioController@listarServicios'); // Listar para asiganarlos a los camiones
        Route::get('servicios/paraModi', 'ServicioController@obtenerServicios'); // Listar para editarlos a los camiones
        
        //Empresas
        Route::get('/empresas/codigo', 'EmpresaController@codigo'); // Generar Codigo
        Route::get('/empresas', 'EmpresaController@index'); // Listar
        Route::post('/empresas/registrar', 'EmpresaController@create'); // Registrar
        Route::put('/empresas/actualizar', 'EmpresaController@update'); // Actualizar
        Route::put('/empresas/eliminar', 'EmpresaController@destroy'); // Eliminar
        
        //Camiones
        Route::get('/camiones/codigo', 'CamionController@codigo'); // Generar Codigo
        Route::get('/camiones', 'CamionController@index'); // Listar
        Route::post('/camiones/registrar', 'CamionController@store'); // Registrar
        Route::put('/camiones/actualizar', 'CamionController@update'); // Actualizar
        Route::put('/camiones/eliminar', 'CamionController@destroy'); // Eliminar
        Route::get('/empresa/selectem', 'EmpresaController@index'); // esta peticion se usa para la seleccion de empresa
        
        //Trabajadores
        Route::get('/personal/codigo', 'PersonalController@codigo'); // Generar Codigo
        Route::get('/personal', 'PersonalController@index'); // Listar
        Route::post('/personal/registrar', 'PersonalController@store'); // Registrar
        Route::put('/personal/actualizar', 'PersonalController@update'); // Actualizar
        Route::put('/personal/eliminar', 'PersonalController@destroy'); // Eliminar
        
        // Gestion de Clientes Empresa
        Route::get('/clientempresa', 'ClienteController@index'); // Listar
        Route::post('/clientempresa/registrar', 'ClienteController@store'); // Registrar
        Route::put('/clientempresa/actualizar', 'ClienteController@update'); // Actualizar
        Route::put('/clientempresa/eliminar', 'ClienteController@destroy'); // Actualizar
        
        // Todos los clientes existentes
        Route::get('/clientempresa/todos', 'ClienteController@todosClientes'); // Listar

        // Gestion de Clientes Particulares
        Route::get('/clienteparti', 'ClienteController@indexParti'); // Listar
        Route::post('/clienteparti/registrar', 'ClienteController@storeParti'); // Registrar
        Route::put('/clienteparti/actualizar', 'ClienteController@updateParti'); // Actualizar
        Route::put('/clienteparti/eliminar', 'ClienteController@destroyParti'); // Actualizar
        
        //Gestiones de Solicitudes Empresas
        Route::get('/gestionsoli', 'SolicitudController@index'); // Listar
        Route::get('/gestionsoli/codigo', 'SolicitudController@codigo'); // Generar Codigo
        Route::get('gestionsoli/selectEmpre', 'SolicitudController@selectEm'); //para cargar el select de clientes empresa
        Route::post('/gestionsoli/registrar', 'SolicitudController@store'); // Registrar
        Route::put('/gestionsoli/actualizar', 'SolicitudController@update'); // Actualizar
        Route::put('/gestionsoli/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('gestionsoli/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        // Gestion de Solicitudes Aprobados
        Route::get('/solicitudes/apro', 'SolicitudController@aprobados'); // prueba
        
        //Gestiones de Solicitudes Particulares
        Route::get('/gestisoliparti', 'SolicitudController@indexParti'); // Listar
        Route::get('/gestisoliparti/codigo', 'SolicitudController@codigoParti'); // Generar Codigo
        Route::get('gestisoliparti/selectParti', 'SolicitudController@selectParti'); //para cargar el select de clientes empresa
        Route::post('/gestisoliparti/registrar', 'SolicitudController@storeParti'); // Registrar
        Route::put('/gestisoliparti/actualizar', 'SolicitudController@updateParti'); // Actualizar
        Route::put('/gestisoliparti/eliminar', 'SolicitudController@destroy'); // Eliminar Solicitud
        Route::get('/gestisoliparti/obtenerDetalles', 'SolicitudController@obtenerDetalles'); //para cargar el select de clientes empresa
        
        //Presupuestos
        Route::get('/presupuesto', 'PresupuestoController@index'); // Listar
        Route::get('/presupuesto/total', 'PresupuestoController@indextotal'); // ListarTotal Cotizaciones
        Route::get('/presupuesto/codigo', 'PresupuestoController@codigo'); // Generar Codigo
        Route::post('/presupuesto/registrar', 'PresupuestoController@store'); // Registrar
        Route::get('/presupuesto/obtenerDetalles', 'PresupuestoController@obtenerDetalles'); //Obtener Detalles del Presupuesto
        Route::put('/presupuesto/eliminar', 'PresupuestoController@destroy'); // Eliminar Presupuesto
        Route::put('/presupuesto/aprobar', 'PresupuestoController@aprobar'); // Aprobar Presupuesto
        Route::put('/presupuesto/rechazar', 'PresupuestoController@rechazar'); // Rechazar Presupuesto
        Route::get('/cotizacion/pdf/{id}', 'PresupuestoController@mostrarPdf'); //Pdf del Presupuesto
        // 
        
        // ** Rutas adicionales usadas en presupuestos**
        Route::get('/solicitudes', 'SolicitudController@solicitudesTodas'); // Listar Todas las solicitudes
        Route::get('/solicitudes/asignar', 'SolicitudController@solicitudesAsignar'); // Listar Todas las solicitudes
        Route::get('/presupuesto_apro', 'PresupuestoController@aprobados'); // Listar
        //
        
        
        // Coordinaciones realizadas para usar en el modulo OT
        Route::get('/coordinacion/ot', 'CoordinacionesController@indexListas'); // Listar
        Route::get('/coordinacion/ot2', 'CoordinacionesController@indexListas2'); // Listar
        //
        
        // Evento Coordinacion Calendario //
        Route::post('Evento/create','CalendarController@create');
        // Calendario Con Evento
        Route::get('Evento/index','CalendarController@indexEvento');
        Route::get('Evento/index/{month}','CalendarController@index_month_event');
        Route::get('Evento/details/{id}','CalendarController@details');
        // Route::get('coordinacion/pdf/{id}','CalendarController@pdf');
        
        // Contratos con Empresas y Particular
        Route::get('/contratos', 'ContratosController@index'); // Listar
        Route::get('/contratos/codigo', 'ContratosController@codigo'); // Generar Codigo
        Route::get('contratos/selectcli', 'ContratosController@selectCli'); //para cargar el select de clientes empresa
        Route::post('/contratos/registrar', 'ContratosController@store'); //para cargar el select de clientes empresa
        Route::post('/contratos/serviregistrar', 'ContratosController@store_servi'); //para cargar el select de clientes empresa
        Route::get('/contratos/servicioscli', 'ContratosController@verServicios'); //para cargar el select de clientes empresa
        Route::put('/contratos/actualizarprecio', 'ContratosController@cambiarPrecio'); // para Actualizar en las prendas de empresas
        Route::put('/contratos/eliminarprecio', 'ContratosController@eliminarPrecio'); // para Actualizar en las prendas de empresas
        Route::get('/contratos/validarcli', 'ContratosController@validarCli'); //para cargar el select de clientes empresa

        // Listar todos los contratos de una empresa
        Route::get('/contratos/cliente', 'ContratosController@verContratos'); //para cargar el select de clientes empresa
        Route::get('/contratos/clientempresa', 'ContratosController@verContratosEmpresa'); //para cargar el select de clientes empresa
        // 
        
        // Dashboard 
        Route::get('/dashboard', 'DashboardController');
        
    });
    
});

// Route::get('/home', 'HomeController@index')->name('home');




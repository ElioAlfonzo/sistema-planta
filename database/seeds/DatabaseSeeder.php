<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTablas([
            'departamentos',
            'formacostos'
            
        ]);

        $this->call(DepartamentosTableSeeder::class);
        $this->call(FormacostosTableSeeder::class);
    }

    protected function truncateTablas(array $tablas) {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_kEY_CHECKS = 1;');
    }
}

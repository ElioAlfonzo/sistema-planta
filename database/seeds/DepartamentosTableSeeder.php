<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamentos =[
            'RRHH',
            'Finanzas',
            'Mecanica',
            'Gerencia'
        ];
        foreach ($departamentos as $key => $value){
            DB::table('departamentos')->insert([
                'nombre_depa' => $value,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}

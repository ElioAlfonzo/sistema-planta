<?php

use App\Models\Formacosto;
use Illuminate\Database\Seeder;

class FormacostosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Formacosto::class,50)->create();
    }
}

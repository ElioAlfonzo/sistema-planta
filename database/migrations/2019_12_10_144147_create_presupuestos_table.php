<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_pre',15);
            $table->date('fecha_pre');
            $table->decimal('monto_pre', 11, 2);
            $table->decimal('iva_pre', 11, 2);
            $table->decimal('total_pre', 11, 2);
            $table->text('observacion_presu')->nullable();
            $table->boolean('estado_presu')->default(1);            

            //Solicitudes
            $table->integer('respuesta_presu')->unsigned()->nullable();
            $table->foreign('respuesta_presu')->references('id')->on('estados_pro');

            //Solicitudes
            $table->integer('soli_id')->unsigned()->nullable();
            $table->foreign('soli_id')->references('id')->on('solicitudes');
            
            // Clientes
            $table->integer('idclientempre')->unsigned()->nullable();
            $table->foreign('idclientempre')->references('id')->on('clientempresas');

            //Empresa que realizara el trabajo
            $table->integer('id_empre')->unsigned()->nullable();
            $table->foreign('id_empre')->references('id')->on('empresas');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}

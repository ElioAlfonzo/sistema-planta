<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_fact',15)->unique();
            $table->date('fechaemi');
            $table->date('fechavenci');

            //Cliente
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')->references('id')->on('clientempresas');
            
            //Empresa
            $table->integer('id_empre')->unsigned();
            $table->foreign('id_empre')->references('id')->on('empresas');


            $table->decimal('monto', 11, 2);
            $table->decimal('monto_actual', 11, 2);
            $table->text('glosa')->nullable();

            // Tipo 0 Sin contrato 1 Con contrato
            $table->integer('tipo_contra')->unsigned()->nullable();

            //Tipo 1 es por OT y tipo 2 es por Guia
            $table->integer('tipo_factu')->unsigned()->nullable();

            //Estados del proceso
            $table->integer('pro_factu')->unsigned()->nullable();
            $table->foreign('pro_factu')->references('id')->on('estados_pro');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            $table->boolean('estado_fact')->default(1);  //este es para eliminarlo o no


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturaciones');
    }
}

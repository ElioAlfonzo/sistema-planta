<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoAsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_asi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_tipo',150)->unique();
        });

        DB::table('tipo_asi')->insert(array('id'=>'1','nombre_tipo'=>'Entrada'));
        DB::table('tipo_asi')->insert(array('id'=>'2','nombre_tipo'=>'Salida'));
        DB::table('tipo_asi')->insert(array('id'=>'3','nombre_tipo'=>'Ausente'));
        DB::table('tipo_asi')->insert(array('id'=>'4','nombre_tipo'=>'Vacaciones'));
        DB::table('tipo_asi')->insert(array('id'=>'5','nombre_tipo'=>'Licencia Medica'));
        DB::table('tipo_asi')->insert(array('id'=>'6','nombre_tipo'=>'Permiso'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_asi');
    }
}

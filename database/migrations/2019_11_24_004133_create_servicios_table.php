<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomb_servi', 150)->unique();
            $table->string('descrip_servi', 256)->nullable();
            $table->boolean('estado_servi')->default(1);
            $table->timestamps();
        });
        DB::table('servicios')->insert(array('id' => '1','nomb_servi'=>'Limpieza de Fosa','descrip_servi'=>'Limpieza de Fosa'));
        DB::table('servicios')->insert(array('id' => '2','nomb_servi'=>'Limpieza de Ductos','descrip_servi'=>'Limpieza de Ductos'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}

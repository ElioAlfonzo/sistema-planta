<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCordiTraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cordi_tra', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_tra', 100);

            //Trabajador
            $table->integer('id_tra')->unsigned();
            $table->foreign('id_tra')->references('id')->on('trabajadores');
            
            //Trabajador
            $table->integer('id_depa')->unsigned();
            $table->foreign('id_depa')->references('id')->on('departamentos');

            //Cordinacion
            $table->integer('id_cordi')->unsigned();
            $table->foreign('id_cordi')->references('id')->on('cordinaciones');

            $table->boolean('chofer')->default(0);  //Para ver Cual sera el chofer que saldra en l Estado P

            $table->boolean('estado_cordi_tra')->default(1);  //este es para eliminarlo o no

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cordi_tra');
    }
}

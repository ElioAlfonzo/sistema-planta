<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_pro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_es',50);
            $table->string('descrip_es',50);
            $table->timestamps();
        });

        // Todos los estados para clientes sin contrato
        DB::table('estados_pro')->insert(array('id' => '1','nombre_es'=>'Pendiente Cotizacion','descrip_es'=>'Pendiente Cotizacion'));  

        DB::table('estados_pro')->insert(array('id' => '2','nombre_es'=>'Pendiente Aprobacion','descrip_es'=>'Pendiente Aprobacion')); 

        DB::table('estados_pro')->insert(array('id' => '3','nombre_es'=>'Pendiente de Coordinacion','descrip_es'=>'Pendiente de Coordinacion')); 

        DB::table('estados_pro')->insert(array('id' => '4','nombre_es'=>'Rechazado','descrip_es'=>'Rechazado')); 

        DB::table('estados_pro')->insert(array('id' => '5','nombre_es'=>'Asignar Servicios','descrip_es'=>'Asignar Servicios')); 

        DB::table('estados_pro')->insert(array('id' => '6','nombre_es'=>'Pendiente de Coordinacion','descrip_es'=>'Pendiente de Coordinacion')); 

        DB::table('estados_pro')->insert(array('id' => '7','nombre_es'=>'Coordinado','descrip_es'=>'Coordinado')); 
        
        DB::table('estados_pro')->insert(array('id' => '8','nombre_es'=>'Pendiente','descrip_es'=>'Pendiente')); 

        DB::table('estados_pro')->insert(array('id' => '9','nombre_es'=>'EEPP','descrip_es'=>'EEPP')); 

        DB::table('estados_pro')->insert(array('id' => '10','nombre_es'=>'Facturado','descrip_es'=>'Facturado')); 

        DB::table('estados_pro')->insert(array('id' => '11','nombre_es'=>'Pagado','descrip_es'=>'Pagado')); 

        DB::table('estados_pro')->insert(array('id' => '12','nombre_es'=>'Facturado y Pag','descrip_es'=>'Facturado y Pag')); 

        DB::table('estados_pro')->insert(array('id' => '13','nombre_es'=>'Pendiente','descrip_es'=>'Cheque Pendiente')); 

        DB::table('estados_pro')->insert(array('id' => '14','nombre_es'=>'ONP','descrip_es'=>'Cheque Orden de No Pago')); 

        DB::table('estados_pro')->insert(array('id' => '15','nombre_es'=>'Potestado','descrip_es'=>'Cheque Potestado')); 

        DB::table('estados_pro')->insert(array('id' => '16','nombre_es'=>'Pagado','descrip_es'=>'Cheque Pagado')); 


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_pro');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EspagoGuia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espago_guias', function (Blueprint $table) {
            $table->increments('id');

            $table->string('folio_epg',15)->unique();
            $table->date('fechaini');
            $table->date('fechafin');
            $table->date('fechaorigen');

            //Dinero
            $table->decimal('esguia_neto', 11, 2);
            $table->decimal('esguia_iva', 11, 2);
            $table->decimal('esguia_total', 11, 2);

            //Empresa
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');

            //Cliente
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')->references('id')->on('clientempresas');

            //Contrato
            $table->integer('id_contrato')->unsigned()->nullable();
            $table->foreign('id_contrato')->references('id')->on('contratos');

            //Tipo tipo 0 es sin contrato y tipo 1 es con contrato
            $table->integer('tipo')->unsigned()->nullable();

            // Observaciones
            $table->text('observa_espa')->nullable();

            //Estados del proceso
            $table->integer('respu_espa')->unsigned()->nullable();
            $table->foreign('respu_espa')->references('id')->on('estados_pro');

            //Este es para eliminarlo o no
            $table->boolean('estado_espa')->default(1);

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

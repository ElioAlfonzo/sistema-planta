<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tra_ordenes', function (Blueprint $table) {
            $table->increments('id');

            //Ordenes
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')->on('ordenes_tra');

            //Servicios
            $table->integer('servi_id')->unsigned()->nullable();
            $table->foreign('servi_id')->references('id')->on('servicios');
            
            //Forma de Cobro
            $table->integer('forma_id')->unsigned()->nullable();
            $table->foreign('forma_id')->references('id')->on('formacostos');

            $table->date('fecha_ot');
            $table->time('hora_ot');
            
            $table->integer('cantidad');
            $table->decimal('valor_uni', 11, 2);
            $table->decimal('valor_neto', 11, 2);

            //para ver si esta dentro de un estado de pago o no, 1 si 0 no
            $table->boolean('pago_orden')->default(0);

            //Estao de Pago de Guia al que pertenece
            $table->integer('cual_espago')->unsigned()->nullable();
            $table->foreign('cual_espago')->references('id')->on('espago_tra');

            $table->boolean('estado')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tra_ordenes');
    }
}

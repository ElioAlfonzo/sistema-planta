<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factu_detalles', function (Blueprint $table) {
            $table->increments('id');
            //Es id de la Factura
            $table->integer('id_factu')->unsigned();
            $table->foreign('id_factu')->references('id')->on('facturaciones');
            
            //Estado de Pago OT
            $table->integer('pago_ot')->unsigned()->nullable();
            $table->foreign('pago_ot')->references('id')->on('espago_tra');

            //Estado de Pago GUIA
            $table->integer('pago_guia')->unsigned()->nullable();
            $table->foreign('pago_guia')->references('id')->on('espago_guias');

            //Tipo 1 es por OT y tipo 0 es por Guia
            $table->integer('tipo_pago')->unsigned();

            $table->boolean('estado_deta')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factu_detalles');
    }
}

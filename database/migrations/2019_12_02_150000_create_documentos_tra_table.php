<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos_tra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_contrato');
            $table->string('jornada', 50);
            $table->date('fecha_ini')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->string('tipo_contrato', 50);
            $table->integer('estado_contratos')->default(1);

            $table->integer('id_traem')->unsigned();//para poder relacionar con roles;
            $table->foreign('id_traem')->references('id')->on('traba_empresas');

            //relacion con trabajadores
            $table->integer('trabajador_id')->unsigned();
            $table->foreign('trabajador_id')->references('id')->on('trabajadores');


            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos_tra');
    }
}

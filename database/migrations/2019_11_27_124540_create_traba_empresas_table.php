<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabaEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traba_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_contrato');
            $table->string('rut', 50);

            //relacion con empresas 
            $table->integer('departamento_id')->unsigned();
            $table->foreign('departamento_id')->references('id')->on('departamentos');

            //relacion con empresas 
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');

            //Zona a la que pertenece el registro
            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');
            //

            //relacion con trabajadores
            $table->integer('trabajador_id')->unsigned();
            $table->foreign('trabajador_id')->references('id')->on('trabajadores');

            $table->string('tipo_contrato', 50);
            $table->string('cuenta', 20);
            $table->decimal('gratificacion', 11, 2);
            $table->decimal('movilizacion', 11, 2);
            $table->decimal('colacion', 11, 2);
            $table->decimal('bonos', 11, 2);
            $table->decimal('remuneracion', 11, 2);

            //Zona a la que pertenece el registro
            $table->integer('banco')->unsigned();
            $table->foreign('banco')->references('id')->on('bancos');
            //
            
            $table->string('archivos', 500);

            $table->date('fecha_ini')->nullable();
            $table->date('fecha_fin')->nullable();

            $table->date('fecha_ci')->nullable();
            $table->date('fecha_lic')->nullable();

            $table->string('jornada', 50);
            $table->integer('estado_trabajo')->default(1);
            $table->integer('estado_tra_em')->default(1);


           //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traba_empresas');
    }
}

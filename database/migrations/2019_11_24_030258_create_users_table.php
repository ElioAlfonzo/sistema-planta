<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('usuario')->unique();
            $table->string('password');
            $table->string('condicion')->default('1');

            $table->integer('idrol')->unsigned();
            $table->foreign('idrol')->references('id')->on('roles');

            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');

            $table->integer('id_tra')->unsigned()->unique();
            $table->foreign('id_tra')->references('id')->on('trabajadores');

            $table->rememberToken();
            // $table->timestamps();
        });
        DB::table('users')->insert(array('id' => '1','usuario'=>'admin','password'=>'$2y$10$eoXUEkXIrvxMgY78lGBg7eFKdiRc5lnVc0doOWHm9.MtWub/eNIxK',
        'condicion'=>'1','idrol'=>'1','idzona'=>'1','id_tra'=>1,'remember_token'=>NULL));  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

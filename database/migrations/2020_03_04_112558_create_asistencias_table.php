<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->increments('id');

            //traba_empresas trabajadores en empresa
            $table->integer('id_traba')->unsigned();
            $table->foreign('id_traba')->references('id')->on('traba_empresas');

            $table->date('fecha_asi');
            $table->time('hora_asi');
            
            //Tipo
            $table->integer('tipo')->unsigned();
            $table->foreign('tipo')->references('id')->on('tipo_asi');

            $table->text('observa_asi')->nullable();

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            //Usuarios
            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');
            //

            $table->boolean('estado_asi')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
}

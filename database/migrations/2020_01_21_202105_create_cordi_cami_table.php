<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCordiCamiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cordi_cami', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ppu', 50);

            //Trabajador
            $table->integer('id_cordi')->unsigned();
            $table->foreign('id_cordi')->references('id')->on('cordinaciones');

            //Camiones
            $table->integer('id_cami')->unsigned();
            $table->foreign('id_cami')->references('id')->on('camiones');

            $table->boolean('estado_cordi_cami')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cordi_cami');
    }
}

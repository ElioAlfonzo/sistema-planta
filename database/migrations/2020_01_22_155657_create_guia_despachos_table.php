<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuiaDespachosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guia_despachos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('folio_guia',15)->unique();
            $table->date('fecha_guia');
            $table->time('hora_guia');

            
            $table->text('observa_guia')->nullable();
            // $table->text('descrip_guia')->nullable();

            //Empresa
            $table->integer('id_empre')->unsigned();
            $table->foreign('id_empre')->references('id')->on('empresas');

            //Cliente
            $table->integer('id_cli')->unsigned();
            $table->foreign('id_cli')->references('id')->on('clientempresas');

            //Solicitud
            $table->integer('id_soli')->unsigned();
            $table->foreign('id_soli')->references('id')->on('solicitudes');

            //Presupuestos
            $table->integer('pre_id')->unsigned()->nullable();
            $table->foreign('pre_id')->references('id')->on('presupuestos');

            //Asignacion
            $table->integer('id_asig')->unsigned()->nullable();;
            $table->foreign('id_asig')->references('id')->on('asignaciones');

            //Contrato
            $table->integer('id_contrato')->unsigned()->nullable();;
            $table->foreign('id_contrato')->references('id')->on('contratos');

            //Tipo tipo 1 es por presupuesto y tipo 0 es por Asignacion
            $table->integer('tipo')->unsigned()->nullable();

            //Coordinaciones
            $table->integer('id_coordi')->unsigned();
            $table->foreign('id_coordi')->references('id')->on('cordinaciones');

            //Dinero
            $table->decimal('monto_guia', 11, 2);
            $table->decimal('iva_guia', 11, 2);
            $table->decimal('total_guia', 11, 2);
            
            //Estados del proceso
            $table->integer('respuesta_gui')->unsigned()->nullable();
            $table->foreign('respuesta_gui')->references('id')->on('estados_pro');

            $table->boolean('estado_guia')->default(1);  //este es para eliminarlo o no

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guia_despachos');
    }
}

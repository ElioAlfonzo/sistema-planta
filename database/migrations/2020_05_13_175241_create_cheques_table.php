<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheques', function (Blueprint $table) {
            $table->increments('id');
            
            //Empresa
            $table->integer('id_cuenta')->unsigned();
            $table->foreign('id_cuenta')->references('id')->on('cuentas');

            $table->string('n_cheque',50);

            $table->date('fecha_emi');
            $table->date('fecha_pago');

            $table->date('fecha_real')->nullable();;

            $table->decimal('monto_che', 11, 2);

            $table->text('glosa_che')->nullable();

            //Estados del proceso
            $table->integer('pro_che')->unsigned()->nullable();
            $table->foreign('pro_che')->references('id')->on('estados_pro');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->boolean('estado_che')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheques');
    }
}

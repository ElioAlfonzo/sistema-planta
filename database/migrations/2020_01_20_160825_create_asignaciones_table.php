<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsignacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('folio_asi',15);
            $table->date('fecha_asi');
            $table->decimal('monto_asi', 11, 2);
            $table->decimal('iva_asi', 11, 2);
            $table->decimal('total_asi', 11, 2);
            $table->text('observacion_asi')->nullable();
            $table->boolean('estado_asi')->default(1);            
    
            //Solicitudes
            $table->integer('respuesta_asi')->unsigned()->nullable();
            $table->foreign('respuesta_asi')->references('id')->on('estados_pro');
    
            //Solicitudes
            $table->integer('soli_asi')->unsigned()->nullable();
            $table->foreign('soli_asi')->references('id')->on('solicitudes');

            //Contratos
            $table->integer('clicontratos')->unsigned()->nullable();
            $table->foreign('clicontratos')->references('id')->on('contratos');
            
            // Clientes
            $table->integer('idclientempre')->unsigned()->nullable();
            $table->foreign('idclientempre')->references('id')->on('clientempresas');
    
            //Empresa que realizara el trabajo
            $table->integer('id_empre')->unsigned()->nullable();
            $table->foreign('id_empre')->references('id')->on('empresas');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignaciones');
    }
}

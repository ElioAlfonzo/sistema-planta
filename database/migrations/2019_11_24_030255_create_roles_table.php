<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150)->unique();
            $table->text('descripcion')->nullable();
            $table->boolean('condicion')->default(1);
        });
        DB::table('roles')->insert(array('id'=>'1','nombre'=>'Administrador','descripcion'=>'Administrador de Areas'));
        DB::table('roles')->insert(array('id'=>'2','nombre'=>'RRHH','descripcion'=>'Recursos Humanos'));
        DB::table('roles')->insert(array('id'=>'3','nombre'=>'Operaciones','descripcion'=>'Operaciones'));
        DB::table('roles')->insert(array('id'=>'4','nombre'=>'Secretaria','descripcion'=>'Secretaria'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}

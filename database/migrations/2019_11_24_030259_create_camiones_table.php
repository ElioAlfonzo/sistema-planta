<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_camion');
            $table->string('ppu', 50);
            $table->date('anio');
            $table->string('marca', 50);
            $table->integer('km');
            $table->string('color', 50);
            $table->string('foto_camion', 100);

            $table->integer('empresa')->unsigned();
            $table->foreign('empresa')->references('id')->on('empresas');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            //Usuarios
            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');
            //

            $table->integer('estado_camion')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camiones');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientempresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientempresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut', 20)->nullable();
            $table->string('razon', 100);
            $table->text('direccion_em');
            $table->string('tel_em', 20);
            $table->string('nombre_contac', 100)->nullable();
            $table->string('correo_contac', 50);
            $table->string('tel_contac', 20)->nullable();
            $table->integer('tipocli');
            $table->boolean('contrato')->default(0);
            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            //Zona a la que pertenece el registro
            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');
            //

            $table->boolean('estado_em')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientempresas');
    }
}

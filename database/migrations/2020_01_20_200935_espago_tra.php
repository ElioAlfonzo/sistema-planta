<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EspagoTra extends Migration
{
    
    public function up()
    {
        Schema::create('espago_tra', function (Blueprint $table) {
            $table->increments('id');

            $table->string('folio_ept',15)->unique();
            $table->date('fechaini');
            $table->date('fechafin');
            $table->date('fechaorigen');

            //Dinero
            $table->decimal('pagotra_neto', 11, 2);
            $table->decimal('pagotra_iva', 11, 2);
            $table->decimal('pagotra_total', 11, 2);

            //Empresa
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');

            //Cliente
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')->references('id')->on('clientempresas');

            //Contrato
            $table->integer('id_contrato')->unsigned()->nullable();
            $table->foreign('id_contrato')->references('id')->on('contratos');

            //Tipo tipo 0 es sin contrato y tipo 1 es con contrato
            $table->integer('tipo')->unsigned()->nullable();

            // Observaciones
            $table->text('observa_tra')->nullable();

            //Estados del proceso
            $table->integer('respuesta_tra')->unsigned()->nullable();
            $table->foreign('respuesta_tra')->references('id')->on('estados_pro');
            

            //Este es para eliminarlo o no
            $table->boolean('estado')->default(1);

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    
    public function down()
    {
        //
    }
}


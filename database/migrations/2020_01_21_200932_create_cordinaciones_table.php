<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCordinacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cordinaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_cordi',15);
            
            $table->date('fecha');
            $table->time('hora');

            $table->date('fecha_fin');
            $table->time('hora_fin');

            $table->string('razon', 100);

            $table->text('observacion_cordi')->nullable();
            
            //Solicitudes
            $table->integer('respuesta_cordi')->unsigned()->nullable();
            $table->foreign('respuesta_cordi')->references('id')->on('estados_pro');

            $table->boolean('estado_cordi')->default(1);  //este es para eliminarlo o no
            
            // //Este para saber en que parte va ejecutado o no
            // $table->integer('respuesta_codi')->unsigned()->nullable();cls
            // $table->foreign('respuesta_cordi')->references('id')->on('estados_pro');

             //Solicitudes
             $table->integer('soli_id')->unsigned()->nullable();
             $table->foreign('soli_id')->references('id')->on('solicitudes');

             //Presupuestos
             $table->integer('pre_id')->unsigned()->nullable();
             $table->foreign('pre_id')->references('id')->on('presupuestos');

             //Asignaciones
             $table->integer('asig_id')->unsigned()->nullable();
             $table->foreign('asig_id')->references('id')->on('asignaciones');

            // Aqui el numero de folio de cualquiera de los 2 tipos
             $table->string('folio_tipo',15)->nullable();
             
             //Tipo tipo 1 es por presupuesto y tipo 0 es por Asignacion
             $table->integer('tipo')->unsigned()->nullable();

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cordinaciones');
    }
}

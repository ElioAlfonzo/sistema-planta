<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_em');
            $table->string('rut_em', 50)->unique();
            $table->string('telefono_em', 50);
            $table->string('correo_em', 50);
            $table->text('direccion_em');
            $table->text('giro');
            $table->string('nombre_em', 50)->unique();
            $table->text('descrip_em')->nullable(); //pequeña descripcion que aparece en los formatos
            $table->text('descrip_dos')->nullable(); //pequeña descripcion que aparece en los formatos2
            
            
            $table->boolean('estado_em')->default(1);
            $table->text('empre_image')->nullable();
            // $table->timestamps();
        });
        // DB::table('empresas')->insert(array('id' => '1','codigo_em'=>'1','nombre_em'=>'Ecofosa','descrip_em'=>'Ecofosa','direccion_em'=>'Limache'));
        // DB::table('empresas')->insert(array('id' => '2','codigo_em'=>'2','nombre_em'=>'Induclean EIRL','descrip_em'=>'Induclean EIRL','direccion_em'=>'Limache'));
        // DB::table('empresas')->insert(array('id' => '3','codigo_em'=>'3','nombre_em'=>'Induclean Spa','descrip_em'=>'Induclean Spa','direccion_em'=>'Limache'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}

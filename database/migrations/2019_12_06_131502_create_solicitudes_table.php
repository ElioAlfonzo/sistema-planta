<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_soli',15);
            $table->date('fecha_soli');
            $table->string('ciudad',100);
            $table->text('direccion_soli');
            $table->text('observacion');
            $table->boolean('contrato')->default(0);
            $table->boolean('desplaza')->default(0);
            
            
            //respuesta Solicitud
            $table->integer('respuesta_soli')->unsigned()->nullable();
            $table->foreign('respuesta_soli')->references('id')->on('estados_pro');

            //cliente Particulares
            $table->integer('idclientempre')->unsigned()->nullable();
            $table->foreign('idclientempre')->references('id')->on('clientempresas');
            //

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->boolean('estado_soli')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiCamionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servi_camiones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('servicios')->unsigned();
            $table->foreign('servicios')->references('id')->on('servicios');

            $table->integer('camiones')->unsigned();
            $table->foreign('camiones')->references('id')->on('camiones');
            
            $table->string('nombre',50);
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servi_camiones');
    }
}

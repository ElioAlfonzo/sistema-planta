<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cartola extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {          
        Schema::create('cartolas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('origen',150); //de donde viene
            $table->string('numero',50); //numero identificador o Folio
            
            //Empresa
            $table->integer('cuenta_id')->unsigned();
            $table->foreign('cuenta_id')->references('id')->on('cuentas');

            $table->integer('tipo');
            // 1 Efectivo
            // 2 Credito
            // 3 Transferencia
            // 4 Cheque

            $table->decimal('ingresos', 11, 2)->default(0);
            $table->decimal('egresos', 11, 2)->default(0);

            $table->boolean('estado_cartola')->default(1);  //este es para eliminarlo o no

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

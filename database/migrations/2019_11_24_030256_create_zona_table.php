<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_zona',150)->unique();
            $table->text('descrip_zona')->nullable();
            $table->boolean('estado_zona')->default(1);
        });

        DB::table('zona')->insert(array('id'=>'1','nombre_zona'=>'Limache','descrip_zona'=>'Limache'));
        DB::table('zona')->insert(array('id'=>'2','nombre_zona'=>'Arica','descrip_zona'=>'Arica'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zona');
    }
}

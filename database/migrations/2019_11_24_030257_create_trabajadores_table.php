<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabajadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_tra', 80);
            $table->string('apellido_tra', 80);
            $table->string('rut', 50)->unique();
            $table->date('fecha_nacitra');
            $table->string('afp', 50);
            $table->string('salud', 50);
            $table->string('telefono', 50);
            $table->string('tel_adicional', 50)->nullable();;
            $table->string('correo_tra', 50);
            $table->text('direccion_tra');
            $table->string('nacionalidad', 50);
            $table->string('estado_civil', 50);
            $table->integer('numero_cargas');

            $table->integer('estado_tra')->default(1);
        
            $table->timestamps();

        });

        DB::table('trabajadores')->insert(array('id' => '1','nombre_tra'=>'Elio',
        'apellido_tra'=>'Sanchez','rut'=>'26305217-k','fecha_nacitra'=>'2020-01-22','afp'=>'Plan vital','salud'=>'Fonasa',
        'telefono'=>'956259823','correo_tra'=>'elio@gmail.com','direccion_tra'=>'Valparaiso','nacionalidad'=>'Venezolano',
        'estado_civil'=>'soltero','numero_cargas'=>'2'));  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajadores');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesTraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes_tra', function (Blueprint $table) {
            $table->increments('id');

            $table->string('folio_ot',15)->unique();
            $table->date('fecha_ot');
            $table->time('hora_ot');

            //Empresa
            $table->integer('id_empre')->unsigned();
            $table->foreign('id_empre')->references('id')->on('empresas');

            //Cliente
            $table->integer('id_cli')->unsigned();
            $table->foreign('id_cli')->references('id')->on('clientempresas');

            //Solicitud
            $table->integer('id_soli')->unsigned();
            $table->foreign('id_soli')->references('id')->on('solicitudes');

            //Presupuesto
            $table->integer('id_presu')->unsigned()->nullable();
            $table->foreign('id_presu')->references('id')->on('presupuestos');

            //Asignacion
            $table->integer('id_asig')->unsigned()->nullable();;
            $table->foreign('id_asig')->references('id')->on('asignaciones');

            //Contrato
            $table->integer('id_contrato')->unsigned()->nullable();;
            $table->foreign('id_contrato')->references('id')->on('contratos');

            //Tipo tipo 1 es por presupuesto y tipo 0 es por Asignacion
            $table->integer('tipo')->unsigned()->nullable();

            //Coordinaciones
            $table->integer('id_coordi')->unsigned();
            $table->foreign('id_coordi')->references('id')->on('cordinaciones');

            $table->text('observac_ot')->nullable();

            $table->boolean('estado_orden')->default(1);  //este es para eliminarlo o no

            //Dinero
            $table->decimal('monto_ot', 11, 2);
            $table->decimal('iva_ot', 11, 2);
            $table->decimal('total_ot', 11, 2);

            //Estados del proceso
            $table->integer('respuesta_ot')->unsigned()->nullable();
            $table->foreign('respuesta_ot')->references('id')->on('estados_pro');

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenes_tra');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurnosExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos_extras', function (Blueprint $table) {
            $table->increments('id');
            
            //traba_empresas trabajadores en empresa
            $table->integer('id_traba')->unsigned();
            $table->foreign('id_traba')->references('id')->on('traba_empresas');

            $table->date('fecha_tur');
            $table->time('hora_tur');
            
            //Tipo
            $table->integer('tipo_tur')->unsigned();
            $table->foreign('tipo_tur')->references('id')->on('tipo_asi');

            $table->text('observa_tur')->nullable();

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            //Usuarios
            $table->integer('idzona')->unsigned();
            $table->foreign('idzona')->references('id')->on('zona');
            //

            $table->boolean('estado_tur')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos_extras');
    }
}

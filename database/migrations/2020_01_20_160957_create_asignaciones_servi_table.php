<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsignacionesServiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignaciones_servi', function (Blueprint $table) {
            $table->increments('id');

            //Asignaciones
            $table->integer('asig_id')->unsigned()->nullable();
            $table->foreign('asig_id')->references('id')->on('asignaciones');
            
            //Servicios
            $table->integer('servi_id')->unsigned()->nullable();
            $table->foreign('servi_id')->references('id')->on('servicios');

            //Forma de Cobro
            $table->integer('forma_id')->unsigned()->nullable();
            $table->foreign('forma_id')->references('id')->on('formacostos');
            
            $table->integer('cantidad');
            $table->decimal('valor_uni', 11, 2);
            $table->decimal('valor_neto', 11, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignaciones_servi');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContraServiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contra_servi', function (Blueprint $table) {
            $table->increments('id');
            
            //Contratos
            $table->integer('id_con')->unsigned();
            $table->foreign('id_con')->references('id')->on('contratos');

            //Servicios
            $table->integer('id_servi')->unsigned();
            $table->foreign('id_servi')->references('id')->on('servicios');

            //Formas de Costos
            $table->integer('id_formac')->unsigned();
            $table->foreign('id_formac')->references('id')->on('formacostos');

            //Cliente
            $table->integer('id_cli')->unsigned();
            $table->foreign('id_cli')->references('id')->on('clientempresas');

            $table->decimal('precio', 11, 2);

            $table->boolean('estado_contraservi')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contra_servi');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosServiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos_servi', function (Blueprint $table) {
            $table->increments('id');
            
            //Presupuestos
            $table->integer('presu_id')->unsigned()->nullable();
            $table->foreign('presu_id')->references('id')->on('presupuestos');
            
            //Servicios
            $table->integer('servi_id')->unsigned()->nullable();
            $table->foreign('servi_id')->references('id')->on('servicios');

            //Forma de Cobro
            $table->integer('forma_id')->unsigned()->nullable();
            $table->foreign('forma_id')->references('id')->on('formacostos');
            
            $table->integer('cantidad');
            $table->decimal('valor_uni', 11, 2);
            $table->decimal('valor_neto', 11, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos_servi');
    }
}

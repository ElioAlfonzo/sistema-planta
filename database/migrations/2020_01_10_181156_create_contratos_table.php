<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_contra',15)->unique();

            // Nombre del Contrato
            $table->string('nombre_contra', 150);
            //

            // Fecha y hora del Contrato
            $table->dateTime('fecha_contra');
            
            //Cliente
            $table->integer('id_cli')->unsigned();
            $table->foreign('id_cli')->references('id')->on('clientempresas');

            //Empresa con la que hizo el contrato
            $table->integer('id_empre')->unsigned()->nullable();
            $table->foreign('id_empre')->references('id')->on('empresas');

            $table->boolean('estado_contra')->default(1);  //este es para eliminarlo o no

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}

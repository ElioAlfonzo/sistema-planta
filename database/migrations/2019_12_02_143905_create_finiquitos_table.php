<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiniquitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finiquitos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_contrato');
            $table->date('fecha_finiquito');
            $table->string('causal', 100);
            $table->string('estadofiniquito', 100);
            $table->integer('id_traem')->unsigned();//para poder relacionar con roles;
            $table->foreign('id_traem')->references('id')->on('traba_empresas');
            $table->integer('estado_status')->default(1);

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finiquitos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_depa',150)->unique();
            $table->text('descrip_depa')->nullable();
            $table->boolean('estado_depa')->default(1);
            $table->timestamps();
        });

        DB::table('departamentos')->insert(array('id' => '1','nombre_depa'=>'Chofer','descrip_depa'=>'Chofer'));    
        DB::table('departamentos')->insert(array('id' => '2','nombre_depa'=>'Mecanico','descrip_depa'=>'Mecanico'));
        DB::table('departamentos')->insert(array('id' => '3','nombre_depa'=>'Operaciones','descrip_depa'=>'Operaciones'));
        DB::table('departamentos')->insert(array('id' => '4','nombre_depa'=>'RRHH','descrip_depa'=>'RRHH'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamentos');
    }
}

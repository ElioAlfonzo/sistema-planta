<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacostos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_costo', 50)->unique();
            $table->string('descrip_costo', 256)->nullable();
            $table->boolean('estado_costo')->default(1);
            $table->timestamps();
        });
        DB::table('formacostos')->insert(array('id' => '1','nombre_costo'=>'Mts2','descrip_costo'=>'Mts2'));
        DB::table('formacostos')->insert(array('id' => '2','nombre_costo'=>'Hrs','descrip_costo'=>'Horas de Trabajo'));
        DB::table('formacostos')->insert(array('id' => '3','nombre_costo'=>'Toneladas','descrip_costo'=>'Toneladas'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacostos');
    }
}

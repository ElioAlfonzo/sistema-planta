<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factu_pagos', function (Blueprint $table) {
            $table->increments('id');

            //Id de la Factura
            $table->integer('id_fac')->unsigned();
            $table->foreign('id_fac')->references('id')->on('facturaciones');

            $table->date('fecha_pago');

            $table->integer('tipo');
            // 1 Efectivo
            // 2 Credito
            // 3 Transferencia

            $table->decimal('monto_pago', 11, 2);

            //Usuarios
            $table->integer('idusuario')->unsigned();
            $table->foreign('idusuario')->references('id')->on('users');
            //

            $table->boolean('estado_pago')->default(1);  //este es para eliminarlo o no

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factu_pagos');
    }
}

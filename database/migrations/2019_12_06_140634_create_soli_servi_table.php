<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoliServiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_servi', function (Blueprint $table) {
            $table->increments('id');
            
            //Servicios
            $table->integer('id_servi')->unsigned()->nullable();
            $table->foreign('id_servi')->references('id')->on('servicios');

            //Solicitud
            $table->integer('id_soli')->unsigned()->nullable();
            $table->foreign('id_soli')->references('id')->on('solicitudes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soli_servi');
    }
}

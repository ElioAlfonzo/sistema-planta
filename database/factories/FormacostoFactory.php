<?php

use App\Models\Formacosto;
use Faker\Generator as Faker;

$factory->define(Formacosto::class, function (Faker $faker) {
    return [
        'nombre_costo' => $faker->word
    ];
});

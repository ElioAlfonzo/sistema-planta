<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = 'zona';
    protected $fillable = ['nombre_zona','descrip_zona','estado_zona'];
    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}

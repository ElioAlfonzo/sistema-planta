<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    public $table = "cuentas";
    protected $fillable = [
        'nombre_cuenta',
        'empresa_id',
        'tipo_cuenta',
        'banco_id',
        'num_cuenta',
        'total'
    ];  
    
    // public $timestamps = false;

    public function empresas()
    {
        return $this->hasMany('App\Models\Empresa');
    }
}

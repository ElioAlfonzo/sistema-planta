<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asignacion_Servi extends Model
{
    public $table = "asignaciones_servi";
    protected $fillable = [
        'asig_id',
        'servi_id',
        'forma_id',
        'cantidad',
        'valor_uni',
        'valor_neto'
    ];

    public function presupuestos()
    {
        return $this->hasMany('App\Models\Presupuesto_Servi');
    }
    public function formacostos()
    {
        return $this->hasMany('App\Models\Formacosto');
    }
}

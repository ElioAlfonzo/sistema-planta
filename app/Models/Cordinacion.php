<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Cordinacion extends Model
{
    public $table = "cordinaciones";
    protected $fillable = [
        'folio_cordi',
        'fecha',
        'hora',
        'fecha_fin',
        'hora_fin',
        'razon',
        'observacion_cordi',
        'estado_cordi',
        'soli_id',
        'pre_id',
        'asig_id',
        'folio_tipo',
        'respuesta_cordi',
        'tipo'
    ];   
    
    public function solicitud()
    {
        return $this->belongsTo('App\Models\Solicitud');
    }

    public function presupuesto()
    {
        return $this->belongsTo('App\Models\Presupuesto');
    }

    public function empresas()
    {
        return $this->hasMany('App\Models\Empresa');
    }
}

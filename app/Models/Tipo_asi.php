<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo_asi extends Model
{
    protected $table = 'tipo_asi';

    protected $fillable = ['id','nombre_tipo'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factu_Pago extends Model
{
    public $table = "factu_pagos";
    protected $fillable = [
        'id_fac',
        'fecha_pago',
        'monto_pago',
        'estado_pago',
        'idusuario',
        'tipo'
    ];  
}

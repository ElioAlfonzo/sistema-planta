<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hora_extra extends Model
{
    public $table = "horas_extras";

    protected $fillable = [
        'id_traba', //voy almacenar los id de tabajadores en empresas
        'fecha',
        'horas',
        'observa_horas',
        'idusuario',
        'idzona',
        'estado_hora'
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }
}

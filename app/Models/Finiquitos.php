<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finiquitos extends Model
{
    public $table = "finiquitos";
    protected $fillable = [
        'num_contrato',
        'fecha_finiquito',
        'causal',
        'estadofiniquito',
        'id_traem',
        'estado_status',
        'idusuario'
    ];

    public function traba_empre()
    {
        return $this->belongsTo('App\Models\Traba_empre');
    }
}

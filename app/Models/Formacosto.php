<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formacosto extends Model
{
    public $table ="formacostos";

    protected $fillable = [
        'nombre_costo',
        'descrip_costo',
        'estado_costo'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    public $table = "departamentos";
    protected $fillable = [
        'nombre_depa',
        'descrip_depa',
        'estado_depa'
    ];

    public function traba_empre()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }
}

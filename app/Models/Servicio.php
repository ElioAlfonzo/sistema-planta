<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    public $table = "servicios"; 
    protected $fillable = [
        'nomb_servi',
        'descrip_servi',
        'estado_servi',
    ];
    public function servicios_camiones()
    {
        return $this->hasMany('App\Models\Servi_Camion');
    }
}

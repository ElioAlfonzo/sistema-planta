<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contratos_tra extends Model
{
    public $table = "contratos_tra";
    protected $fillable = [
        'num_contrato',
        'jornada',
        'fecha_ini',
        'fecha_fin',
        'tipo_contrato',
        'estado_contratos',
        'id_traem',
        'trabajador_id',
        'idusuario'
    ];

    public function trabajadores()
    {
        return $this->belongsTo('App\Models\Trabajador');
    }
}

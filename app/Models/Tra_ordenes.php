<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tra_ordenes extends Model
{
    public $table = "tra_ordenes";

    protected $fillable = [
        'orden_id',
        'servi_id',
        'forma_id',
        'fecha_ot',
        'hora_ot',
        'cantidad',
        'valor_uni',
        'valor_neto',
        'pago_orden',
        'cual_espago',
        'estado'

    ];
}

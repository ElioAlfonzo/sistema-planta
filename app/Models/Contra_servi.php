<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contra_servi extends Model
{
    public $table = "contra_servi";
    protected $fillable = [
        'id_con',
        'id_servi',
        'id_formac',
        'id_cli',
        'precio',
        'estado_contraservi',
        'idusuario'
    ];

    public function clientes()
    {
        return $this->hasMany('App\Models\Clientempresa');
    }
}

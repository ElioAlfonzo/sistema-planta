<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    public $table = "contratos";
    protected $fillable = [
        'folio_contra',
        'nombre_contra',
        'fecha_contra',
        'id_cli',
        'id_empre',
        'estado_contra'
    ];

    public function clientes()
    {
        return $this->hasMany('App\Models\Clientempresa');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    public $table = "creditos";
    protected $fillable = [
        'id_cuenta',
        'nombre',
        'descripcion',
        'monto',
    ];  
    
    public $timestamps = false;

    public function cuentas()
    {
        return $this->hasMany('App\Models\Cuenta');
    }
}

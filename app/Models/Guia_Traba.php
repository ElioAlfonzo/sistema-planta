<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guia_Traba extends Model
{
    public $table = "guia_traba";

    protected $fillable = [
        'guia_id',
        'servi_id',
        'forma_id',
        'fecha_guia',
        'hora_guia',
        'cantidad',
        'valor_uni',
        'valor_neto',
        'pago_traba',
        'cual_espago',
        'estado_traba'
    ];
}

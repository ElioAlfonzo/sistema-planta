<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    public $table = "trabajadores";
    protected $fillable = [
        'nombre_tra',
        'apellido_tra',
        'rut',
        'fecha_nacitra',
        'afp',
        'salud',
        'telefono',
        'tel_adicional',
        'correo_tra',
        'direccion_tra',
        'nacionalidad',
        'estado_civil',
        'numero_cargas',
        'estado_tra'
    ];

    public function traba_empresas()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }

    public function contratos()
    {
        return $this->hasMany('App\Models\Contratos_tra');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
    
}

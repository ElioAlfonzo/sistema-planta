<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Traba_empre extends Model
{
    public $table = "traba_empresas";
    protected $fillable = [
        'num_contrato',
        'rut',
        'trabajador_id',
        'empresa_id',
        'idzona',
        'departamento_id',
        'tipo_contrato',
        'cuenta',
        'gratificacion',
        'movilizacion',
        'colacion',
        'archivos',
        'bonos',
        'banco',
        'remuneracion',
        'estado_tra_em',
        'fecha_ini',
        'fecha_fin',
        'fecha_ci',
        'fecha_lic',
        'jornada',
        'tipo_contrato',
        'estado_trabajo',
        'idusuario'
    ];

    public function despartamentos()
    {
        return $this->belongsTo('App\Models\Departamento');
    }

    public function empresas()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function trabajadores()
    {
        return $this->belongsTo('App\Models\Trabajador');
    }
}

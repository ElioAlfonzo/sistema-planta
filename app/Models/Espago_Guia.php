<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Espago_Guia extends Model
{
    public $table = "espago_guias";

    protected $fillable = [
        'folio_epg',
        'fechaini',
        'fechafin',
        'fechaorigen',
        'esguia_neto',
        'esguia_iva',
        'esguia_total',
        'stock_total',
        'id_cliente',
        'id_contrato',
        'id_empresa',
        'observa_espa',
        'respu_espa',
        'estado_pago',
        'estado_espa',
        'tipo'
    ];
}

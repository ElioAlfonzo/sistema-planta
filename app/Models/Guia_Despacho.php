<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guia_Despacho extends Model
{
    public $table ="guia_despachos";

    protected $fillable = [
        'folio_guia',
        'fecha_guia',
        'hora_guia',
        'id_empre',
        'id_cli',
        'id_soli',
        'id_asig',
        'id_coordi',
        'observa_guia',
        'estado_guia',
        'respuesta_gui',
        'monto_guia',
        'iva_guia',
        'total_guia',
        'id_contrato',
        'tipo'
    ];
}

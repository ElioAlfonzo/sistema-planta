<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turno_extra extends Model
{
    public $table = "turnos_extras";

    protected $fillable = [
        'id_traba', //voy almacenar los id de tabajadores en empresas
        'fecha_tur',
        'hora_tur',
        'tipo_tur',
        'observa_tur',
        'idusuario',
        'idzona',
        'estado_tur'
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }
}

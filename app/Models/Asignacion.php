<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    public $table = "asignaciones";
    protected $fillable = [
        'folio_asi',
        'fecha_asi',
        'monto_asi',
        'iva_asi',
        'total_asi',
        'observacion_asi',
        'soli_asi',
        'idclientempre',
        'estado_asi',
        'respuesta_asi',
        'id_empre',
        'clicontratos'
    ];

    public function solicitudes()
    {
        return $this->belongsTo('App\Models\Solicitudes');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    public $table = "cheques";
    protected $fillable = [
        'id_cuenta',
        'n_cheque',
        'fecha_emi',
        'fecha_pago',
        'fecha_real',
        'monto_che',
        'glosa_che',
        'pro_che',
        'idusuario',
        'estado_che'
    ];
}

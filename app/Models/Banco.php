<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    public $table = "bancos";
    protected $fillable = [
        'nom_banco',
    ];
}

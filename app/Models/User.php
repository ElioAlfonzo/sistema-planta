<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'usuario', 'password','condicion','idrol','idzona','id_tra'
    ];

    public $timestamps = false;

    // este lo que hace es cultar cierto campos para que no se muestren
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol(){
        return $this->belongsTo('App\Models\Rol');
    }

    public function trabajador(){
        return $this->belongsTo('App\Models\Trabajador');
    }
}

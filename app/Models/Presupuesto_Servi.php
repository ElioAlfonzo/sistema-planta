<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presupuesto_Servi extends Model
{
    public $table = "presupuestos_servi";
    protected $fillable = [
        'presu_id',
        'servi_id',
        'forma_id',
        'cantidad',
        'valor_uni',
        'valor_neto'
    ];

    public function presupuestos()
    {
        return $this->hasMany('App\Models\Presupuesto_Servi');
    }
    public function formacostos()
    {
        return $this->hasMany('App\Models\Formacosto');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    public $table = "solicitudes";
    protected $fillable = [
        'folio_soli',
        'fecha_soli',
        'ciudad',
        'direccion_soli',
        'alias_direccion',
        'idclientempre',
        'estado_soli',
        'observacion',
        'contrato',
        'desplaza',
        'respuesta_soli'

    ];


    public function empresas()
    {
        return $this->belongsTo('App\Models\Clientempresa');
    }
}

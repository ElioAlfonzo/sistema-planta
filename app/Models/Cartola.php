<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cartola extends Model
{
    public $table = "cartolas";
    protected $fillable = [
        'fecha',
        'origen',
        'numero',
        'cuenta_id',
        'tipo',
        'ingresos',
        'egresos'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Orden_tra extends Model
{
    public $table ="ordenes_tra";

    protected $fillable = [
        'folio_ot',
        'fecha_ot',
        'hora_ot',
        'id_empre',
        'id_cli',
        'id_soli',
        'id_presu',
        'id_coordi',
        'observac_ot',
        'descrip_ot',
        'respuesta_ot',
        'monto_ot',
        'iva_ot',
        'total_ot',
        'pago_orden',
        'cual_espago',
        'tipo'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Espago_Ot extends Model
{
    public $table = "espago_tra";

    protected $fillable = [
        'folio_ept',
        'fechaini',
        'fechafin',
        'fechaorigen',
        'pagotra_neto',
        'pagotra_iva',
        'pagotra_total',
        'id_cliente',
        'id_empresa',
        'observa_tra',
        'respuesta_tra',
        'estado',
    ];
}

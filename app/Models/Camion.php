<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Camion extends Model
{
    public $table = "camiones";
    protected $fillable = [
        'codigo_camion',
        'ppu',
        'anio',
        'marca',
        'km',
        'color',
        'foto_camion',
        'estado_camion',
        'empresa',
        'idusuario',
        'idzona'
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function servicios_camiones()
    {
        return $this->hasMany('App\Models\Servi_Camion');
    }

}

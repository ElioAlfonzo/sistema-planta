<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitudservi extends Model
{
    public $table = "solicitud_servi";
    protected $fillable = [
        'id_servi',
        'id_soli'
    ];

    public function servicios()
    {
        return $this->hasMany('App\Models\Servicio');
    }
    
    public function solicitudes()
    {
        return $this->hasMany('App\Models\Solicitud');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    public $table = "facturaciones";
    protected $fillable = [
        'folio_fact',
        'fechaemi',
        'fechavenci',
        'id_empre',
        'id_cliente',
        'monto',
        'monto_actual',
        'glosa',
        'estado_fact',
        'idusuario',
        'tipo_factu',
        'tipo_contra'
    ];  
}

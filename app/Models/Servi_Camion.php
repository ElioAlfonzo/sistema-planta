<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servi_Camion extends Model
{
    public $table ="servi_camiones";
    protected $fillable = [
        'servicios',
        'empresas',
        'estado',
        'nombre'
    ];

    public function camiones()
    {
        return $this->belongsTo('App\Models\Camion');
    }

    public function servicios()
    {
        return $this->belongsTo('App\Models\Servicio');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factu_Detalle extends Model
{
    public $table = "factu_detalles";
    protected $fillable = [
        'id_factu',
        'pago_ot',
        'pago_guia',
        'tipo_pago',
        'estado_deta',
    ];  
}

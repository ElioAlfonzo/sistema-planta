<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cordi_Cami extends Model
{
    public $table = "cordi_cami";
    protected $fillable = [
        'ppu',
        'id_cami',
        'id_cordi',
        'estado_cordi_cami'
    ];

    public function camiones()
    {
        return $this->hasMany('App\Models\Camion');
    }

    public function cordinaciones()
    {
        return $this->hasMany('App\Models\Cordinacion');
    }
}

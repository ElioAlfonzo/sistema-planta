<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    public $table = "presupuestos";
    protected $fillable = [
        'folio_pre',
        'fecha_pre',
        'monto_pre',
        'iva_pre',
        'total_pre',
        'observacion_pre',
        'soli_id',
        'idclientempre',
        'estado_presu',
        'respuesta_presu',
        'id_empre'
    ];

    public function solicitudes()
    {
        return $this->belongsTo('App\Models\Solicitudes');
    }
}

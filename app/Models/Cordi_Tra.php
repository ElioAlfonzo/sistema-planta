<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cordi_Tra extends Model
{
    public $table = "cordi_tra";
    protected $fillable = [
        'nombre_tra',
        'id_tra',
        'id_cordi',
        'id_depa',
        'chofer',
        'estado_cordi_tra'
    ];

    public function trabajadores()
    {
        return $this->hasMany('App\Models\trabajadores');
    }

    public function cordinaciones()
    {
        return $this->hasMany('App\Models\Cordinacion');
    }
}

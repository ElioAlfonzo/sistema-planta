<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientempresa extends Model
{
    public $table = "clientempresas";
    protected $fillable = [
        'rut',
        'razon',
        'direcion_em',
        'tel_em',
        'nombre_contac',
        'correo_contac',
        'tel_contac',
        'tipocli',
        'estado_em',
        'contrato',
        'idusuario',
        'idzona'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    public $table = "empresas";
    protected $fillable = [
        'codigo_em',
        'rut_em',
        'telefono_em',
        'correo_em',
        'direccion_em',
        'giro',
        'nombre_em',
        'descrip_em',
        'descrip_dos',
        'estado_em',
        'empre_image'
    ];    
    
    public $timestamps = false;

    public function traba_empresas()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }
    public function camiones()
    {
        return $this->hasMany('App\Models\Camion');
    }
}

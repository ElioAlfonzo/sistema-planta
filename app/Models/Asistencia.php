<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    public $table = "asistencias";

    protected $fillable = [
        'id_traba', //voy almacenar los id de tabajadores en empresas
        'fecha_asi',
        'id_tra',
        'hora_asi',
        'tipo',
        'observa_asi',
        'idusuario',
        'idzona',
        'estado_asi'
    ];

    public function asistencias_tipos()
    {
        return $this->hasMany('App\Models\Tipo_asi');
    }

    public function trabajadores()
    {
        return $this->hasMany('App\Models\Traba_empre');
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Camion;
use App\Models\Servi_Camion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CamionController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
            $camiones=Camion::join('users', 'users.id', '=', 'camiones.idusuario')
            ->join('zona', 'zona.id', '=', 'camiones.idzona')   
            ->join('empresas', 'empresas.id', '=', 'camiones.empresa')
            ->select('camiones.id','camiones.codigo_camion','ppu','anio','marca','km','color','foto_camion','empresas.nombre_em','zona.nombre_zona',
            'estado_camion','camiones.empresa','camiones.idzona')    
            ->where('estado_camion','=','1')->get();
        return [
            'camiones' => $camiones
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            DB::beginTransaction();

            $camiones = new Camion();
            $camiones->codigo_camion = $request->codigo;
            $camiones->ppu = $request->ppu;
            $camiones->anio = $request->anio;
            $camiones->marca = $request->marca;
            $camiones->km = $request->km;
            $camiones->color = $request->color;
            $camiones->foto_camion = $request->foto_camion;
            $camiones->empresa = $request->empresa;
            $camiones->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $camiones->idzona = $request->idzona;
            $camiones->save();

            $arrayServi = $request->data;//Array de Servicios

            foreach($arrayServi as $ep=>$deta){

                $servi = new Servi_Camion();
                $servi->servicios = $deta['id'];
                $servi->nombre = $deta['nomb_servi'];
                $servi->camiones = $camiones->id;
                $servi->save();
            }

            DB::commit();

        }catch (Exception $e){
            DB::rollback();
        }
        
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $camion = Camion::findOrFail($request->id);//ojo
        $camion->ppu = $request->ppu;
        $camion->anio = $request->anio;
        $camion->marca = $request->marca;
        $camion->km = $request->km;
        $camion->color = $request->color;
        $camion->foto_camion = $request->foto;
        $camion->empresa = $request->empresa;
        $camion->idzona = $request->idzona;
        $camion->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $camion = Camion::findOrFail($request->id);
        $camion->estado_camion = '0';
        $camion->save();
    }

    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Camion::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Contratos_tra;
use App\Models\Traba_empre;
use App\Models\Trabajador;
use App\Models\Finiquitos;
use App\Models\Banco;
use Carbon\Carbon; //para la hora actual
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');        
            $trabajadores=Trabajador::where('estado_tra', '=', '1')->get();
            
        return [
            'trabajadores' => $trabajadores
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $trabajador = new Trabajador();
        $trabajador->nombre_tra = $request->nombre;
        $trabajador->apellido_tra = $request->apellido;
        $trabajador->rut = $request->rut;
        $trabajador->fecha_nacitra = $request->nacimiento;
        $trabajador->telefono = $request->telefono;
        $trabajador->tel_adicional = $request->tel_adicional;
        $trabajador->afp = $request->afp;
        $trabajador->salud = $request->salud;
        $trabajador->correo_tra = $request->correo;
        $trabajador->direccion_tra = $request->direccion;
        $trabajador->nacionalidad = $request->nacionalidad;
        $trabajador->estado_civil = $request->estado_civil;
        $trabajador->numero_cargas = $request->numero_cargas;
        $trabajador->save();
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $trabajador = Trabajador::findOrFail($request->id);//ojo
        $trabajador->nombre_tra = $request->nombre;
        $trabajador->apellido_tra = $request->apellido;
        $trabajador->rut = $request->rut;
        $trabajador->fecha_nacitra = $request->nacimiento;
        $trabajador->telefono = $request->telefono;
        $trabajador->tel_adicional = $request->tel_adicional;
        $trabajador->afp = $request->afp;
        $trabajador->salud = $request->salud;
        $trabajador->correo_tra = $request->correo;
        $trabajador->direccion_tra = $request->direccion;
        $trabajador->nacionalidad = $request->nacionalidad;
        $trabajador->estado_civil = $request->estado_civil;
        $trabajador->numero_cargas = $request->numero_cargas;
        $trabajador->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $trabajador = Trabajador::findOrFail($request->id);
        $trabajador->estado_tra = '0';
        $trabajador->save();
    }
    
    //FUNCIONES CONSULTAS PARA CORDINACIONES
    
    public function indexTraCordi(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $trabajadores=Traba_empre::join('trabajadores','trabajadores.id','=','traba_empresas.trabajador_id')
        ->join('departamentos','departamentos.id','=','traba_empresas.departamento_id')
        ->select('trabajadores.id','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut','departamentos.nombre_depa',
        'traba_empresas.departamento_id as depa')
        ->where('traba_empresas.departamento_id','<','3')
        ->where('trabajadores.estado_tra','=','1')->get();
        
        // ->where('traba_empresas.departamento_id','=','1' )->get();

        return [
            'trabajadores' => $trabajadores
        ];
    }
    // 

    
    

    // FUNCIONES DE CONTRATOS //
    public function selectTraba(Request $request){
        
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;//guardamos el filtro q viene por request

        $trabajador=Trabajador::select('id','nombre_tra','apellido_tra','rut')
        ->where('nombre_tra', 'like' , '%' . $filtro . '%' )
        ->where('estado_tra', '=', '1')
        ->orderby('id','asc')->get();

        return ['trabajadores' => $trabajador];
        // where('nombre', 'like' , '%' . $filtro . '%' )
        // ->orwhere('id_cod_persona', 'like' , '%' . $filtro . '%' )
    }

    // Funcion usada en el registrar usuarios
    public function selectTrabaUsu(Request $request){
        
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;//guardamos el filtro q viene por request
        $trabajador=Trabajador::join('traba_empresas', 'traba_empresas.trabajador_id', '=', 'trabajadores.id')
        ->join('departamentos', 'departamentos.id', '=', 'traba_empresas.departamento_id')
        ->select('trabajadores.id','nombre_tra','apellido_tra','trabajadores.rut','departamentos.id as depa')
        ->where('nombre_tra', 'like' , '%' . $filtro . '%' )
        ->where('estado_tra', '=', '1')
        ->orderby('trabajadores.id','asc')->get();

        return ['trabajadores' => $trabajador];
        // where('nombre', 'like' , '%' . $filtro . '%' )
        // ->orwhere('id_cod_persona', 'like' , '%' . $filtro . '%' )
    }


    public function codcontrato(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Contratos_tra::all();
            return [
                'codigo' => $codigo->last()
            ];
    }
    
    public function contratos(Request $request)
    {
        // $fecha_actual = $request->$buscar;

        $actual = Carbon::now();
        $dia = $actual->toDateString();

        if(!$request->ajax()) return redirect('/');
        
            $contratos=Traba_empre::join('trabajadores','trabajador_id','=', 'trabajadores.id')
            ->join('departamentos','departamento_id','=', 'departamentos.id')
            ->join('empresas','empresa_id','=', 'empresas.id')
            ->join('zona', 'zona.id', '=', 'traba_empresas.idzona')
            ->select('traba_empresas.id','traba_empresas.num_contrato','trabajadores.rut','trabajadores.nombre_tra','trabajadores.apellido_tra',
            'traba_empresas.fecha_ini','traba_empresas.fecha_fin','traba_empresas.remuneracion','traba_empresas.departamento_id','departamentos.nombre_depa',
            'traba_empresas.empresa_id','empresas.nombre_em','traba_empresas.estado_trabajo',
            'traba_empresas.jornada','traba_empresas.cuenta','traba_empresas.banco','traba_empresas.gratificacion',
            'traba_empresas.movilizacion','traba_empresas.colacion','traba_empresas.bonos','traba_empresas.archivos','traba_empresas.fecha_ci','traba_empresas.fecha_lic',
            'traba_empresas.tipo_contrato','traba_empresas.idzona','zona.nombre_zona','traba_empresas.trabajador_id')
            ->where('estado_tra_em', '=', '1')->get();


            $vencidos = Traba_empre::select('traba_empresas.fecha_fin')
            ->where('estado_tra_em', '=', '1')->get();

            $bancos = Banco::select('id','nom_banco')->get();
            

        return [
            'contratos' => $contratos,
            'carbon' => $dia,
            'vencidos' => $vencidos,
            'bancos' => $bancos
        ];
    }

    
    public function storecontrato(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        try{

            DB::beginTransaction();
            $contrato = new Traba_empre();
            $contrato->num_contrato = $request->num_contra;
            $contrato->rut = $request->rut;
            $contrato->trabajador_id = $request->trabajador_id;
            $contrato->empresa_id = $request->empresa_id;
            $contrato->idzona = $request->idzona;
            $contrato->departamento_id = $request->cargo;
            $contrato->cuenta = $request->cuenta;
            $contrato->gratificacion = $request->gratificacion;
            $contrato->movilizacion = $request->movilizacion;
            $contrato->colacion = $request->colacion;
            $contrato->archivos = $request->archivos;
            $contrato->bonos = $request->bonos;
            $contrato->banco = $request->banco;
            $contrato->remuneracion = $request->remuneracion;
            $contrato->fecha_ini = $request->fecha_i;
            $contrato->fecha_fin = $request->fecha_f;
            $contrato->fecha_lic = $request->fecha_lic;
            $contrato->fecha_ci = $request->fecha_ci;
            $contrato->jornada = $request->jornada;
            $contrato->tipo_contrato = $request->tipo_contra;
            $contrato->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $contrato->save();

            $contratos_tra = new Contratos_tra();
            $contratos_tra->id_traem = $contrato->id;
            $contratos_tra->trabajador_id = $request->trabajador_id;
            $contratos_tra->num_contrato = $request->num_contra;
            $contratos_tra->fecha_ini = $request->fecha_i;
            $contratos_tra->fecha_fin = $request->fecha_f;
            $contratos_tra->jornada = $request->jornada;
            $contratos_tra->tipo_contrato = $request->tipo_contra;
            $contratos_tra->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $contratos_tra->save();
        
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
        }
    }

    public function upcontrato(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $contrato = Traba_empre::findOrFail($request->id);//ojo
        $contrato->empresa_id = $request->empresa_id;
        $contrato->idzona = $request->idzona;
        $contrato->departamento_id = $request->cargo;
        $contrato->cuenta = $request->cuenta;
        $contrato->gratificacion = $request->gratificacion;
        $contrato->movilizacion = $request->movilizacion;
        $contrato->colacion = $request->colacion;
        $contrato->archivos = $request->archivos;
        $contrato->bonos = $request->bonos;
        $contrato->banco = $request->banco;
        $contrato->remuneracion = $request->remuneracion;
        $contrato->fecha_ini = $request->fecha_i;
        $contrato->fecha_fin = $request->fecha_f;
        $contrato->fecha_ci = $request->fecha_ci;
        $contrato->fecha_lic = $request->fecha_lic;
        $contrato->jornada = $request->jornada;
        $contrato->save();
    }

    public function descontrato()
    {
        //
    }

    public function vercontratos(Request $request)
    {
      if(!$request->ajax()) return redirect('/');   
        $id = $request->id;
        $contratos = Contratos_tra::select('contratos_tra.id','num_contrato','fecha_ini','fecha_fin','tipo_contrato')
        ->where('id_traem', '=', $id)
        ->orderBy('num_contrato','desc')->get();

        return [
            'contratos' => $contratos
        ];

    }

    public function vercontratos2(Request $request)
    {

      if(!$request->ajax()) return redirect('/');   
        $id = $request->id;
        $fe1 = $request->fecha1;
        $fe2 = $request->fecha2;
        $contrato=0;

        $contratos_ini = Contratos_tra::select('contratos_tra.id','contratos_tra.num_contrato','contratos_tra.fecha_ini','contratos_tra.fecha_fin','contratos_tra.tipo_contrato')
        ->where('contratos_tra.trabajador_id', '=', $id)
        ->where('contratos_tra.fecha_ini','<=', $fe1)
        ->where('contratos_tra.fecha_fin','>=', $fe1)->count();

        if($contratos_ini > 0){

            $contrato = 1;
        }

        $contratos_fin = Contratos_tra::select('contratos_tra.id','contratos_tra.num_contrato','contratos_tra.fecha_ini','contratos_tra.fecha_fin','contratos_tra.tipo_contrato')
        ->where('contratos_tra.trabajador_id', '=', $id)
        ->where('contratos_tra.fecha_ini','<=', $fe2)
        ->where('contratos_tra.fecha_fin','>=', $fe2)->count();

        if($contratos_fin > 0){

            $contrato = 1;
        }

        $contratos_ini_fin = Contratos_tra::select('contratos_tra.num_contrato','contratos_tra.fecha_ini','contratos_tra.fecha_fin','contratos_tra.tipo_contrato')
        ->where('contratos_tra.trabajador_id', '=', $id)
        ->where('contratos_tra.fecha_ini','>=', $fe1)
        ->where('contratos_tra.fecha_fin','<=', $fe2)->count();

        if($contratos_fin > 0){

            $contrato = 1;
        }

        return $contrato;

    }

    public function verfiniquito(Request $request)
    {
      if(!$request->ajax()) return redirect('/');   
        $id = $request->id;
        // join('traba_empresas', 'traba_empresas.trabajador_id', '=', 'trabajadores.id')
        $contratos = Finiquitos::select('num_contrato','fecha_finiquito','causal','estadofiniquito')
        ->where('id_traem', '=', $id)
        ->orderBy('num_contrato','desc')->get();

        return  $contratos;
        

    }

    public function anexar(Request $request){

        if(!$request->ajax()) return redirect('/');
        $trabajador = new Contratos_tra();
        $trabajador->num_contrato = $request->num_contrato;
        $trabajador->jornada = $request->jornada;
        $trabajador->tipo_contrato = $request->tipo_contrato;
        $trabajador->fecha_ini = $request->fecha_ini;
        $trabajador->fecha_fin = $request->fecha_fin;
        $trabajador->id_traem = $request->id_traem;
        $trabajador->trabajador_id = $request->trabajador_id;

        $tra_em = Traba_empre::findOrFail($request->id_traem);
        $tra_em->tipo_contrato = $request->tipo_contrato;
        $tra_em->fecha_ini = $request->fecha_ini;
        $tra_em->fecha_fin = $request->fecha_fin;
        $tra_em->save();

        $trabajador->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
        $trabajador->save();

    }

    // FUNCIONES DE FINIQUITOS //
    public function indexfini(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
            $finiquitos=Finiquitos::join('traba_empresas', 'traba_empresas.id', '=', 'finiquitos.id_traem')
            ->join('trabajadores', 'trabajadores.id', '=', 'traba_empresas.trabajador_id')
            ->select('finiquitos.id','finiquitos.num_contrato','finiquitos.estadofiniquito','trabajadores.nombre_tra','trabajadores.apellido_tra',
            'finiquitos.fecha_finiquito','finiquitos.causal')
            ->where('estado_status', '=', '1')->get();

        return [
            'finiquitos' => $finiquitos
        ];
    }

    public function storefini(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

        $finiquito = new Finiquitos();
        $finiquito->num_contrato = $request->num_contrato;
        $finiquito->fecha_finiquito= $request->fecha_finiquito;
        $finiquito->causal = $request->causal;
        $finiquito->estadofiniquito = $request->estado_finiquito;
        $finiquito->id_traem = $request->id_traem;
        $finiquito->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
        $finiquito->save();
        
        // Para que el contrato ya este finquitado
        $traba = Traba_empre::findOrFail($request->id_traem);
        $traba->estado_trabajo='0';
        $traba->tipo_contrato='Finiquito';
            $traba->save();

        DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function updatefini(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $finiquito = Finiquito::findOrFail($request->id);//ojo
        $finiquito->num_contrato = $request->num_contrato;
        $finiquito->fecha_finiquito = $request->fecha_finiquito;
        $finiquito->causal = $request->causal;
        $finiquito->estado_finiquito = $request->estado_finiquito;
        $finiquito->save();
    }
}


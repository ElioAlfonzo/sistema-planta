<?php

namespace App\Http\Controllers;

// use App\Mail\SendMail;
// use Illuminate\Support\Facades\Mail;
use Mail; 

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function index() {
        return view ('email');
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required|email',
            'message' =>  'required'
            ]);
            
        $data = array(
                'name'      =>  $request->input('name'),
                'message'   =>   $request->input('message')
            );

            $email = $request->input('email');

        Mail::to($email)->send(new SendMail($data));

        return back()->with('success', 'Enviado exitosamente!');

    }

    public function contact(Request $request){
        $subject = "Asunto del correo";
        $for = "elioalfonzo6@gmail.com";
        Mail::send('email',$request->all(), function($msj) use($subject,$for){
            $msj->from("alfonzoelio7@gmail.com","NombreQueApareceráComoEmisor");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back();
    }

}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function __invoke(Request $request)
    {
        // aqui tomaremos el año actual del sistema
        $anio = date('Y');

        // Vamos a tomar el mes y el año del campo fecha_ot
        $ordenes = DB::table('ordenes_tra as or')
        ->select(DB::raw('MONTH(or.fecha_ot) as mes'),
        DB::raw('YEAR(or.fecha_ot) as anio'),
        DB::raw('SUM(or.total_ot) as numero'))
        ->whereYear('or.fecha_ot', $anio)
        ->groupBy( DB::raw('MONTH(or.fecha_ot)'), DB::raw('YEAR(or.fecha_ot)') )
        ->get();

        $guias = DB::table('guia_despachos as guia')
        ->select(DB::raw('MONTH(guia.fecha_guia) as mes'),
        DB::raw('YEAR(guia.fecha_guia) as anio'),
        DB::raw('SUM(guia.total_guia) as numero'))
        ->whereYear('guia.fecha_guia', $anio)
        ->groupBy( DB::raw('MONTH(guia.fecha_guia)'), DB::raw('YEAR(guia.fecha_guia)') )
        ->get();


        $pendi = DB::table('solicitudes as t') ->select(DB::raw('COUNT(t.respuesta_soli) as pendientes'))
        ->where('t.respuesta_soli','=','1')
        ->where('t.estado_soli','=','1')
        ->get();

        $ecofosa = DB::table('traba_empresas as t') ->select(DB::raw('COUNT(t.empresa_id) as ecofosa'))
        // ->where('t.respuesta_cordi','=','5')
        ->where('t.empresa_id','=','1')
        ->where('t.estado_tra_em','=','1')
        ->get();

        $indu = DB::table('traba_empresas as t') ->select(DB::raw('COUNT(t.empresa_id) as indu'))
        // ->where('t.respuesta_cordi','=','5')
        ->where('t.empresa_id','=','3')
        ->where('t.estado_tra_em','=','1')
        ->get();

        $eirl = DB::table('traba_empresas as t') ->select(DB::raw('COUNT(t.empresa_id) as eirl'))
        // ->where('t.respuesta_cordi','=','5')
        ->where('t.empresa_id','=','2')
        ->where('t.estado_tra_em','=','1')
        ->get();

        // Ordenes pendientes de cobrar
        $ordenpendi = DB::table('ordenes_tra as t') ->select(DB::raw('COUNT(t.respuesta_ot) as orpendi'))
        ->where('t.respuesta_ot','=','7') //pendiente de Facturar
        ->where('t.estado_orden','=','1')
        ->get();

        // Estado de Pago de Ots pendientes por cobrar
        $facturar = DB::table('espago_tra as t') ->select(DB::raw('COUNT(t.respuesta_tra) as estado_pen'))
        ->where('t.respuesta_tra','=','7') //pendiente de Facturar
        ->where('t.estado','=','1')
        ->get();
        
        // Estado de Pago de Guias pendientes por cobrar
        $facturar2 = DB::table('espago_guias as t') ->select(DB::raw('COUNT(t.respu_espa) as estado_pen'))
        ->where('t.respu_espa','=','13') //pendiente de Facturar
        ->where('t.estado_espa','=','1')
        ->get();
        
        return ['ordenes' => $ordenes, 'anio' => $anio, 'guias' => $guias, 'anio' => $anio, 'pendientes' => $pendi, 'ecofosa' => $ecofosa,
                'eirl'=> $eirl, 'indu' => $indu , 'estado_ot' => $facturar, 'estado_guia' => $facturar2];
    }
}
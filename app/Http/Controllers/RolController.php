<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\Zona;

class RolController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $roles=Rol::where('condicion', '=', '1')->get();

        return [
            'roles' => $roles
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $rol = new Role();
        $rol->nombre = $request->nombre;
        $rol->descripcion = $request->descripcion;
        $rol->save();
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $rol = Rol::findOrFail($request->id);//ojo
        $rol->nombre = $request->nombre;
        $rol->descripcion = $request->descripcion;
        $rol->condicion = '1';
        $rol->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $rol = Rol::findOrFail($request->id);
        $rol->condicion = '0';
        $rol->save();
    }

    public function indexzonas(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $zonas=Zona::where('estado_zona', '=', '1')->get();

        return [
            'zonas' => $zonas
        ];
    }

    public function storezona(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $zona = new Zona();
        $zona->nombre_zona = $request->nombre_zona;
        $zona->descrip_zona = $request->descrip_zona;
        $zona->save();
    }

    public function updatezona(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $zona = Zona::findOrFail($request->id);//ojo
        $zona->nombre_zona = $request->nombre_zona;
        $zona->descrip_zona = $request->descrip_zona;
        $zona->save();
    }

    public function destroyzona(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $zona = Zona::findOrFail($request->id);
        $zona->estado_zona = '0';
        $zona->save();
    }
}

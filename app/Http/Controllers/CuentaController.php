<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Cuenta;
use App\Models\Cartola;
use App\Models\Credito;
use App\Models\Banco;
use App\Models\Guia_Despacho;
use App\Models\Guia_Traba;
use App\Models\Orden_tra;
use App\Models\Tra_ordenes;
use App\Models\Solicitud;
use App\Models\Asignacion;
use App\Models\Presupuesto;
use App\Models\Cordinacion;
use App\Models\Factura;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;

class CuentaController extends Controller
{


    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $cuentas=Cuenta::join('empresas','empresas.id','=','cuentas.empresa_id')
        ->join('bancos','bancos.id','=','cuentas.banco_id')
        ->select('cuentas.id as idcuenta','nombre_cuenta','empresas.id as id_em','empresas.nombre_em','num_cuenta','total','tipo_cuenta','bancos.id as banco','nom_banco')
        ->get();
        // ->where('estado_presu', '=', 1)->get();

        $bancos = Banco::select('id','nom_banco')->get();

        return [
            'cuentas' => $cuentas,
            'bancos' => $bancos
        ];
    }

   
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $cuenta = new Cuenta();
            $cuenta->nombre_cuenta = $request->nombre_cuenta;
            $cuenta->empresa_id = $request->empresa_id;
            $cuenta->tipo_cuenta = $request->tipo_cuenta;
            $cuenta->banco_id = $request->banco_id;
            $cuenta->num_cuenta = $request->num_cuenta;
            $cuenta->total = 0;
            // $presupuesto->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $cuenta->save();

            $credi = $request->data;

            foreach($credi as $ep=>$det){
                $credito = new Credito();
                $credito->id_cuenta = $cuenta->id;
                $credito->nombre = $det['nombre'];
                $credito->descripcion = $det['descripcion'];
                $credito->monto = $det['monto'];
                $credito->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

   
    public function verCreditos(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
        $id = $request->id;
        $creditos = Credito::select('id','nombre','descripcion','monto')
        ->where('id_cuenta', '=', $id)->get();

            return [
                'creditos' => $creditos
            ];

    }

  
    public function cambiarMonto(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            $credi = Credito::findOrFail($request->id);//ojo
            $credi->monto = $request->monto;
            $credi->save();

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cuenta = Cuenta::findOrFail($request->id);//ojo
        $cuenta->empresa_id = $request->empresa_id;
        $cuenta->tipo_cuenta = $request->tipo_cuenta;
        $cuenta->banco_id = $request->banco_id;
        $cuenta->num_cuenta = $request->num_cuenta;
        $cuenta->save(); 
    }

    // Registrar Pagos
    public function pagoGuia(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();

            $cartola = new Cartola();
            $cartola->fecha = $request->fecha;
            $cartola->origen = $request->origen;
            $cartola->numero = $request->numero;
            $cartola->cuenta_id = $request->idcuenta;
            $cartola->tipo = $request->tipo;
            $cartola->ingresos = $request->ingresos;
            $cartola->save();

            $cuenta = Cuenta::findOrFail($request->idcuenta);
            $cuenta->total += $cartola->ingresos;
            $cuenta->save();

            // Cambiar de Estado de las Guias
            $guia = Guia_Despacho::findOrFail($request->idguia);
            $guia->respuesta_gui = 11;
            $guia->save();

            // Cambiar de Estado de las Solicitud
            $soli = Solicitud::findOrFail($request->idesoli);
            $soli->respuesta_soli = 11;
            $soli->save();

            // Cambiar de Estado de las Asignacion
            $asignacion = Asignacion::findOrFail($request->id_asig);
            $asignacion->respuesta_asi = 11;
            $asignacion->save();

            // Cambiar de Estado de las Coordinacion
            $coordinacion = Cordinacion::findOrFail($request->id_cordi);
            $coordinacion->respuesta_cordi = 11;
            $coordinacion->save();

            $trabajos = $request->data;

            foreach($trabajos as $ep => $det){
                $guiatra = Guia_Traba::findOrFail($det['guiatra_id']);
                $guiatra->pago_traba = 1;
                $guiatra->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function pagoOt(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();

            $cartola = new Cartola();
            $cartola->fecha = $request->fecha;
            $cartola->origen = $request->origen;
            $cartola->numero = $request->numero;
            $cartola->cuenta_id = $request->idcuenta;
            $cartola->tipo = $request->tipo;
            $cartola->ingresos = $request->ingresos;
            $cartola->save();

            $cuenta = Cuenta::findOrFail($request->idcuenta);
            $cuenta->total += $cartola->ingresos;
            $cuenta->save();

            // Cambiar de Estado de las Guias
            $orden = Orden_tra::findOrFail($request->id_orden);
            $orden->respuesta_ot = 11;
            $orden->save();

            // Cambiar de Estado de las Solicitud
            $soli = Solicitud::findOrFail($request->idesoli);
            $soli->respuesta_soli = 11;
            $soli->save();

            // Cambiar de Estado de las Asignacion
            $presupuesto = Presupuesto::findOrFail($request->id_pre);
            $presupuesto->respuesta_presu = 11;
            $presupuesto->save();

            // Cambiar de Estado de las Coordinacion
            $coordinacion = Cordinacion::findOrFail($request->id_cordi);
            $coordinacion->respuesta_cordi = 11;
            $coordinacion->save();

            $trabajos = $request->data;

            // foreach($trabajos as $ep => $det){
            //     $traorden = Tra_ordenes::findOrFail($det['traorden_id']);
            //     $traorden->pago_orden = 1;
            //     $traorden->save();
            // }


        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }
    //
    
    // Cartolas
    public function indexCartolas(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $cartolas=Cartola::join('cuentas','cuentas.id','=','cartolas.cuenta_id')
        ->select('cartolas.id','fecha','origen','numero','cuentas.nombre_cuenta','cuentas.num_cuenta','ingresos','egresos')->get();
        // ->where('estado_presu', '=', 1)->get();

        return [
            'cartolas' => $cartolas,
        ];
    }
    //

    
}

<?php

namespace App\Http\Controllers;
use App\Models\Espago_Guia;
use App\Models\Espago_Ot;

use App\Models\Tra_ordenes;
use App\Models\Presupuesto;
use App\Models\Orden_tra;
use App\Models\Guia_Despacho;
use App\Models\Guia_Traba;
use App\Models\Clientempresa;
use App\Models\Contrato;
use App\Models\Solicitud;
use App\Models\Asignacion;
use App\Models\Cordinacion;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon; //para la hora actual


class EstadoController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Espago_Guia::all();
            return [
                'codigo' => $codigo->last()
            ];
    }
    // Todo lo relacionado a Estados de Guias de Despacho
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $espagui=Espago_Guia::join('clientempresas','clientempresas.id','=','espago_guias.id_cliente')
            // ->join('contratos','contratos.id','=','espago_guias.id_contrato')
            // 'contratos.nombre_contra'
            ->join('empresas','empresas.id','=','espago_guias.id_empresa')
            ->join('estados_pro','estados_pro.id','=','espago_guias.respu_espa')
            ->join('zona','zona.id','=','clientempresas.idzona')

            ->select('espago_guias.id','folio_epg','fechaini','fechafin','fechaorigen','clientempresas.razon','empresas.nombre_em',
            'esguia_neto','esguia_iva','esguia_total','estados_pro.id as idestado','estados_pro.nombre_es','espago_guias.observa_espa','espago_guias.tipo',
            'id_cliente','id_contrato','id_empresa','zona.nombre_zona')
            ->where('estado_espa', '=', '1')
            ->orderBy('espago_guias.id','desc')->get();

        return [
            'espagui' => $espagui
        ];
    }

    public function storegui(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{
            DB::beginTransaction();

            $estado = new Espago_Guia();
            $estado->folio_epg = $request->folio_epg;
            $estado->fechaini = $request->fechaini;
            $estado->fechafin = $request->fechafin;
            $estado->fechaorigen = $request->fechaorigen;

            $estado->id_empresa = $request->id_empresa;
            $estado->id_cliente = $request->id_cliente;
            
            $estado->id_contrato = $request->id_contrato;
            $estado->tipo = $request->tipo;

            $estado->observa_espa = $request->observa_espa;
            $estado->esguia_neto = $request->esguia_neto;
            $estado->esguia_iva = $request->esguia_iva;
            $estado->esguia_total = $request->esguia_total;
            // $estado->stock_total = 0;

            // estado del proceso
            $estado->respu_espa = 9;
            $estado->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $estado->save();
            $servicios = $request->data;

            foreach($servicios as $ep=>$det){
                $servi = Guia_Despacho::findOrFail($det['id']);
                $servi->respuesta_gui = 9;
                $servi->save();

                $servi = Guia_Traba::findOrFail($det['traba']);
                $servi->pago_traba = 1;
                $servi->cual_espago = $estado->id;
                $servi->save();

                $orden = Solicitud::findOrFail($det['id_soli']);
                $orden->respuesta_soli = 9;
                $orden->save();
                
                if($request->tipo == 1){
                    $asig = Asignacion::findOrFail($det['id_asig']);
                    $asig->respuesta_asi = 9;
                    $asig->save();
                }else{
                    $pre = Presupuesto::findOrFail($det['pre_id']);
                    $pre->respuesta_presu = 9;
                    $pre->save();
                }

                $cordi = Cordinacion::findOrFail($det['id_coordi']);
                $cordi->respuesta_cordi = 9;
                $cordi->save();
            }

        DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function resumen(Request $request){
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');

        $idem = $request->idem;
        $fechaini = $request->fechaini;
        $fechafin = $request->fechafin;
        $idcontra = $request->idcontra;
        $idem2 = $request->idem2;

        $resumen = Guia_Traba::join('guia_despachos','guia_despachos.id','=','guia_traba.guia_id')
        ->join('formacostos','formacostos.id','=','guia_traba.forma_id')
        ->join('servicios','servicios.id','=','guia_traba.servi_id')
        ->join('solicitudes','solicitudes.id','=','guia_despachos.id_soli')
        ->join('cordinaciones','cordinaciones.id','=','guia_despachos.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','guia_despachos.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        ->join('cordi_tra','cordi_tra.id_cordi','=','guia_despachos.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')
        
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('guia_traba.pago_traba', '=', 0)
        ->where('guia_despachos.id_contrato', '=', $idcontra)
        ->where('guia_despachos.id_cli', '=', $idem)
        ->where('guia_despachos.id_empre', '=', $idem2)
        ->whereBetween('guia_despachos.fecha_guia',[$fechaini, $fechafin])

        ->select('guia_despachos.id','guia_traba.id as traba','guia_traba.fecha_guia','guia_despachos.folio_guia','servicios.nomb_servi','formacostos.nombre_costo',
        'guia_traba.valor_uni','guia_traba.valor_neto','guia_traba.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa','guia_despachos.id_soli','guia_despachos.id_asig','guia_despachos.pre_id','guia_despachos.id_coordi')
        ->orderBy('guia_traba.id','asc')->get();



         return [
             'resumen' => $resumen
         ];
    }

    public function mostrarPdf(Request $request) {
        $idem= $request->idem; //cliente
        $fechaini = $request->ini; //inicio
        $fechafin = $request->fin; //fin
        $empre = $request->empre; //empresa a cargo
        $contra = $request->contra; //idcontra id del contrato
        $folio = $request->folio; //Folio
        $fechao = $request->fechao; //Folio
        $id = $request->id; //id
        // Fecha de Realizacion
        $fecharea = 0;

        $resumen = Guia_Traba::join('guia_despachos','guia_despachos.id','=','guia_traba.guia_id')
        ->join('formacostos','formacostos.id','=','guia_traba.forma_id')
        ->join('servicios','servicios.id','=','guia_traba.servi_id')
        ->join('solicitudes','solicitudes.id','=','guia_despachos.id_soli')
        ->join('cordinaciones','cordinaciones.id','=','guia_despachos.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','guia_despachos.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        ->join('asignaciones','asignaciones.id','=','cordinaciones.asig_id')
        ->join('contratos','contratos.id','=','asignaciones.clicontratos')
        ->join('cordi_tra','cordi_tra.id_cordi','=','guia_despachos.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')

        ->where('guia_traba.cual_espago', '=', $folio)
        ->where('guia_traba.pago_traba', '=', 1)
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('guia_despachos.id_cli', '=', $idem)
        ->where('guia_despachos.id_empre', '=', $empre)
        ->where('guia_despachos.id_contrato', '=', $contra)

        ->whereBetween('guia_traba.fecha_guia',[$fechaini, $fechafin])
        ->select('guia_traba.fecha_guia','guia_despachos.folio_guia','servicios.nomb_servi','formacostos.nombre_costo',
        'guia_traba.valor_uni','guia_traba.valor_neto','guia_traba.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa')
        ->orderBy('guia_traba.fecha_guia','asc')->get();

        $nomcontra = Contrato::where('id_empre', '=', $empre) //empresa a cargo
        ->where('id_cli', '=', $idem)
        ->where('id', '=', $contra)
        ->select('contratos.nombre_contra','contratos.folio_contra')->get();

        $total = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_total) as total'))->get();

        $subtotal = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_neto) as monto' ))->get();

        
        $ivatotal = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_iva) as iva' ))->get();

        $tota = $total->sum('total');
        $subtota = $subtotal->sum('monto');
        $ivatota = $ivatotal->sum('iva');

        // Cliente
        $razon = Clientempresa::where('id', '=', $idem)->select('razon')->get();
        
        // Empresa a Cargo
        $empre = Empresa::where('id', '=', $empre)->select('rut_em','empre_image')->get();
        
        
        // Fechas
        setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon::parse($fechaini);
        $fecha->format("F"); // Inglés.
        $mes = ucfirst($fecha->formatLocalized('%B'));// mes en idioma español y ucfirst es para colocar la primera letra en mayuscula
        // 

        $pdf = \PDF::loadView('pdf.espago',['resumen'=>$resumen, 'razon'=>$razon,'nomcontra'=>$nomcontra,'tota' => $tota,'subtota' => $subtota,'ivatota' => $ivatota,
        'fechaini'=>$fechaini, 'fechafin'=>$fechafin,'mes'=>$mes, 'fecharea'=>$fecharea, 'empre'=>$empre, 'folio' =>$folio, 'fechao'=>$fechao ]);
        return $pdf->stream('Estado de Pago ES-PL-'.$folio.'.pdf');
        
        // return $pdf->stream('Guia D. N° -'.$numero_orden[0]->folio_guia.'.pdf');
    
    }

    public function mostrarPdf2(Request $request) {
        $idem= $request->idem; //cliente
        $fechaini = $request->ini; //inicio
        $fechafin = $request->fin; //fin
        $empre = $request->empre; //empresa a cargo
        $contra = $request->contra; //idcontra id del contrato
        $folio = $request->folio; //Folio
        $fechao = $request->fechao; //Folio
        $id = $request->id; //id
        // Fecha de Realizacion
        $fecharea = 0;

        $resumen = Guia_Traba::join('guia_despachos','guia_despachos.id','=','guia_traba.guia_id')
        ->join('formacostos','formacostos.id','=','guia_traba.forma_id')
        ->join('servicios','servicios.id','=','guia_traba.servi_id')
        ->join('solicitudes','solicitudes.id','=','guia_despachos.id_soli')
        ->join('cordinaciones','cordinaciones.id','=','guia_despachos.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','guia_despachos.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        ->join('cordi_tra','cordi_tra.id_cordi','=','guia_despachos.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')

        ->where('guia_traba.cual_espago', '=', $folio)
        ->where('guia_traba.pago_traba', '=', 1)
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('guia_despachos.id_cli', '=', $idem)
        ->where('guia_despachos.id_empre', '=', $empre)
        ->where('guia_despachos.id_contrato', '=', $contra)

        ->whereBetween('guia_traba.fecha_guia',[$fechaini, $fechafin])
        ->select('guia_traba.fecha_guia','guia_despachos.folio_guia','servicios.nomb_servi','formacostos.nombre_costo',
        'guia_traba.valor_uni','guia_traba.valor_neto','guia_traba.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa')
        ->orderBy('guia_traba.fecha_guia','asc')->get();

        $total = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_total) as total'))->get();

        $subtotal = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_neto) as monto' ))->get();

        
        $ivatotal = Espago_Guia::where('espago_guias.id', '=', $id)
        ->select(DB::raw ('SUM(esguia_iva) as iva' ))->get();

        $tota = $total->sum('total');
        $subtota = $subtotal->sum('monto');
        $ivatota = $ivatotal->sum('iva');

        // Cliente
        $razon = Clientempresa::where('id', '=', $idem)->select('razon')->get();
        
        // Empresa a Cargo
        $empre = Empresa::where('id', '=', $empre)->select('rut_em','empre_image')->get();
        
        
        // Fechas
        setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon::parse($fechaini);
        $fecha->format("F"); // Inglés.
        $mes = ucfirst($fecha->formatLocalized('%B'));// mes en idioma español y ucfirst es para colocar la primera letra en mayuscula
        // 

        $pdf = \PDF::loadView('pdf.espago2',['resumen'=>$resumen, 'razon'=>$razon,'tota' => $tota,'subtota' => $subtota,'ivatota' => $ivatota,
        'fechaini'=>$fechaini, 'fechafin'=>$fechafin,'mes'=>$mes, 'fecharea'=>$fecharea, 'empre'=>$empre, 'folio' =>$folio, 'fechao'=>$fechao ]);
        return $pdf->stream('Estado de Pago ES-PL-'.$folio.'.pdf');
        
        // return $pdf->stream('Guia D. N° -'.$numero_orden[0]->folio_guia.'.pdf');
    
    }

    // Estados de Pago de las Ordenes de Pago
    public function codigot(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Espago_Ot::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    // Todo lo relacionado Estado de Pago de Ordenes
    public function indexot(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $espagui=Espago_Ot::join('clientempresas','clientempresas.id','=','espago_tra.id_cliente')
            ->join('empresas','empresas.id','=','espago_tra.id_empresa')
            ->join('estados_pro','estados_pro.id','=','espago_tra.respuesta_tra')
            ->join('zona','zona.id','=','clientempresas.idzona')

            ->select('espago_tra.id','folio_ept','fechaini','fechafin','fechaorigen','clientempresas.razon','empresas.nombre_em',
            'pagotra_neto','pagotra_iva','pagotra_total','estados_pro.id as idestado','estados_pro.nombre_es','espago_tra.observa_tra','espago_tra.id_cliente',
            'espago_tra.id_empresa','tipo','id_cliente','id_contrato','zona.nombre_zona')
            ->where('espago_tra.estado', '=', '1')
            ->orderBy('espago_tra.id','desc')->get();

            return [
                'espagui' => $espagui
            ];
    }

    public function storeot(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
        try{
            DB::beginTransaction();
            // $mytime= Carbon::now('America/Santiago');
            $estado = new Espago_Ot();

            $estado->folio_ept = $request->folio_ept;
            $estado->fechaini = $request->fechaini;
            $estado->fechafin = $request->fechafin;
            $estado->fechaorigen = $request->fechaorigen;
            $estado->id_cliente = $request->id_cliente;
            $estado->id_empresa = $request->id_empresa;
            
            $estado->observa_tra = $request->observa_tra;

            $estado->id_contrato = $request->id_contrato;
            $estado->tipo = $request->tipo;

            $estado->pagotra_neto = $request->pagotra_neto;
            $estado->pagotra_iva = $request->pagotra_iva;
            $estado->pagotra_total = $request->pagotra_total;
            // estado del proceso
            $estado->respuesta_tra = 9;
            $estado->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $estado->save();

            $servicios = $request->data;

            foreach($servicios as $ep=>$det){
                $servi = Orden_tra::findOrFail($det['id']);
                $servi->respuesta_ot = 9;
                $servi->save();

                $servi = Tra_ordenes::findOrFail($det['idtra']);
                $servi->pago_orden = 1;
                $servi->cual_espago = $estado->id;
                $servi->save();

                $orden = Solicitud::findOrFail($det['id_soli']);
                $orden->respuesta_soli = 9;
                $orden->save();

                if($request->tipo == 1){
                    $asig = Asignacion::findOrFail($det['id_asig']);
                    $asig->respuesta_asi = 9;
                    $asig->save();
                }else{
                    $pre = Presupuesto::findOrFail($det['id_presu']);
                    $pre->respuesta_presu = 9;
                    $pre->save();
                }


                $cordi = Cordinacion::findOrFail($det['id_coordi']);
                $cordi->respuesta_cordi = 9;
                $cordi->save();
            }

        DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function resumenot(Request $request){
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');

        $idem = $request->idem;
        $fechaini = $request->fechaini;
        $fechafin = $request->fechafin;
        $idcontra = $request->idcontra;
        $idem2 = $request->idem2;

        $resumen = Tra_ordenes::join('ordenes_tra','ordenes_tra.id','=','tra_ordenes.orden_id')
        ->join('formacostos','formacostos.id','=','tra_ordenes.forma_id')
        ->join('servicios','servicios.id','=','tra_ordenes.servi_id')
        ->join('solicitudes','solicitudes.id','=','ordenes_tra.id_soli')
        ->join('cordinaciones','cordinaciones.id','=','ordenes_tra.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','ordenes_tra.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        
        ->join('cordi_tra','cordi_tra.id_cordi','=','ordenes_tra.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')
        
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('tra_ordenes.pago_orden', '=', 0)
        ->where('ordenes_tra.id_contrato', '=', $idcontra)
        ->where('ordenes_tra.id_cli', '=', $idem)
        ->where('ordenes_tra.id_empre', '=', $idem2)
        ->whereBetween('ordenes_tra.fecha_ot',[$fechaini, $fechafin])

        ->select('ordenes_tra.id','tra_ordenes.id as idtra','tra_ordenes.fecha_ot','ordenes_tra.folio_ot','servicios.nomb_servi','formacostos.nombre_costo',
        'tra_ordenes.valor_uni','tra_ordenes.valor_neto','tra_ordenes.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa','ordenes_tra.id_soli','ordenes_tra.id_coordi',
        'ordenes_tra.id_presu','ordenes_tra.id_asig')
        ->orderBy('tra_ordenes.id','asc')->get();

         return [
             'resumen' => $resumen
         ];
    }

    public function mostrarPdfot(Request $request) {
        $id= $request->id; //id
        $idem= $request->idem; //cliente
        $fechaini = $request->ini; //inicio
        $fechafin = $request->fin; //fin
        $empre = $request->empre; //empresa a cargo
        $contra = $request->contra; //idcontra id del contrato
        $folio = $request->folio; //Folio
        $fechao = $request->fechao; //Folio
        // Fecha de Realizacion
        $fecharea = 0;

        $resumen = Tra_ordenes::join('ordenes_tra','ordenes_tra.id','=','tra_ordenes.orden_id')
        ->join('formacostos','formacostos.id','=','tra_ordenes.forma_id')
        ->join('servicios','servicios.id','=','tra_ordenes.servi_id')
        ->join('solicitudes','solicitudes.id','=','ordenes_tra.id_soli')
        
        ->join('cordinaciones','cordinaciones.id','=','ordenes_tra.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','ordenes_tra.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        ->join('presupuestos','presupuestos.id','=','cordinaciones.pre_id')
        // ->join('contratos','contratos.id','=','asignaciones.clicontratos')
        ->join('cordi_tra','cordi_tra.id_cordi','=','ordenes_tra.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')

        ->where('tra_ordenes.pago_orden', '=', 1)
        ->where('tra_ordenes.cual_espago', '=', $id)
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('ordenes_tra.id_cli', '=', $idem)
        ->where('ordenes_tra.id_empre', '=', $empre)

        ->whereBetween('ordenes_tra.fecha_ot',[$fechaini, $fechafin])

        ->select('ordenes_tra.id','tra_ordenes.fecha_ot','ordenes_tra.folio_ot','servicios.nomb_servi','formacostos.nombre_costo',
        'tra_ordenes.valor_uni','tra_ordenes.valor_neto','tra_ordenes.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa')
        ->orderBy('tra_ordenes.fecha_ot','asc')->get();

        // $nomcontra = Contrato::where('id_empre', '=', $empre) //empresa a cargo
        // ->where('id_cli', '=', $idem)
        // ->where('id', '=', $contra)
        // ->select('contratos.nombre_contra','contratos.folio_contra')->get();

      
        $total = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_total) as total'))->get();

        $subtotal = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_neto) as monto' ))->get();

        
        $ivatotal = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_iva) as iva' ))->get();

        $tota = $total->sum('total');
        $subtota = $subtotal->sum('monto');
        $ivatota = $ivatotal->sum('iva');

        // Cliente
        $razon = Clientempresa::where('id', '=', $idem)->select('razon')->get();

        // Empresa a Cargo
        $empre = Empresa::where('id', '=', $empre)->select('rut_em','empre_image')->get();
        
        // Fechas
        setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon::parse($fechaini);
        $fecha->format("F"); // Inglés.
        $mes = ucfirst($fecha->formatLocalized('%B'));// mes en idioma español y ucfirst es para colocar la primera letra en mayuscula
        // 

        $pdf = \PDF::loadView('pdf.espagoot',['resumen'=>$resumen, 'razon'=>$razon,'tota' => $tota,'subtota' => $subtota,'ivatota' => $ivatota,
        'fechaini'=>$fechaini, 'fechafin'=>$fechafin,'mes'=>$mes, 'fecharea'=>$fecharea, 'empre'=>$empre, 'folio' =>$folio, 'fechao'=>$fechao ]);
        return $pdf->stream('Estado de Pago ES-PL-'.$folio.'.pdf');
        
    
    }

    public function mostrarPdfot2(Request $request) {
        $id= $request->id; //id
        $idem= $request->idem; //cliente
        $fechaini = $request->ini; //inicio
        $fechafin = $request->fin; //fin
        $empre = $request->empre; //empresa a cargo
        $contra = $request->contra; //idcontra id del contrato
        $folio = $request->folio; //Folio
        $fechao = $request->fechao; //Folio
        // Fecha de Realizacion
        $fecharea = 0;

        $resumen = Tra_ordenes::join('ordenes_tra','ordenes_tra.id','=','tra_ordenes.orden_id')
        ->join('formacostos','formacostos.id','=','tra_ordenes.forma_id')
        ->join('servicios','servicios.id','=','tra_ordenes.servi_id')
        ->join('solicitudes','solicitudes.id','=','ordenes_tra.id_soli')
        
        ->join('cordinaciones','cordinaciones.id','=','ordenes_tra.id_coordi')
        ->join('cordi_cami','cordi_cami.id','=','ordenes_tra.id_coordi')
        ->join('camiones','camiones.id','=','cordi_cami.id_cami')
        // ->join('presupuestos','presupuestos.id','=','cordinaciones.pre_id')
        ->join('asignaciones','asignaciones.id','=','cordinaciones.asig_id')
        ->join('contratos','contratos.id','=','asignaciones.clicontratos')
        ->join('cordi_tra','cordi_tra.id_cordi','=','ordenes_tra.id_coordi')
        ->join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')

        ->where('tra_ordenes.pago_orden', '=', 1)
        ->where('tra_ordenes.cual_espago', '=', $id)
        ->where('cordi_tra.id_depa', '=', 1)
        ->where('cordi_tra.chofer', '=', 1)
        ->where('ordenes_tra.id_cli', '=', $idem)
        ->where('ordenes_tra.id_empre', '=', $empre)

        ->whereBetween('ordenes_tra.fecha_ot',[$fechaini, $fechafin])

        ->select('ordenes_tra.id','tra_ordenes.fecha_ot','ordenes_tra.folio_ot','servicios.nomb_servi','formacostos.nombre_costo',
        'tra_ordenes.valor_uni','tra_ordenes.valor_neto','tra_ordenes.cantidad','solicitudes.direccion_soli','camiones.ppu',
        'trabajadores.nombre_tra','trabajadores.apellido_tra','cordi_tra.id_depa')
        ->orderBy('tra_ordenes.fecha_ot','asc')->get();

        $nomcontra = Contrato::where('id_empre', '=', $empre) //empresa a cargo
        ->where('id_cli', '=', $idem)
        ->where('id', '=', $contra)
        ->select('contratos.nombre_contra','contratos.folio_contra')->get();

      
        $total = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_total) as total'))->get();

        $subtotal = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_neto) as monto' ))->get();

        
        $ivatotal = Espago_OT::where('espago_tra.id', '=', $id)
        ->select(DB::raw ('SUM(pagotra_iva) as iva' ))->get();

        $tota = $total->sum('total');
        $subtota = $subtotal->sum('monto');
        $ivatota = $ivatotal->sum('iva');

        // Cliente
        $razon = Clientempresa::where('id', '=', $idem)->select('razon')->get();

        // Empresa a Cargo
        $empre = Empresa::where('id', '=', $empre)->select('rut_em','empre_image')->get();
        
        // Fechas
        setlocale(LC_ALL, 'es_ES');
        $fecha = Carbon::parse($fechaini);
        $fecha->format("F"); // Inglés.
        $mes = ucfirst($fecha->formatLocalized('%B'));// mes en idioma español y ucfirst es para colocar la primera letra en mayuscula
        // 

        $pdf = \PDF::loadView('pdf.espagoot2',['resumen'=>$resumen, 'razon'=>$razon,'tota' => $tota,'subtota' => $subtota,'ivatota' => $ivatota,
        'fechaini'=>$fechaini, 'fechafin'=>$fechafin,'mes'=>$mes, 'fecharea'=>$fecharea, 'empre'=>$empre, 'folio' =>$folio, 'fechao'=>$fechao,'nomcontra'=>$nomcontra ]);
        return $pdf->stream('Estado de Pago ES-PL-'.$folio.'.pdf');
        
    
    }

}

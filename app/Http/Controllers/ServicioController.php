<?php

namespace App\Http\Controllers;

use App\Models\Servi_Camion;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ServicioController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $servicio=Servicio::where('estado_servi', '=', '1')->get();

        return [
            'servicios' => $servicio
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $servicio = new Servicio();
        $servicio->nomb_servi = $request->nombre;
        $servicio->descrip_servi = $request->descripcion;
        $servicio->estado_servi = '1';
        $servicio->save();
    }
    
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $servicio = Servicio::findOrFail($request->id);//ojo
        $servicio->nomb_servi = $request->nombre;
        $servicio->descrip_servi = $request->descripcion;
        $servicio->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $servicio = Servicio::findOrFail($request->id);
        $servicio->estado_servi = '0';
        $servicio->save();
    }

    public function listarServicios()
    {
        
        $servicio=Servicio::select('nomb_servi','id')
        ->where('estado_servi', '=', '1')
        ->orderBy('id','desc')->get();
    
        return [
           
            'servicios' => $servicio
        ];
    }

    public function obtenerServicios(Request $request){
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $servicio=Servi_Camion::select('nombre as nomb_servi','id')
        ->where('camiones', '=', $id)
        ->orderBy('id','desc')->get();

        return [
           
            'servicios' => $servicio
        ];
    }
    
    
}

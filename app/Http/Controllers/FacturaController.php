<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use App\Models\Factura;
use App\Models\Factu_Detalle;
use App\Models\Factu_Pago;
use App\Models\Empresa;
use App\Models\Espago_Ot;
use App\Models\Espago_Guia;
use App\Models\Cuenta;
use App\Models\Cartola;

class FacturaController extends Controller
{
    // Facturas de EEPP de OT
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Factura::all();

        return [
            'codigo' => $codigo->last()
        ];
    }

    public function index(Request $request){
        if(!$request->ajax()) return redirect('/');
        
        $facturas=Factura::join('empresas','empresas.id','=','facturaciones.id_empre')
        ->join('estados_pro','estados_pro.id','=','facturaciones.pro_factu')
        ->join('clientempresas','clientempresas.id','=','facturaciones.id_cliente')
        ->select('facturaciones.id as idfac','folio_fact','fechaemi','fechavenci','empresas.id as idem','empresas.nombre_em','monto','monto_actual','glosa',
        'estados_pro.id as idestado','estados_pro.nombre_es','clientempresas.id as cli','clientempresas.razon','tipo_factu','tipo_contra','glosa')
        ->where('facturaciones.tipo_factu', '=', 1)
        ->where('facturaciones.estado_fact', '=', 1)->get();

        $empresas=Empresa::where('estado_em', '=', '1')->get();

        return [
            'facturas' => $facturas,
            'empresas' => $empresas
        ];
    }

    public function listarEstados(Request $request){
        if(!$request->ajax()) return redirect('/');

        $id_cliente = $request->id_cliente;
        $tipo = $request->tipo;
        $resumenot = Espago_Ot::join('empresas','espago_tra.id_empresa', '=' ,'empresas.id')
        ->where('espago_tra.id_cliente', '=', $id_cliente)
        ->where('espago_tra.tipo', '=', $tipo)
        ->where('espago_tra.respuesta_tra', '=', 9)
        ->where('espago_tra.estado', '=', 1)

        ->select('espago_tra.id','folio_ept','fechaini','fechafin','fechaorigen','pagotra_neto','pagotra_iva','pagotra_total')->get();

        return [
            'resumenot' => $resumenot
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $factura = new Factura();
            $factura->folio_fact = $request->folio_fact;
            $factura->fechaemi = $request->fechaemi;
            $factura->fechavenci = $request->fechavenci;
            $factura->id_empre = $request->id_empre;
            $factura->id_cliente = $request->id_cliente;
            $factura->glosa = $request->glosa;

            $factura->tipo_contra = $request->tipo_contra;

            //Tipo 1 es por OT y tipo 2 es por Guia
            $factura->tipo_factu = 1;
            $factura->pro_factu = 10;

            $factura->monto = $request->monto;
            $factura->monto_actual = 0;
            $factura->idusuario = \Auth::user()->id;
            $factura->save();

            $detalle = $request->data;

            foreach($detalle as $ep=>$det){
                $detal = new Factu_Detalle();
                $detal->id_factu = $factura->id;
                $detal->pago_ot = $det['id'];
                //Tipo 1 es por OT y tipo 2 es por Guia
                $detal->tipo_pago = 1;
                

                $espago = Espago_Ot::findOrFail($det['id']);
                $espago->respuesta_tra = 10;
                $espago->save();
                $detal->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function detallesFactura(Request $request){
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $detalles = Factu_Detalle::join('espago_tra','espago_tra.id','=','factu_detalles.pago_ot')
        ->select('espago_tra.id','espago_tra.folio_ept','espago_tra.fechaini','espago_tra.fechafin','espago_tra.fechaorigen','espago_tra.pagotra_neto',
        'espago_tra.pagotra_iva','espago_tra.pagotra_total')
        ->where('factu_detalles.id_factu', '=', $id)
        ->where('factu_detalles.tipo_pago', '=', 1)
        ->where('factu_detalles.estado_deta', '=', 1)->get();
        
        return [
            'detalles' => $detalles
        ];
        
    }

    public function pagarFactuOt(Request $request){
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();

            $factu_pago = new Factu_Pago();
            $factu_pago->id_fac = $request->id_factu;
            $factu_pago->fecha_pago = $request->fecha_pago;
            $factu_pago->monto_pago = $request->monto_pago;
            $factu_pago->tipo = $request->tipo;
            $factu_pago->idusuario = \Auth::user()->id;
            $factu_pago->save();

            $factura = Factura::findOrFail($request->id_factu);
            $factura->monto_actual += $request->monto_pago;
            $factura->save();

            $cartola = new Cartola();
            $cartola->fecha = $request->fecha;
            $cartola->origen = $request->origen;
            $cartola->numero = $request->numero;
            $cartola->cuenta_id = $request->idcuenta;
            $cartola->ingresos = $request->ingresos;
            $cartola->tipo = $request->tipo;
            $cartola->save();

            $cuenta = Cuenta::findOrFail($request->idcuenta);
            $cuenta->total += $cartola->ingresos;
            $cuenta->save();


            if($request->activo == 1){
                $factura = Factura::findOrFail($request->id_factu);
                $factura->pro_factu = 12;
                $factura->save();

                $detalle = $request->data;

                foreach($detalle as $ep=>$det){ 
                    $espago = Espago_Ot::findOrFail($det['id']);
                    $espago->respuesta_tra = 12;
                    $espago->save();
                }
            }

            DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function pagosFactura(Request $request){
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $pagos = Factu_pago::select('id','id_fac','fecha_pago','monto_pago','tipo')
        ->where('factu_pagos.id_fac', '=', $id)
        ->where('factu_pagos.estado_pago', '=', 1)->get();
        
        return [
            'pagos' => $pagos
        ];
        
    }
    //
    
    // Facturas de EEPP de GUIAS
    public function index2(Request $request){
        if(!$request->ajax()) return redirect('/');
        
        $facturas=Factura::join('empresas','empresas.id','=','facturaciones.id_empre')
        ->join('estados_pro','estados_pro.id','=','facturaciones.pro_factu')
        ->join('clientempresas','clientempresas.id','=','facturaciones.id_cliente')
        ->select('facturaciones.id as idfac','folio_fact','fechaemi','fechavenci','empresas.id as idem','empresas.nombre_em','monto','monto_actual','glosa',
        'estados_pro.id as idestado','estados_pro.nombre_es','clientempresas.id as cli','clientempresas.razon','tipo_factu','tipo_contra','glosa')
        ->where('facturaciones.tipo_factu', '=', 2)
        ->where('facturaciones.estado_fact', '=', 1)->get();

        $empresas=Empresa::where('estado_em', '=', '1')->get();

        return [
            'facturas' => $facturas,
            'empresas' => $empresas
        ];
    }

    public function listarEstadosGui(Request $request){
        if(!$request->ajax()) return redirect('/');

        $id_cliente = $request->id_cliente;
        $tipo = $request->tipo;

        $resumengui = Espago_Guia::join('empresas','espago_guias.id_empresa', '=' ,'empresas.id')
        ->where('espago_guias.id_cliente', '=', $id_cliente)
        ->where('espago_guias.tipo', '=', $tipo)
        ->where('espago_guias.respu_espa', '=', 9)
        ->where('espago_guias.estado_espa', '=', 1)

        ->select('espago_guias.id','folio_epg','fechaini','fechafin','fechaorigen','esguia_neto','esguia_iva','esguia_total')->get();

        return [
            'resumengui' => $resumengui
        ];
    }

    public function storeGuia(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $factura = new Factura();
            $factura->folio_fact = $request->folio_fact;
            $factura->fechaemi = $request->fechaemi;
            $factura->fechavenci = $request->fechavenci;
            $factura->id_empre = $request->id_empre;
            $factura->id_cliente = $request->id_cliente;
            $factura->glosa = $request->glosa;

            $factura->tipo_contra = $request->tipo_contra;

            //Tipo 1 es por OT y tipo 2 es por Guia
            $factura->tipo_factu = 2;

            $factura->pro_factu = 10;
            $factura->monto = $request->monto;
            $factura->monto_actual = 0;
            $factura->idusuario = \Auth::user()->id;
            $factura->save();

            $detalle = $request->data;

            foreach($detalle as $ep=>$det){
                $detal = new Factu_Detalle();
                $detal->id_factu = $factura->id;
                $detal->pago_guia = $det['id'];
                //Tipo 1 es por OT y tipo 0 es por Guia
                $detal->tipo_pago = 2;
                
                $espago = Espago_Guia::findOrFail($det['id']);
                $espago->respu_espa = 10;

                $espago->save();
                $detal->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function detallesFacturaGui(Request $request){


        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $detalles = Factu_Detalle::join('espago_guias','espago_guias.id','=','factu_detalles.pago_guia')
        ->select('espago_guias.id','espago_guias.folio_epg','espago_guias.fechaini','espago_guias.fechafin','espago_guias.fechaorigen','espago_guias.esguia_neto',
        'espago_guias.esguia_iva','espago_guias.esguia_total')
        ->where('factu_detalles.id_factu', '=', $id)
        ->where('factu_detalles.tipo_pago', '=', 2)
        ->where('factu_detalles.estado_deta', '=', 1)->get();
        
        return [
            'detalles' => $detalles
        ];
        
    }

    public function pagarFactuGui(Request $request){
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();

            $factu_pago = new Factu_Pago();
            $factu_pago->id_fac = $request->id_factu;
            $factu_pago->fecha_pago = $request->fecha_pago;
            $factu_pago->monto_pago = $request->monto_pago;
            $factu_pago->tipo = $request->tipo;
            $factu_pago->idusuario = \Auth::user()->id;
            $factu_pago->save();

            $factura = Factura::findOrFail($request->id_factu);
            $factura->monto_actual += $request->monto_pago;
            $factura->save();

            $cartola = new Cartola();
            $cartola->fecha = $request->fecha;
            $cartola->origen = $request->origen;
            $cartola->numero = $request->numero;
            $cartola->cuenta_id = $request->idcuenta;
            $cartola->ingresos = $request->ingresos;
            $cartola->tipo = $request->tipo;
            $cartola->save();

            $cuenta = Cuenta::findOrFail($request->idcuenta);
            $cuenta->total += $cartola->ingresos;
            $cuenta->save();

            if($request->activo == 1){
                $factura = Factura::findOrFail($request->id_factu);
                $factura->pro_factu = 12;
                $factura->save();

                $detalle = $request->data;

                foreach($detalle as $ep=>$det){ 
                    $espago = Espago_Guia::findOrFail($det['id']);
                    $espago->respu_espa = 12;
                    $espago->save();
                }
            }


            
            DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function pagosFacturaGuia(Request $request){
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $pagos = Factu_pago::select('id','id_fac','fecha_pago','monto_pago','tipo')
        ->where('factu_pagos.id_fac', '=', $id)
        ->where('factu_pagos.estado_pago', '=', 1)->get();
        
        return [
            'pagos' => $pagos
        ];
        
    }
    // 

}


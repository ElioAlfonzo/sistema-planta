<?php

namespace App\Http\Controllers;

use App\Models\Traba_empre;
use Illuminate\Http\Request;

class GestionController extends Controller
{
    
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if($buscar==''){
            $gestion=Traba_empre::where('estado_em', '=', '1')
            ->orderBy('id','desc')->paginate(10);
        }else{
            $gestion=Traba_empre::where($criterio, 'like' , '%'. $buscar . '%')
            ->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'empresas' => $gestion
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $gestion = new Traba_empre();
        $gestion->codigo_em = $request->codigo;
        $gestion->nombre_em = $request->nombre;
        $gestion->descrip_em = $request->descripcion;
        $gestion->direccion_em = $request->direccion;
        $gestion->save();
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $gestion = Traba_empre::findOrFail($request->id);//ojo
        $gestion->nombre_em = $request->nombre;
        $gestion->descrip_em = $request->descripcion;
        $gestion->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $gestion = Traba_empre::findOrFail($request->id);
        $gestion->estado_em = '0';
        $gestion->save();
    }

    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Traba_empre::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Cordinacion;
use App\Models\Solicitud;
use App\Models\Clientempresa;
use App\Models\Cordi_Cami;
use App\Models\Cordi_Tra;
use App\Models\Presupuesto_Servi;
use App\Models\Asignacion_Servi;
use App\Models\Asignacion;
use App\Models\Presupuesto;
use App\Models\Servicio;

use App\Models\Asistencia;
use App\Models\Hora_extra;
use App\Models\Tipo_asi;
use App\Models\Turno_extra;


class CalendarController extends Controller
{
    

    public function index(){

        $month = date("Y-m");
        $data = $this->calendar_month($month);
        $mes = $data['month'];
        // obtener mes en espanol
        $mespanish = $this->spanish_month($mes);
        $mes = $data['month'];
  
        return view("index",[
          'data' => $data,
          'mes' => $mes,
          'mespanish' => $mespanish
        ]);
    }

    public function index_month($month){

        $data = $this->calendar_month($month);
        $mes = $data['month'];
        // obtener mes en espanol
        $mespanish = $this->spanish_month($mes);
        $mes = $data['month'];
  
        return view("index",[
          'data' => $data,
          'mes' => $mes,
          'mespanish' => $mespanish
        ]);
  
    }

    

    public static function spanish_month($month)
    {

        $mes = $month;
        if ($month=="Jan") {
          $mes = "Enero";
        }
        elseif ($month=="Feb")  {
          $mes = "Febrero";
        }
        elseif ($month=="Mar")  {
          $mes = "Marzo";
        }
        elseif ($month=="Apr") {
          $mes = "Abril";
        }
        elseif ($month=="May") {
          $mes = "Mayo";
        }
        elseif ($month=="Jun") {
          $mes = "Junio";
        }
        elseif ($month=="Jul") {
          $mes = "Julio";
        }
        elseif ($month=="Aug") {
          $mes = "Agosto";
        }
        elseif ($month=="Sep") {
          $mes = "Septiembre";
        }
        elseif ($month=="Oct") {
          $mes = "Octubre";
        }
        elseif ($month=="Oct") {
          $mes = "December";
        }
        elseif ($month=="Dec") {
          $mes = "Diciembre";
        }
        else {
          $mes = $month;
        }
        return $mes;
    }

    public function details($id){

      // $event = Cordinacion::find($id);
      //Consulta eventos y filtra por fecha

      $event = Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
      // ->join('presupuestos','presupuestos.id','=','cordinaciones.pre_id')
      ->select('cordinaciones.id','cordinaciones.razon','cordinaciones.hora','cordinaciones.hora_fin','cordinaciones.soli_id',
      'cordinaciones.pre_id','cordinaciones.asig_id','folio_cordi','cordinaciones.tipo','cordinaciones.observacion_cordi')
      ->find($id);

      //1 Presupuesto
      if($event->tipo == 1){
        $empre = Presupuesto::join('empresas','presupuestos.id_empre','=', 'empresas.id')
        ->select('empresas.id','empresas.nombre_em')->find($event->pre_id);
        // ->where('presupuestos.id','=', $event->pre_id)->get();

        $servi = Presupuesto_Servi::join('servicios','presupuestos_servi.servi_id','=', 'servicios.id')
        ->join('formacostos','presupuestos_servi.forma_id','=', 'formacostos.id')
        ->select('servicios.id','servicios.nomb_servi','presupuestos_servi.cantidad','formacostos.nombre_costo')
        ->where('presupuestos_servi.presu_id','=', $event->pre_id)->get();

      }

      //0 Asignacion
      if($event->tipo == 0){
        $empre = Asignacion::join('empresas','asignaciones.id_empre','=', 'empresas.id')
        ->select('empresas.id','empresas.nombre_em')->find($event->asig_id);
        // ->where('asignaciones.id','=', $event->asig_id)->get();

        $servi = Asignacion_Servi::join('servicios','asignaciones_servi.servi_id','=', 'servicios.id')
        ->join('formacostos','asignaciones_servi.forma_id','=', 'formacostos.id')
        ->select('servicios.id','servicios.nomb_servi','asignaciones_servi.cantidad','formacostos.nombre_costo')
        ->where('asignaciones_servi.asig_id','=', $event->asig_id)->get();
      }

      $solicitud = Solicitud::select('solicitudes.fecha_soli','solicitudes.idclientempre','solicitudes.direccion_soli','solicitudes.folio_soli')
      ->find($event->soli_id);

      $cliente = Clientempresa::select('rut','tel_em','direccion_em','correo_contac','tel_contac')
      ->find($solicitud->idclientempre);

      $traba = Cordi_Tra::join('trabajadores','cordi_tra.id_tra','=', 'trabajadores.id')
      ->select('trabajadores.id','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut')
      ->where('cordi_tra.id_cordi','=', $event->id)->get();

      $cami = Cordi_Cami::join('camiones','cordi_cami.id_cami','=', 'camiones.id')
      ->select('camiones.id','camiones.ppu','camiones.marca')
      ->where('cordi_cami.id_cordi','=', $event->id)->get();

      return view("evento",[
        "event" => $event,
        "soli" =>$solicitud,
        "cli" => $cliente,
        "servi" => $servi,
        "traba" => $traba,
        "cami" => $cami,
        "empre" => $empre
      ]);

    }

    public function pdf($id){

      // $event = Cordinacion::find($id);
      //Consulta eventos y filtra por fecha
      $valor = $id;
      $event = Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
      ->select('cordinaciones.id','cordinaciones.razon','cordinaciones.hora','cordinaciones.hora_fin','cordinaciones.soli_id',
      'cordinaciones.pre_id','cordinaciones.asig_id','folio_cordi','cordinaciones.tipo','cordinaciones.observacion_cordi')
      ->find($id);

      //1 Presupuesto
      if($event->tipo == 1){
        $empre = Presupuesto::join('empresas','presupuestos.id_empre','=', 'empresas.id')
        ->select('empresas.id','empresas.nombre_em')->find($event->pre_id);
        // ->where('presupuestos.id','=', $event->pre_id)->get();

        $servi = Presupuesto_Servi::join('servicios','presupuestos_servi.servi_id','=', 'servicios.id')
        ->join('formacostos','presupuestos_servi.forma_id','=', 'formacostos.id')
        ->select('servicios.id','servicios.nomb_servi','presupuestos_servi.cantidad','formacostos.nombre_costo')
        ->where('presupuestos_servi.presu_id','=', $event->pre_id)->get();

      }

      //0 Asignacion
      if($event->tipo == 0){
        $empre = Asignacion::join('empresas','asignaciones.id_empre','=', 'empresas.id')
        ->select('empresas.id','empresas.nombre_em')->find($event->asig_id);
        // ->where('asignaciones.id','=', $event->asig_id)->get();

        $servi = Asignacion_Servi::join('servicios','asignaciones_servi.servi_id','=', 'servicios.id')
        ->join('formacostos','asignaciones_servi.forma_id','=', 'formacostos.id')
        ->select('servicios.id','servicios.nomb_servi','asignaciones_servi.cantidad','formacostos.nombre_costo')
        ->where('asignaciones_servi.servi_id','=', $event->asig_id)->get();
      }

      $solicitud = Solicitud::select('solicitudes.fecha_soli','solicitudes.idclientempre','solicitudes.direccion_soli','solicitudes.folio_soli')
      ->find($event->soli_id);

      $cliente = Clientempresa::select('rut','tel_em','direccion_em','correo_contac','tel_contac')
      ->find($solicitud->idclientempre);

      $traba = Cordi_Tra::join('trabajadores','cordi_tra.id_tra','=', 'trabajadores.id')
      ->select('trabajadores.id','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut')
      ->where('cordi_tra.id_cordi','=', $event->id)->get();

      $cami = Cordi_Cami::join('camiones','cordi_cami.id_cami','=', 'camiones.id')
      ->select('camiones.id','camiones.ppu','camiones.marca')
      ->where('cordi_cami.id_cordi','=', $event->id)->get();

      return view("pdf.coordinacion",[
        "event" => $event,
        "soli" =>$solicitud,
        "cli" => $cliente,
        "servi" => $servi,
        "traba" => $traba,
        "cami" => $cami,
        "empre" => $empre,
        
      ]);

    }

    //Eventos
    public static function calendar_month($month){
      //$mes = date("Y-m");
      $mes = $month;
      //sacar el ultimo de dia del mes
      $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
      //sacar el dia de dia del mes
      $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));
      $daysmonth  =  date("d", strtotime($fecha));
      $montmonth  =  date("m", strtotime($fecha));
      $yearmonth  =  date("Y", strtotime($fecha));
      // sacar el lunes de la primera semana
      $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
      $diaDeLaSemana = date("w", $nuevaFecha);
      $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600); //Restar los segundos totales de los dias transcurridos de la semana
      $dateini = date ("Y-m-d",$nuevaFecha);
      //$dateini = date("Y-m-d",strtotime($dateini."+ 1 day"));
      // numero de primer semana del mes
      $semana1 = date("W",strtotime($fecha));
      // numero de ultima semana del mes
      $semana2 = date("W",strtotime($daylast));
      // semana todal del mes
      // en caso si es diciembre
      if (date("m", strtotime($mes))==12) {
          $semana = 5;
      }
      else {
        $semana = ($semana2-$semana1)+1;
      }
      // semana todal del mes
      $datafecha = $dateini;
      $calendario = array();
      $iweek = 0;
      while ($iweek < $semana):
          $iweek++;
          //echo "Semana $iweek <br>";
          //
          $weekdata = [];
          for ($iday=0; $iday < 7 ; $iday++){
            // code...
            $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
            $datanew['mes'] = date("M", strtotime($datafecha));
            $datanew['dia'] = date("d", strtotime($datafecha));
            $datanew['fecha'] = $datafecha;
            //AGREGAR CONSULTAS EVENTO
            //Consulta eventos y filtra por fecha
            $datanew['evento'] = Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
            ->select('cordinaciones.id','cordinaciones.razon','cordinaciones.hora','cordinaciones.hora_fin', 
            'cordinaciones.respuesta_cordi')
            ->where("cordinaciones.fecha",$datafecha)->get();
            array_push($weekdata,$datanew);
          }
          $dataweek['semana'] = $iweek;
          $dataweek['datos'] = $weekdata;
          //$datafecha['horario'] = $datahorario;
          array_push($calendario,$dataweek);
      endwhile;
      $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
      $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
      $month = date("M",strtotime($mes));
      $yearmonth = date("Y",strtotime($mes));
      //$month = date("M",strtotime("2019-03"));
      $data = array(
        'next' => $nextmonth,
        'month'=> $month,
        'year' => $yearmonth,
        'last' => $lastmonth,
        'calendar' => $calendario,
      );
      return $data;
    }

    public function indexEvento(){

        $month = date("Y-m");
        $data = $this->calendar_month($month);
        $mes = $data['month'];
        // obtener mes en espanol
        $mespanish = $this->spanish_month($mes);
        $mes = $data['month'];
  
        return view("calendario",[
          'data' => $data,
          'mes' => $mes,
          'mespanish' => $mespanish
        ]);
    }

    public function index_month_event($month){

        $data = $this->calendar_month($month);
        $mes = $data['month'];
        // obtener mes en espanol
        $mespanish = $this->spanish_month($mes);
        $mes = $data['month'];
  
        return view("calendario",[
          'data' => $data,
          'mes' => $mes,
          'mespanish' => $mespanish
        ]);
  
    }


  // Asistencias
  public static function calendar_month_asis($month){
      //$mes = date("Y-m");
      $mes = $month;
      //sacar el ultimo de dia del mes
      $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
      //sacar el dia de dia del mes
      $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));
      $daysmonth  =  date("d", strtotime($fecha));
      $montmonth  =  date("m", strtotime($fecha));
      $yearmonth  =  date("Y", strtotime($fecha));
      // sacar el lunes de la primera semana
      $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
      $diaDeLaSemana = date("w", $nuevaFecha);
      $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600); //Restar los segundos totales de los dias transcurridos de la semana
      $dateini = date ("Y-m-d",$nuevaFecha);
      //$dateini = date("Y-m-d",strtotime($dateini."+ 1 day"));
      // numero de primer semana del mes
      $semana1 = date("W",strtotime($fecha));
      // numero de ultima semana del mes
      $semana2 = date("W",strtotime($daylast));
      // semana todal del mes
      // en caso si es diciembre
      if (date("m", strtotime($mes))==12) {
          $semana = 5;
      }
      else {
        $semana = ($semana2-$semana1)+1;
      }
      // semana todal del mes
      $datafecha = $dateini;
      $calendario = array();
      $iweek = 0;
      while ($iweek < $semana):
          $iweek++;
          //echo "Semana $iweek <br>";
          //
          $weekdata = [];
          for ($iday=0; $iday < 7 ; $iday++){
            // code...
            $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
            $datanew['mes'] = date("M", strtotime($datafecha));
            $datanew['dia'] = date("d", strtotime($datafecha));
            $datanew['fecha'] = $datafecha;
            //AGREGAR CONSULTAS EVENTO
            //Consulta eventos y filtra por fecha
            $datanew['evento'] = Asistencia::join('traba_empresas', 'traba_empresas.id', 'asistencias.id_traba')
            ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
            ->join('tipo_asi', 'tipo_asi.id', 'asistencias.tipo')

            ->select('asistencias.id as idasi','trabajadores.nombre_tra','trabajadores.apellido_tra',
            'fecha_asi','hora_asi','asistencias.tipo')
            ->where("asistencias.fecha_asi",$datafecha)
            ->where("asistencias.estado_asi",1)
            ->get();

            array_push($weekdata,$datanew);
          }
          $dataweek['semana'] = $iweek;
          $dataweek['datos'] = $weekdata;
          //$datafecha['horario'] = $datahorario;
          array_push($calendario,$dataweek);
      endwhile;
      $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
      $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
      $month = date("M",strtotime($mes));
      $yearmonth = date("Y",strtotime($mes));
      //$month = date("M",strtotime("2019-03"));
      $data = array(
        'next' => $nextmonth,
        'month'=> $month,
        'year' => $yearmonth,
        'last' => $lastmonth,
        'calendar' => $calendario,
      );
      return $data;
  }

  public function indexAsis(){
      $tipo = 0;
      $month = date("Y-m");
      $data = $this->calendar_month_asis($month);
      $mes = $data['month'];
      // obtener mes en espanol
      $mespanish = $this->spanish_month($mes);
      $mes = $data['month'];

      return view("calendarioasis",[
        'data' => $data,
        'mes' => $mes,
        'mespanish' => $mespanish,
        'tipo' => $tipo
      ]);
  }
  //  los botones de  siguiente  anterior
  public function index_month_asis($month){
    $tipo = 0;
    $data = $this->calendar_month_asis($month);
    $mes = $data['month'];
    // obtener mes en espanol
    $mespanish = $this->spanish_month($mes);
    $mes = $data['month'];

    return view("calendarioasis",[
      'data' => $data,
      'mes' => $mes,
      'mespanish' => $mespanish,
      'tipo' => $tipo
    ]);

  }
  // 


 // Asistencias por trabajador
 public static function calendar_month_asis_tra($month, $id){
  //$mes = date("Y-m");
  $mes = $month;
  //sacar el ultimo de dia del mes
  $daylast =  date("Y-m-d", strtotime("last day of ".$mes));
  //sacar el dia de dia del mes
  $fecha      =  date("Y-m-d", strtotime("first day of ".$mes));
  $daysmonth  =  date("d", strtotime($fecha));
  $montmonth  =  date("m", strtotime($fecha));
  $yearmonth  =  date("Y", strtotime($fecha));
  // sacar el lunes de la primera semana
  $nuevaFecha = mktime(0,0,0,$montmonth,$daysmonth,$yearmonth);
  $diaDeLaSemana = date("w", $nuevaFecha);
  $nuevaFecha = $nuevaFecha - ($diaDeLaSemana*24*3600); //Restar los segundos totales de los dias transcurridos de la semana
  $dateini = date ("Y-m-d",$nuevaFecha);
  //$dateini = date("Y-m-d",strtotime($dateini."+ 1 day"));
  // numero de primer semana del mes
  $semana1 = date("W",strtotime($fecha));
  // numero de ultima semana del mes
  $semana2 = date("W",strtotime($daylast));
  // semana todal del mes
  // en caso si es diciembre
  if (date("m", strtotime($mes))==12) {
      $semana = 5;
  }
  else {
    $semana = ($semana2-$semana1)+1;
  }
  // semana todal del mes
  $datafecha = $dateini;
  $calendario = array();
  $iweek = 0;
  while ($iweek < $semana):
      $iweek++;
      //echo "Semana $iweek <br>";
      //
      $weekdata = [];
      for ($iday=0; $iday < 7 ; $iday++){
        // code...
        $datafecha = date("Y-m-d",strtotime($datafecha."+ 1 day"));
        $datanew['mes'] = date("M", strtotime($datafecha));
        $datanew['dia'] = date("d", strtotime($datafecha));
        $datanew['fecha'] = $datafecha;
        //AGREGAR CONSULTAS EVENTO
        //Consulta eventos y filtra por fecha
        $datanew['evento'] = Asistencia::join('traba_empresas', 'traba_empresas.id', 'asistencias.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('tipo_asi', 'tipo_asi.id', 'asistencias.tipo')

        ->select('asistencias.id as idasi','trabajadores.nombre_tra','trabajadores.apellido_tra',
        'fecha_asi','hora_asi','asistencias.tipo')
        ->where("asistencias.fecha_asi",$datafecha)
        ->where("asistencias.id_traba",$id)
        ->where("asistencias.estado_asi",1)
        ->get();

        array_push($weekdata,$datanew);
      }
      $dataweek['semana'] = $iweek;
      $dataweek['datos'] = $weekdata;
      //$datafecha['horario'] = $datahorario;
      array_push($calendario,$dataweek);
  endwhile;
  $nextmonth = date("Y-M",strtotime($mes."+ 1 month"));
  $lastmonth = date("Y-M",strtotime($mes."- 1 month"));
  $month = date("M",strtotime($mes));
  $yearmonth = date("Y",strtotime($mes));
  //$month = date("M",strtotime("2019-03"));
  $data = array(
    'next' => $nextmonth,
    'month'=> $month,
    'year' => $yearmonth,
    'last' => $lastmonth,
    'calendar' => $calendario,
  );
  return $data;
}

public function indexAsis_tra($id){
  $tipo = 1;

  $month = date("Y-m");
  $data = $this->calendar_month_asis_tra($month,$id);
  $mes = $data['month'];
  // obtener mes en espanol
  $mespanish = $this->spanish_month($mes);
  $mes = $data['month'];

  return view("calendarioasis",[
    'data' => $data,
    'mes' => $mes,
    'mespanish' => $mespanish,
    'tipo' => $tipo,
    'id'=>$id
  ]);
}
//  los botones de  siguiente  anterior
public function index_month_asis_tra($month,$id){
  $tipo = 1;

  $data = $this->calendar_month_asis_tra($month,$id);
  $mes = $data['month'];
  // obtener mes en espanol
  $mespanish = $this->spanish_month($mes);
  $mes = $data['month'];

  return view("calendarioasis",[
    'data' => $data,
    'mes' => $mes,
    'mespanish' => $mespanish,
    'tipo' =>$tipo,
    'id'=>$id
  ]);

}
// 


}

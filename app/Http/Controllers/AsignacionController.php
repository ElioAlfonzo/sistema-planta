<?php

namespace App\Http\Controllers;
use App\Models\Asignacion;
use App\Models\Asignacion_Servi;
use App\Models\Solicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsignacionController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Asignacion::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function indextotal(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $totalasig=Asignacion::select(DB::raw('COUNT(asignaciones.id) as totalasig'))
        ->where('asignaciones.estado_asi', '=', 1)
        ->where('asignaciones.respuesta_asi', '=', 6)
        ->get();

        return [
            'totalasig' => $totalasig
        ];
    }

    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $asignaciones=Asignacion::join('solicitudes','solicitudes.id','=','asignaciones.soli_asi')
        ->join('clientempresas','clientempresas.id','=','asignaciones.idclientempre')
        ->join('estados_pro','estados_pro.id','=','asignaciones.respuesta_asi')
        ->join('empresas','empresas.id','=','asignaciones.id_empre')
        ->join('zona','zona.id','=','clientempresas.idzona')
        ->select('asignaciones.id as idasig','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','asignaciones.folio_asi','asignaciones.fecha_asi','solicitudes.direccion_soli',
        'solicitudes.ciudad','solicitudes.idclientempre','solicitudes.folio_soli','clientempresas.rut','clientempresas.razon','clientempresas.tipocli','asignaciones.total_asi','asignaciones.observacion_asi'
        ,'asignaciones.respuesta_asi','asignaciones.id_empre','estados_pro.nombre_es','asignaciones.monto_asi','asignaciones.iva_asi','asignaciones.total_asi','asignaciones.soli_asi','clientempresas.tel_contac','clientempresas.tel_em',
        'empresas.nombre_em','zona.nombre_zona')
        ->where('estado_asi', '=', '1')->get();

        return [
            'asignaciones' => $asignaciones
        ];
    }
    
    public function indexCordi(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $asignaciones=Asignacion::join('solicitudes','solicitudes.id','=','asignaciones.soli_asi')
        ->join('clientempresas','clientempresas.id','=','asignaciones.idclientempre')
        ->join('estados_pro','estados_pro.id','=','asignaciones.respuesta_asi')
        ->join('empresas','empresas.id','=','asignaciones.id_empre')
        ->select('asignaciones.id as idasig','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','asignaciones.folio_asi','asignaciones.fecha_asi','solicitudes.direccion_soli',
        'solicitudes.ciudad','solicitudes.idclientempre','solicitudes.folio_soli','clientempresas.rut','clientempresas.razon','clientempresas.tipocli','asignaciones.total_asi','asignaciones.observacion_asi'
        ,'asignaciones.respuesta_asi','asignaciones.id_empre','estados_pro.nombre_es','asignaciones.monto_asi','asignaciones.iva_asi','asignaciones.total_asi','asignaciones.soli_asi','clientempresas.tel_contac','clientempresas.tel_em',
        'empresas.nombre_em')
        ->where('respuesta_asi', '=', '6')
        ->where('estado_asi', '=', '1')->get();

        return [
            'asignaciones' => $asignaciones
        ];
    }
    

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $asignacion = new Asignacion();
            $asignacion->folio_asi = $request->folio_asi;
            $asignacion->fecha_asi = $request->fecha_asi;
            $asignacion->iva_asi = $request->iva_asi;
            $asignacion->monto_asi = $request->monto_asi;
            $asignacion->total_asi = $request->total_asi;
            $asignacion->observacion_asi = $request->observacion_asi;
            $asignacion->soli_asi = $request->soli_asi; //id de la solicitud
            $asignacion->idclientempre = $request->idclientempre;
            $asignacion->id_empre = $request->id_empre;
            $asignacion->clicontratos = $request->clicontratos;
            $asignacion->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $asignacion->respuesta_asi = 6;
            $asignacion->save();

            // Aqui cambia de estado la solicitud a Coordinacion
            $solicitud = Solicitud::findOrFail($request->soli_asi);// ojo
            $solicitud->respuesta_soli = 6;
            $solicitud->save();
            // 
            
            $servicios = $request->data;//Array de detalles

            foreach($servicios as $ep=>$det){
                $servi = new Asignacion_Servi();
                $servi->asig_id = $asignacion->id;
                $servi->servi_id = $det['id_servi'];
                $servi->forma_id = $det['idcosto'];
                $servi->cantidad = $det['cantidad'];
                $servi->valor_uni = $det['precio'];
                $servi->valor_neto = $det['cantidad']* $det['precio'];
                $servi->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $asignaciones = Asignacion_Servi::join('servicios','asignaciones_servi.servi_id','=','servicios.id')
        ->join('formacostos','asignaciones_servi.forma_id','=','formacostos.id')
        ->select('asignaciones_servi.servi_id','servicios.nomb_servi','asignaciones_servi.forma_id','formacostos.nombre_costo','asignaciones_servi.cantidad',
        'asignaciones_servi.valor_uni','asignaciones_servi.valor_neto')
        ->where('asignaciones_servi.asig_id', '=', $id)
        ->orderBy('asignaciones_servi.servi_id','desc')->get();

        $id_empre=Asignacion::where('asignaciones.id', '=', $id)->select('id_empre','clicontratos')->get();

        return [
            'asignaciones' => $asignaciones,
            'empresa' => $id_empre
        ];
    }

    public function mostrarPdf(Request $request, $id){

        $asignacion=Asignacion::join('solicitudes','solicitudes.id','=','asignaciones.soli_asi')
        ->join('clientempresas','clientempresas.id','=','asignaciones.idclientempre')
        ->join('estados_pro','estados_pro.id','=','asignaciones.respuesta_asi')
        ->join('empresas','empresas.id','=','asignaciones.id_empre')

        ->select('asignaciones.id as idasig','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','clientempresas.direccion_em','asignaciones.folio_asi','asignaciones.fecha_asi','solicitudes.direccion_soli',
        'solicitudes.ciudad','solicitudes.idclientempre','solicitudes.folio_soli','clientempresas.razon','clientempresas.tipocli','asignaciones.total_asi','asignaciones.observacion_asi'
        ,'asignaciones.respuesta_asi','asignaciones.id_empre','estados_pro.nombre_es','asignaciones.monto_asi','asignaciones.iva_asi','asignaciones.total_asi','asignaciones.soli_asi','clientempresas.tel_contac','clientempresas.tel_em',
        'empresas.nombre_em','empresas.empre_image','empresas.descrip_em','empresas.descrip_dos','empresas.rut_em','empresas.telefono_em','empresas.correo_em')
        ->where('asignaciones.id','=', $id)
        ->orderBy('asignaciones.id', 'desc')->take(1)->get();

        $detalles = Asignacion_Servi::join('servicios','asignaciones_servi.servi_id','=','servicios.id')
        ->join('formacostos','asignaciones_servi.forma_id','=','formacostos.id')
        ->select('asignaciones_servi.servi_id','servicios.nomb_servi','asignaciones_servi.forma_id','formacostos.nombre_costo','asignaciones_servi.cantidad',
        'asignaciones_servi.valor_uni','asignaciones_servi.valor_neto')
        
        ->where('asignaciones_servi.asig_id', '=', $id)
        ->orderBy('asignaciones_servi.servi_id','desc')->get();

        
        $monto_neto = Asignacion::select('monto_asi')->where('id',$id)->get();
        $iva = Asignacion::select('iva_asi')->where('id',$id)->get();
        $total = Asignacion::select('total_asi')->where('id',$id)->get();

        $numero_asi=Asignacion::select('folio_asi')->where('id',$id)->get();

        $pdf = \PDF::loadView('pdf.asignacion',['asignacion'=>$asignacion,'detalles'=>$detalles,'monto'=>$monto_neto,'iva'=>$iva,'total'=>$total]);
        return $pdf->stream('Asignacion N° '.$numero_asi[0]->folio_asi.'.pdf');
        // return $pdf->download('Cotizacion-'.$numero_presu[0]->folio_pre.'.pdf');
    }

    
}


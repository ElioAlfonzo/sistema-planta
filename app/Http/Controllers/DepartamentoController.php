<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use App\Models\Formacosto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class DepartamentoController extends Controller
{
    
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $departamento=Departamento::where('estado_depa', '=', '1')->get();

        return [
            'departamentos' => $departamento
        ];
    }

   
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $departamento = new Departamento();
        $departamento->nombre_depa = $request->nombre;
        $departamento->descrip_depa = $request->descripcion;
        $departamento->estado_depa = '1';
        $departamento->save();
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $departamento = Departamento::findOrFail($request->id);//ojo
        $departamento->nombre_depa = $request->nombre;
        $departamento->descrip_depa = $request->descripcion;
        $departamento->estado_depa = '1';
        $departamento->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $departamento = Departamento::findOrFail($request->id);
        $departamento->estado_depa = '0';
        $departamento->save();
    }

    // Forma de Costos
    public function indexforma(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $formac=Formacosto::where('estado_costo', '=', '1')->get();

        return [
            'formac' => $formac
        ];
    }

   
    public function storeforma(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $formac = new Formacosto();
        $formac->nombre_costo = $request->nombre;
        $formac->descrip_costo = $request->descripcion;
        $formac->estado_costo = '1';
        $formac->save();
    }

    public function updateforma(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $formac = Formacosto::findOrFail($request->id);//ojo
        $formac->nombre_costo = $request->nombre;
        $formac->descrip_costo = $request->descripcion;
        $formac->estado_costo = '1';
        $formac->save();
    }

    public function destroyforma(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $formac = Formacosto::findOrFail($request->id);
        $formac->estado_costo = '0';
        $formac->save();
    }
}
   
    


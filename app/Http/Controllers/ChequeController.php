<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cuenta;
use App\Models\Cartola;
use App\Models\Credito;
use App\Models\Banco;
use App\Models\Cheque;
use App\Models\Cordinacion;

class ChequeController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $cheques=Cheque::join('cuentas','cuentas.id','=','cheques.id_cuenta')
        ->join('empresas','empresas.id','=','cuentas.empresa_id')
        ->join('bancos','bancos.id','=','cuentas.banco_id')
        ->join('estados_pro','estados_pro.id','=','cheques.pro_che')

        ->select('cheques.id as idcheque','cuentas.id as idcuenta','cuentas.nombre_cuenta','cuentas.num_cuenta','empresas.nombre_em','bancos.nom_banco','cuentas.total','n_cheque', 
        'fecha_emi','fecha_pago','fecha_real','monto_che','glosa_che','estados_pro.nombre_es','cheques.pro_che')
        ->where('cheques.estado_che', '=', 1)->get();

        $cuentas=Cuenta::join('empresas','empresas.id','=','cuentas.empresa_id')
        ->join('bancos','bancos.id','=','cuentas.banco_id')
        ->select('cuentas.id as idcuenta','nombre_cuenta','empresas.id as id_em','empresas.nombre_em','num_cuenta','total','tipo_cuenta','bancos.id as banco','nom_banco')
        ->get();

        return [
            'cheques' => $cheques,
            'cuentas'=> $cuentas
        ];
    }   

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;
        
        try{
            DB::beginTransaction();
            $cheque = new Cheque();
            $cheque->id_cuenta = $request->id_cuenta;
            $cheque->n_cheque = $request->n_cheque;
            $cheque->fecha_emi = $request->fecha_emi;
            $cheque->fecha_pago = $request->fecha_pago;
            $cheque->monto_che = $request->monto_che;
            $cheque->glosa_che = $request->glosa_che;
            $cheque->pro_che = 13;
            $cheque->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $cheque->save();
            
            // $cartola = new Cartola();
            // $cartola->fecha = $request->fecha_emi;
            // $cartola->origen = 'Cheque';
            // $cartola->numero = $cheque->id;
            // $cartola->cuenta_id = $request->id_cuenta;
            // $cartola->egresos = $request->monto_che;
            // $cartola->tipo = 4;
            // $cartola->save();

            // $cuenta = Cuenta::findOrFail($request->id_cuenta);// ojo
            // $cuenta->total = $request->montoactual;
            // $cuenta->save();

        DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function procesoFinal(Request $request){
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;
        
        try{
            DB::beginTransaction();

                $cheque = Cheque::findOrFail($request->id_cheque);// ojo
                $cheque->pro_che = $request->estadopago;
                $cheque->fecha_real = $request->fecha_pago;
                $cheque->save();

                if($request->estadopago == 16){

                    $cartola = new Cartola();
                    $cartola->fecha = $request->fecha_pago;
                    $cartola->origen = 'Cheque';
                    $cartola->numero = $request->cheque_id;
                    $cartola->cuenta_id = $request->idcuenta;
                    $cartola->egresos = $request->monto_che;
                    $cartola->tipo = 4;
                    $cartola->save();

                    $cuenta = Cuenta::findOrFail($request->id_cuenta);// ojo
                    $cuenta->total = $request->montoactual;
                    $cuenta->save();

                }

                

            DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
    }
}

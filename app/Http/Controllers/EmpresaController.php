<?php

namespace App\Http\Controllers;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $empresa=Empresa::where('estado_em', '=', '1')->get();

            foreach ($empresa as  $value) {
                $value['empre_image'] = asset('storage/'.$value->empre_image);
            }
              
        return [
            'empresas' => $empresa
        ];
    }

    // public function store(Request $request)
    // {
    //     if(!$request->ajax()) return redirect('/');
    //     $empresa = new Empresa();
    //     $empresa->codigo_em = $request->codigo;
    //     $empresa->rut_em = $request->rut_em;
    //     $empresa->telefono_em = $request->telefono_em;
    //     $empresa->correo_em = $request->correo_em;
    //     $empresa->direccion_em = $request->direccion;

    //     $empresa->nombre_em = $request->nombre;
    //     $empresa->descrip_em = $request->descripcion;
    //     $empresa->descrip_dos = $request->descrip_dos;
        

    //     if ($request->file('empre_image')==null)
    //     {
    //         $request->empre_image = null;
    //         // $input['prod_image'] = null;
    //     }
    //     else
    //     {
    //       $image = $request->file('empre_image');
    //       // almacena y captura el nombre del archivo
    //       $empresa->empre_image =  $image->store('empresa','public');
    //     }

    //     $empresa->save();
    // }


    public function create(Request $request){

        $input['codigo_em'] = $request->input('codigo'); 
        $input['rut_em'] = $request->input('rut_em'); 
        $input['telefono_em'] = $request->input('telefono_em'); 
        $input['correo_em'] = $request->input('correo_em'); 
        $input['direccion_em'] = $request->input('direccion_em'); 
        $input['nombre_em'] = $request->input('nombre_em'); 
        $input['descrip_em'] = $request->input('descrip_em');
        $input['descrip_dos'] = $request->input('descrip_dos');
        $input['giro'] = $request->input('giro');
        $input['empre_image'] = null;
  
        if ($request->file('empre_image')==null)
        {
          $input['empre_image'] = null;
        }
        else
        {
          $image = $request->file('empre_image');
          // almacena y captura el nombre del archivo
          $input['empre_image'] =  $image->store('empresa','public');
        }
  
        $data = Empresa::insert($input);
  
        $response['message'] = "Guardo exitosamente ";
        $response['success'] = true;
        return $response;
  
    }


    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $empresa = Empresa::findOrFail($request->id);//ojo
        $empresa->nombre_em = $request->nombre;
        $empresa->descrip_em = $request->descripcion;
        $empresa->direccion_em = $request->direccion;
        $empresa->save();
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $empresa = Empresa::findOrFail($request->id);
        $empresa->estado_em = '0';
        $empresa->save();
    }

    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Empresa::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

}

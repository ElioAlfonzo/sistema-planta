<?php

namespace App\Http\Controllers;
use App\Models\Guia_Despacho;
use App\Models\Guia_Traba;
use App\Models\Solicitud;
use App\Models\Cordinacion;
use App\Models\Asignacion;
use App\Models\Presupuesto;
use App\Models\Cuenta;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GuiaController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Guia_Despacho::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $guias=Guia_Despacho::join('clientempresas','clientempresas.id','=','guia_despachos.id_cli')
            ->join('empresas','empresas.id','=','guia_despachos.id_empre')
            ->join('estados_pro','estados_pro.id','=','guia_despachos.respuesta_gui')
            ->join('solicitudes','solicitudes.id','=','guia_despachos.id_soli')
            ->join('cordinaciones','cordinaciones.id','=','guia_despachos.id_coordi')
            ->join('zona','zona.id','=','clientempresas.idzona')

            ->select('guia_despachos.id','folio_guia','fecha_guia','hora_guia','clientempresas.razon','clientempresas.rut','clientempresas.tel_em',
            'clientempresas.tel_contac','clientempresas.correo_contac','empresas.nombre_em','monto_guia','iva_guia','total_guia',
            'estados_pro.nombre_es','estados_pro.id as idestado','guia_despachos.id_soli','guia_despachos.id_asig','guia_despachos.id_coordi','cordinaciones.folio_cordi','guia_despachos.id_empre','guia_despachos.observa_guia',
            'solicitudes.direccion_soli','zona.nombre_zona')
            ->where('estado_guia', '=', '1')
            ->orderBy('guia_despachos.id','desc')->get();

            
            $cuentas=Cuenta::join('empresas','empresas.id','=','cuentas.empresa_id')
            ->join('bancos','bancos.id','=','cuentas.banco_id')
            ->select('cuentas.id as idcuenta','nombre_cuenta','empresas.id as id_em','empresas.nombre_em','num_cuenta','total','tipo_cuenta','bancos.id as banco','nom_banco')
            ->get();


        return [
            'guias' => $guias,
            'cuentas' => $cuentas
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $guia = new Guia_Despacho();
            $guia->folio_guia = $request->folio_guia;
            $guia->fecha_guia = $request->fecha_guia;
            $guia->hora_guia = $request->hora_guia;

            $guia->id_empre = $request->id_empre;
            $guia->id_cli = $request->id_cli;
            $guia->id_soli = $request->id_soli;

            if($request->tipo == 1){
            $guia->pre_id = $request->id_asig;
            $guia->tipo = 1;

            }else{
            $guia->id_asig = $request->id_asig;
            $guia->tipo = 0;
            $guia->id_contrato = $request->id_contrato;
            }

            $guia->id_coordi = $request->id_coordi;

            $guia->observa_guia = $request->observa_guia;
            $guia->respuesta_gui = 8;

            $guia->monto_guia = $request->monto_guia;
            $guia->iva_guia = $request->iva_guia;
            $guia->total_guia = $request->total_guia;
            $guia->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $guia->save();

            $servicios = $request->data;//Array de detalles

            foreach($servicios as $ep=>$det){

                $servi = new Guia_Traba();
                $servi->guia_id =  $guia->id;
                $servi->servi_id = $det['servi_id'];
                $servi->forma_id = $det['forma_id'];
                $servi->fecha_guia = $guia->fecha_guia;
                $servi->hora_guia = $guia->hora_guia;
                $servi->cantidad = $det['cantidad'];
                $servi->valor_uni = $det['valor_uni'];
                $servi->valor_neto = $det['cantidad']* $det['valor_uni'];
                $servi->save();
            }

            $solicitud = Solicitud::findOrFail($request->id_soli);// ojo
            $solicitud->respuesta_soli = 8;
            $solicitud->save();

             // Si tipo es igual a 1 vino de presupuesto
             if($request->tipo == 1){
                $presupuesto = Presupuesto::findOrFail($request->id_asig);// ojo
                $presupuesto->respuesta_presu = 8;
                $presupuesto->save();
            }else{
                $asignacion = Asignacion::findOrFail($request->id_asig);// ojo
                $asignacion->respuesta_asi = 8;
                $asignacion->save();

            }
            
            $coordinacion = Cordinacion::findOrFail($request->id_coordi);// ojo
            $coordinacion->respuesta_cordi = 8;
            $coordinacion->save();
            

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $guia_traba = Guia_Traba::join('servicios','servicios.id','=','guia_traba.servi_id')
        ->join('formacostos','guia_traba.forma_id','=','formacostos.id')
        ->select('guia_traba.id as guiatra_id','guia_traba.servi_id','servicios.nomb_servi','guia_traba.forma_id','formacostos.nombre_costo','guia_traba.cantidad',
        'guia_traba.valor_uni','guia_traba.valor_neto')
        ->where('guia_traba.guia_id', '=', $id)
        ->orderBy('guia_traba.servi_id','desc')->get();

        $guia=Guia_Despacho::select('monto_guia','iva_guia','total_guia')
        ->where('estado_guia', '=', '1')
        ->where('guia_despachos.id', '=', $id)
        ->orderBy('guia_despachos.id', 'desc')->get();

        return [
            'guia_traba' => $guia_traba,
            'guia' => $guia
        ];
    }

    public function mostrarPdf(Request $request, $id){
        
        $orden=Guia_Despacho::join('clientempresas','clientempresas.id','=','guia_despachos.id_cli')
        ->join('empresas','empresas.id','=','guia_despachos.id_empre')
        ->join('estados_pro','estados_pro.id','=','guia_despachos.respuesta_gui')
        ->join('solicitudes','solicitudes.id','=','guia_despachos.id_soli')

        ->select('guia_despachos.id','folio_guia','fecha_guia','hora_guia','clientempresas.razon','clientempresas.rut','clientempresas.tel_em',
        'clientempresas.tel_contac','clientempresas.direccion_em','clientempresas.correo_contac','monto_guia','iva_guia','total_guia',
        'estados_pro.nombre_es','guia_despachos.id_soli','guia_despachos.id_asig','guia_despachos.id_empre','guia_despachos.observa_guia',
        'solicitudes.direccion_soli','solicitudes.ciudad','empresas.nombre_em','empresas.empre_image','empresas.descrip_em','empresas.descrip_dos','empresas.rut_em','empresas.telefono_em','empresas.correo_em')
        ->where('guia_despachos.id','=', $id)
        ->orderBy('guia_despachos.id', 'desc')->take(1)->get();

        $detalles = Guia_Traba::join('servicios','servicios.id','=','guia_traba.servi_id')
        ->join('formacostos','guia_traba.forma_id','=','formacostos.id')
        ->select('guia_traba.servi_id','servicios.nomb_servi','guia_traba.forma_id','formacostos.nombre_costo','guia_traba.cantidad',
        'guia_traba.valor_uni','guia_traba.valor_neto')
        ->where('guia_traba.guia_id', '=', $id)
        ->orderBy('guia_traba.servi_id','desc')->get();

        
        // $monto_neto = Guia_Despacho::select('monto_guia')->where('id',$id)->get();
        // $iva = Guia_Despacho::select('iva_guia')->where('id',$id)->get();
        // $total = Guia_Despacho::select('total_guia')->where('id',$id)->get();

        $numero_orden=Guia_Despacho::select('folio_guia')->where('id',$id)->get();

        $pdf = \PDF::loadView('pdf.guia_despa',['orden'=>$orden,'detalles'=>$detalles]);
        return $pdf->stream('Guia de Despacho N° -'.$numero_orden[0]->folio_guia.'.pdf');
    }
}

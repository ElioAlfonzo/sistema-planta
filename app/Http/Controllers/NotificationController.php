<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function get(){
        // return Notification::all();
        //tomamos las notificaciones no leidas
        $unreadNotifications = Auth::user()->unreadNotifications;
        $fechaActual = date('Y-m-d'); //tomamos fecha actual

        foreach ($unreadNotifications as $notification) {
            if ($fechaActual != $notification->created_at->toDateString()) {
                // las notifiaciones diferente de hoy las marque como leidas
                $notification->markAsRead();
            }
        }

        // Configuramos que solo se muestres las notificaciones para el user logueado
        // return Auth::user()->notifications; //todas las notificaciones
        return Auth::user()->unreadNotifications;
    }

    public function verVencidos() {
        $fechaActual = date("m"); //tomamos fecha actual
        
        $cdlas = DB::table('traba_empresas as contra')
        ->join('trabajadores','trabajadores.id','=','contra.trabajador_id')
        ->select(DB::raw('MONTH(contra.fecha_ci) as mes'),'trabajadores.rut','trabajadores.nombre_tra','trabajadores.apellido_tra')
        ->whereMonth('contra.fecha_ci', $fechaActual)->get();

        $lic = DB::table('traba_empresas as contra')
        ->join('trabajadores','trabajadores.id','=','contra.trabajador_id')
        ->select(DB::raw('MONTH(contra.fecha_lic) as mes'),'trabajadores.rut','trabajadores.nombre_tra','trabajadores.apellido_tra')
        ->whereMonth('contra.fecha_lic', $fechaActual)->get();


        return ['cdlas' => $cdlas, 'lic' => $lic];
        // DB::raw('YEAR(guia.fecha_guia) as anio'),
        // DB::raw('SUM(guia.total_guia) as numero'))
        
        // ->groupBy( DB::raw('MONTH(guia.fecha_guia)'), DB::raw('YEAR(guia.fecha_guia)') )
        
    }
    
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Solicitud;
use App\Models\Solicitudservi;
use App\Models\Clientempresa;
use App\Models\User;
use App\Notifications\NotifyAdmin; //para las notifycation

class SolicitudController extends Controller
{
    // Solicitudes Empresas
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $solicitudes=Solicitud::join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('estados_pro','estados_pro.id','=','solicitudes.respuesta_soli')
            ->join('zona','zona.id','=','clientempresas.idzona')
            ->select('folio_soli','fecha_soli','solicitudes.created_at as fecha_creacion','ciudad','direccion_soli','clientempresas.razon','clientempresas.rut','clientempresas.id as ide',
            'clientempresas.direccion_em','clientempresas.tel_em','clientempresas.correo_contac','clientempresas.tel_contac','observacion',
            'solicitudes.id as idesoli','estados_pro.nombre_es', 'solicitudes.contrato','zona.nombre_zona','solicitudes.desplaza')
            ->where('estado_soli', '=', '1')
            ->where('clientempresas.tipocli', '=', '1')->get();

        return [
            'solicitudes' => $solicitudes
        ];
    }
  
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        // si el cliente no tiene contrato
        if($request->tipo_contra == 0){
            $respuesta = 1; //comienza proceso desde 1
        }else {
            // en caso de tener contrato salta al 5 paso Asignacion de Servicios
            $respuesta = 5;
        }

        try{
            DB::beginTransaction();
            $solicitud = new Solicitud();
            $solicitud->folio_soli = $request->folio_soli;
            $solicitud->fecha_soli = $request->fecha_soli;
            $solicitud->ciudad = $request->ciudad;
            $solicitud->direccion_soli = $request->direccion_soli;
            $solicitud->idclientempre = $request->idclientempre;
            $solicitud->observacion = $request->observacion;
            $solicitud->contrato = $request->tipo_contra;
            $solicitud->desplaza = $request->desplaza;
            $solicitud->respuesta_soli = $respuesta;
            $solicitud->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $solicitud->save();

            // Tomamo fecha actual y hacemos una consulta de las solicitudes
            // $fechaActual = date('Y-m-d');
            // $numSoli = DB::table('solicitudes')->whereDate('created_at', $fechaActual)->count();

            // // lo enviamos como un arregl
            // $arregloDatos = [
            // 'solicitud' =>  [
            //                 'numero' => $numSoli,
            //                 'msj' => 'Solicitudes'
            //                 ]
            // ];
            // // ahora consultamos todos los usuarios para notificarle
            // $allUser = User::all();

            // // Recorremos todos los usuarios para notificarles
            // foreach ($allUser as $notificar){
            //     User::findOrFail($notificar->id)->notify(new NotifyAdmin($arregloDatos));
            // }


            DB::commit();        
        } catch (Exception $e){
            DB::rollBack();
        }
    }
      
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $solicitud = Solicitud::findOrFail($request->id);//ojo
        $solicitud->fecha_soli = $request->fecha_soli;
        $solicitud->ciudad = $request->ciudad;
        $solicitud->direccion_soli = $request->direccion_soli;
        $solicitud->observacion = $request->observacion;
        $solicitud->save();
    }


    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $solicitud = Solicitud::findOrFail($request->id);
        $solicitud->estado_soli = '0';
        $solicitud->save();
    }

    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Solicitud::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function selectEm (Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;
        $empresa=Clientempresa::select('id','rut','razon','direccion_em','tel_em','correo_contac','tel_contac','contrato')
        ->where('razon', 'like' , '%' . $filtro . '%' )
        ->where('estado_em', '=', '1')
        ->where('tipocli', '=', '1')
        ->orderby('id','asc')->get();

        return ['empresa' =>$empresa];
    }

    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
    
        $id = $request->id;
        //  ->join('users','idusuario','=','users.id')
        $servicios = Solicitudservi::join('servicios','solicitud_servi.id_servi','=','servicios.id')
        ->select('servicios.id','servicios.nomb_servi')
        ->where('solicitud_servi.id_soli', '=', $id)
        ->orderBy('servicios.id','desc')->get();

            return [
                'servicios' => $servicios
            ];

    }
    //--- Termina Solicitudes Empresas----//

    // Solicitudes Aprobadas
    public function aprobados(Request $request){
        if(!$request->ajax()) return redirect('/');
        
            $solicitudes=Solicitud::join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('estados_pro','estados_pro.id','=','solicitudes.respuesta_soli')
            ->select('folio_soli','fecha_soli','ciudad','direccion_soli','clientempresas.razon','clientempresas.rut','clientempresas.id as ide',
            'clientempresas.direccion_em','clientempresas.tel_em','clientempresas.correo_contac','clientempresas.tel_contac','clientempresas.tipocli',
            'observacion','solicitudes.id as idesoli','estados_pro.nombre_es')
            ->where('estado_soli', '=', '1')
            ->where('respuesta_soli', '=', '3')->get();

        return [
            'solicitudes' => $solicitudes
        ];
    }
    // 

    // Solicitudes Particulares //
    public function indexParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $solicitudes=Solicitud::join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('estados_pro','estados_pro.id','=','solicitudes.respuesta_soli')
            ->join('zona','zona.id','=','clientempresas.idzona')
            ->select('folio_soli','fecha_soli','solicitudes.created_at as fecha_creacion','ciudad','direccion_soli','clientempresas.razon','clientempresas.id as ide',
            'clientempresas.tel_em','clientempresas.correo_contac','observacion',
            'solicitudes.id as idesoli','solicitudes.respuesta_soli','solicitudes.desplaza','estados_pro.nombre_es','clientempresas.contrato','zona.nombre_zona')
            ->where('estado_soli', '=', '1')
            ->where('clientempresas.tipocli', '=', '2')->get();

        return [
            'solicitudes' => $solicitudes
        ];
    }

    public function codigoParti(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Solicitud::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function selectParti (Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;
        $empresa=Clientempresa::select('id','razon','direccion_em','tel_em','correo_contac','contrato')
        ->where('razon', 'like' , '%' . $filtro . '%' )
        ->where('estado_em', '=', '1')
        ->where('tipocli', '=', '2')
        ->orderby('id','asc')->get();

        return ['empresa' =>$empresa];
    }

    public function storeParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
         // si el cliente no tiene contrato
        //  if($request->tipo_contra == 0){
        $respuesta = 1; //comienza proceso desde 1
        // }else {
        //     // en caso de tener contrato salta al 5 paso coordinacion
        //     $respuesta = 3;
        // }

        try{
            DB::beginTransaction();
            $solicitud = new Solicitud();
            $solicitud->folio_soli = $request->folio_soli;
            $solicitud->fecha_soli = $request->fecha_soli;
            $solicitud->ciudad = $request->ciudad;
            $solicitud->direccion_soli = $request->direccion_soli;
            $solicitud->idclientempre = $request->idclientempre;
            $solicitud->observacion = $request->observacion;
            $solicitud->desplaza = $request->desplaza;
            $solicitud->contrato = 0;
            $solicitud->respuesta_soli = $respuesta;
            $solicitud->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $solicitud->save();

            // $servicios = $request->data;//Array de detalles
            // foreach($servicios as $ep=>$det){
            //     $servi = new Solicitudservi();
            //     $servi->id_soli = $solicitud->id;
            //     $servi->id_servi = $det['id'];
            //     $servi->save();
            // }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function updateParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $solicitud = Solicitud::findOrFail($request->id);//ojo
        $solicitud->fecha_soli = $request->fecha_soli;
        $solicitud->ciudad = $request->ciudad;
        $solicitud->direccion_soli = $request->direccion_soli;
        $solicitud->observacion = $request->observacion;
        $solicitud->save();
    }
    // Solicitudes Particulares //

    public function solicitudesTodas(Request $request){
        if(!$request->ajax()) return redirect('/');
        
            $solicitudes=Solicitud::join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('estados_pro','estados_pro.id','=','solicitudes.respuesta_soli')
            ->select('folio_soli','fecha_soli','ciudad','direccion_soli','clientempresas.razon','clientempresas.rut','clientempresas.id as ide',
            'clientempresas.direccion_em','clientempresas.tel_em','clientempresas.correo_contac','clientempresas.tel_contac','observacion',
            'solicitudes.id as idesoli','estados_pro.nombre_es','clientempresas.tipocli','solicitudes.created_at')
            ->where('solicitudes.contrato', '=', '0')
            ->where('solicitudes.estado_soli', '=', '1')
            ->where('solicitudes.respuesta_soli', '=', '1')->get();
            
        return [
            'solicitudes' => $solicitudes
        ];
    }

    public function solicitudesAsignar(Request $request){
        if(!$request->ajax()) return redirect('/');
        
            $solicitudes=Solicitud::join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('estados_pro','estados_pro.id','=','solicitudes.respuesta_soli')
            ->select('folio_soli','fecha_soli','ciudad','direccion_soli','clientempresas.razon','clientempresas.rut','clientempresas.id as ide',
            'clientempresas.direccion_em','clientempresas.tel_em','clientempresas.correo_contac','clientempresas.tel_contac','observacion',
            'solicitudes.id as idesoli','estados_pro.nombre_es','clientempresas.tipocli', 'solicitudes.created_at')
            ->where('solicitudes.contrato', '=', '1')
            ->where('estado_soli', '=', '1')
            ->where('respuesta_soli', '=', '5')->get();
            
        return [
            'solicitudes' => $solicitudes
        ];
    }
    
}

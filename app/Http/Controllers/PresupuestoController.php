<?php

namespace App\Http\Controllers;
use App\Models\Presupuesto;
use App\Models\Presupuesto_Servi;
use App\Models\Clientempresa;
use App\Models\Solicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PresupuestoController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $presupuesto=Presupuesto::join('solicitudes','solicitudes.id','=','presupuestos.soli_id')
        ->join('clientempresas','clientempresas.id','=','presupuestos.idclientempre')
        ->join('estados_pro','estados_pro.id','=','presupuestos.respuesta_presu')
        ->join('empresas','empresas.id','=','presupuestos.id_empre')
        ->join('zona','zona.id','=','clientempresas.idzona')
        ->select('presupuestos.id as idpresu','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','presupuestos.folio_pre','presupuestos.fecha_pre','solicitudes.direccion_soli',
        'solicitudes.ciudad','solicitudes.idclientempre','solicitudes.folio_soli','clientempresas.rut','clientempresas.razon','clientempresas.tipocli','presupuestos.total_pre','presupuestos.observacion_presu'
        ,'presupuestos.respuesta_presu','estados_pro.nombre_es','presupuestos.monto_pre','presupuestos.iva_pre','presupuestos.total_pre','presupuestos.soli_id','clientempresas.tel_contac','clientempresas.tel_em',
        'empresas.nombre_em', 'presupuestos.id_empre','zona.nombre_zona')
        ->where('estado_presu', '=', 1)->get();

        return [
            'presupuesto' => $presupuesto
        ];
    }
    
    public function indextotal(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $totalpresu=Presupuesto::select(DB::raw('COUNT(presupuestos.id) as totalpresu'))
        ->where('presupuestos.estado_presu', '=', 1)
        ->where('presupuestos.respuesta_presu', '=', 3)
        ->get();

        return [
            'totalpresu' => $totalpresu
        ];
    }

    public function aprobados(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $presupuesto=Presupuesto::join('solicitudes','solicitudes.id','=','presupuestos.soli_id')
            ->join('clientempresas','clientempresas.id','=','presupuestos.idclientempre')
            ->join('estados_pro','estados_pro.id','=','presupuestos.respuesta_presu')
            ->select('presupuestos.id as idpresu','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','presupuestos.folio_pre',
            'presupuestos.fecha_pre','solicitudes.direccion_soli','solicitudes.folio_soli','solicitudes.ciudad','solicitudes.idclientempre',
            'solicitudes.fecha_soli','solicitudes.desplaza','clientempresas.rut','clientempresas.razon','clientempresas.tipocli',
            'presupuestos.total_pre','presupuestos.observacion_presu','presupuestos.respuesta_presu','estados_pro.nombre_es',
            'presupuestos.monto_pre','presupuestos.iva_pre','presupuestos.total_pre','presupuestos.soli_id','clientempresas.tel_contac','clientempresas.tel_em')

            ->where('respuesta_presu', '=', '3')
            ->where('estado_presu', '=', '1')->get();

        return [
            'presupuesto' => $presupuesto
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $presupuesto = new Presupuesto();
            $presupuesto->folio_pre = $request->folio_pre;
            $presupuesto->fecha_pre = $request->fecha_pre;
            $presupuesto->iva_pre = $request->iva_pre;
            $presupuesto->monto_pre = $request->monto_pre;
            $presupuesto->total_pre = $request->total_pre;
            $presupuesto->observacion_presu = $request->observacion_pre;
            $presupuesto->soli_id = $request->soli_id;
            $presupuesto->respuesta_presu = 2;
            $presupuesto->idclientempre = $request->idclientempre;
            $presupuesto->id_empre = $request->id_empre;
            $presupuesto->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $presupuesto->save();

            // Aqui cambia de estado la solicitud
            $solicitud = Solicitud::findOrFail($request->soli_id);// ojo
            $solicitud->respuesta_soli = 2;
            $solicitud->save();
            // 
            
            $servicios = $request->data;//Array de detalles

            foreach($servicios as $ep=>$det){
                $servi = new Presupuesto_Servi();
                $servi->presu_id = $presupuesto->id;
                $servi->servi_id = $det['id'];
                $servi->forma_id = $det['idcosto'];
                $servi->cantidad = $det['cantidad'];
                $servi->valor_uni = $det['precio'];
                $servi->valor_neto = $det['cantidad']* $det['precio'];
                $servi->save();
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');
        // $solicitud = Solicitud::findOrFail($request->id);//ojo
        // $solicitud->fecha_pre = $request->fecha_pre;
        // $solicitud->monto_pre = $request->ciudad;
        // $solicitud->iva_pre = $request->direccion_soli;
        // $solicitud->observacion = $request->observacion;
        // $solicitud->save();
    }

    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Presupuesto::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $presupuesto = Presupuesto::findOrFail($request->id);
        $presupuesto->estado_presu = 0;
        $presupuesto->save();
    }

    public function aprobar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
        DB::beginTransaction();

            $presupuesto = Presupuesto::findOrFail($request->id);
            $presupuesto->respuesta_presu = '3';
            $presupuesto->save();

            $solicitud = Solicitud::findOrFail($request->soli);
            $solicitud->respuesta_soli = '3';
            $solicitud->save();

        DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function rechazar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $presupuesto = Presupuesto::findOrFail($request->id);
        $presupuesto->respuesta_presu = 4;
        $presupuesto->save();

        $solicitud = Solicitud::findOrFail($request->soli);
        $solicitud->respuesta_soli = 4;
        $solicitud->save();
    }

    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $presupuestos = Presupuesto_Servi::join('servicios','presupuestos_servi.servi_id','=','servicios.id')
        ->join('formacostos','presupuestos_servi.forma_id','=','formacostos.id')
        ->select('presupuestos_servi.servi_id','servicios.nomb_servi','presupuestos_servi.forma_id','formacostos.nombre_costo','presupuestos_servi.cantidad',
        'presupuestos_servi.valor_uni','presupuestos_servi.valor_neto')
        ->where('presupuestos_servi.presu_id', '=', $id)
        ->orderBy('presupuestos_servi.servi_id','desc')->get();

        $id_empre=Presupuesto::select('presupuestos.id_empre')
        ->where('estado_presu', '=', '1')
        ->where('presupuestos.id', '=', $id)->get();

        $montospresu=Presupuesto::select('presupuestos.monto_pre','presupuestos.iva_pre','presupuestos.total_pre')
        ->where('estado_presu', '=', '1')
        ->where('presupuestos.id', '=', $id)
        ->orderBy('id', 'desc')->get();

        return [
            'presupuestos' => $presupuestos, 
            'montospresu' => $montospresu, 
            'empresa' => $id_empre
        ];
    }

    public function mostrarPdf(Request $request, $id){

        $presupuesto=Presupuesto::join('solicitudes','solicitudes.id','=','presupuestos.soli_id')
        ->join('clientempresas','clientempresas.id','=','presupuestos.idclientempre')
        ->join('estados_pro','estados_pro.id','=','presupuestos.respuesta_presu')
        ->join('empresas','empresas.id','=','presupuestos.id_empre')

        ->select('presupuestos.id as idpresu','clientempresas.rut','clientempresas.razon','clientempresas.correo_contac','presupuestos.folio_pre','presupuestos.fecha_pre','solicitudes.direccion_soli',
        'solicitudes.ciudad','solicitudes.idclientempre','solicitudes.folio_soli','clientempresas.rut','clientempresas.razon','clientempresas.tipocli','presupuestos.total_pre','presupuestos.observacion_presu'
        ,'presupuestos.respuesta_presu','estados_pro.nombre_es','presupuestos.monto_pre','presupuestos.iva_pre','presupuestos.total_pre','presupuestos.soli_id','clientempresas.tel_contac','clientempresas.tel_em',
        'empresas.nombre_em','empresas.empre_image','empresas.descrip_em','empresas.descrip_dos','empresas.rut_em','empresas.telefono_em','empresas.correo_em',
        'clientempresas.direccion_em','presupuestos.id_empre')
        
        ->where('presupuestos.id','=', $id)
        ->orderBy('presupuestos.id', 'desc')->take(1)->get();

        // foreach ($presupuesto as  $value) {
        //     $value['empre_image'] = asset('storage/'.$value->empre_image);
        // }

        $detalles=Presupuesto_Servi::join('servicios','presupuestos_servi.servi_id','=','servicios.id')
        ->join('formacostos','presupuestos_servi.forma_id','=','formacostos.id')
        ->select('presupuestos_servi.servi_id','servicios.nomb_servi','presupuestos_servi.forma_id','formacostos.nombre_costo','presupuestos_servi.cantidad',
        'presupuestos_servi.valor_uni','presupuestos_servi.valor_neto')
        ->where('presupuestos_servi.presu_id', '=', $id)
        ->orderBy('presupuestos_servi.servi_id','desc')->get();

        
        $monto_neto = Presupuesto::select('monto_pre')->where('id',$id)->get();
        $iva = Presupuesto::select('iva_pre')->where('id',$id)->get();
        $total = Presupuesto::select('total_pre')->where('id',$id)->get();

        $numero_presu=Presupuesto::select('folio_pre')->where('id',$id)->get();

        $pdf = \PDF::loadView('pdf.presupuesto',['presupuesto'=>$presupuesto,'detalles'=>$detalles,'monto'=>$monto_neto,'iva'=>$iva,'total'=>$total]);
        return $pdf->stream('Cotizacion N° '.$numero_presu[0]->folio_pre.'.pdf');
        // return $pdf->download('Cotizacion-'.$numero_presu[0]->folio_pre.'.pdf');
    }
    
}

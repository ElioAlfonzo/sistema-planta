<?php

namespace App\Http\Controllers;
use App\Models\Asistencia;
use App\Models\Hora_extra;
use App\Models\Tipo_asi;
use App\Models\Turno_extra;
use App\Models\Contratos_tra;
use App\Models\Traba_empre;
use App\Models\Trabajador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsistenciaController extends Controller
{
    // Todo Referente  Asistencias
    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $asistencias = Asistencia::join('traba_empresas', 'traba_empresas.id', 'asistencias.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('tipo_asi', 'tipo_asi.id', 'asistencias.tipo')
        ->join('users', 'users.id', 'asistencias.idusuario')
        ->join('zona', 'zona.id', 'asistencias.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')

        ->select('asistencias.id as idasi','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha_asi','hora_asi','asistencias.tipo',
        'tipo_asi.nombre_tipo','observa_asi','estado_asi','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_asi', '=', 1)
        ->orderBy('fecha_asi', 'asc')->get();

        return [
            'asistencias' => $asistencias
        ];
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            DB::beginTransaction();

            // $asistencia = new Asistencia();
            // $asistencia->id_traba = $request->id_traba;
            // $asistencia->fecha_asi = $request->fecha_asi;
            // $asistencia->hora_asi = $request->hora_asi;
            // $asistencia->tipo = $request->tipo;
            // $asistencia->observa_asi = $request->observa_asi;
            // $asistencia->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            // $asistencia->idzona = $request->idzona;
            // $asistencia->save();

            $asistencias = $request->data;//Array de Camiones
            foreach($asistencias as $ep=>$deta){

                $asistencia = new Asistencia();// ojo
                $asistencia->id_traba = $deta['id'];
                $asistencia->fecha_asi = $deta['fecha_asi'];
                $asistencia->hora_asi = $deta['hora_asi'];
                $asistencia->tipo = $deta['tipo'];
                $asistencia->observa_asi = $deta['observa_asi'];
                $asistencia->idusuario = \Auth::user()->id;
                $asistencia->idzona = $deta['idzona'];
                $asistencia->save();
            // 
            }

            DB::commit();

        }catch (Exception $e){
            DB::rollback();
        }
    }

    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $asistencia = Asistencia::findOrFail($request->id);//ojo
        $asistencia->fecha_asi = $request->fecha_asi;
        $asistencia->hora_asi = $request->hora_asi;
        $asistencia->tipo = $request->tipo;
        $asistencia->observa_asi = $request->observa_asi;
        $asistencia->save();
    }

    public function validarAsis(Request $request)
    {

      if(!$request->ajax()) return redirect('/');   
        $id_traba = $request->id_traba;
        $fecha_asi = $request->fecha_asi;
        $tipo = $request->tipo;
        $asistencias=0;

        $asisten_i = Asistencia::select('id_traba','fecha_asi','tipo')
        ->where('id_traba', '=', $id_traba)
        ->where('fecha_asi', '=', $fecha_asi)
        ->where('tipo', '=', $tipo)->count();

        if($asisten_i > 0){

            $asistencias = 1;
        }

        return $asistencias;

    }

    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $asistencia = Asistencia::findOrFail($request->id);
        $asistencia->estado_asi = '0';
        $asistencia->save();
    }
    //FUNCION PARA CONSULTAR LOS TRABAJADORES CONTRATADOS y usarlo en Asistencias
    public function indexTrasi(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;//guardamos el filtro q viene por request

        $trabajadores=Traba_empre::join('trabajadores','trabajadores.id','=','traba_empresas.trabajador_id')
        ->join('departamentos','departamentos.id','=','traba_empresas.departamento_id')
        ->join('empresas','empresas.id','=','traba_empresas.empresa_id')
        ->join('zona','zona.id','=','traba_empresas.idzona')
        ->select('trabajadores.id','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut','departamentos.nombre_depa',
        'traba_empresas.departamento_id as depa', 'empresas.nombre_em', 'zona.nombre_zona','zona.id as idzona', 'traba_empresas.id as id_traba')
        // ->where('traba_empresas.departamento_id','<','3')
        ->where('trabajadores.nombre_tra', 'like' , '%' . $filtro . '%' )
        ->where('trabajadores.estado_tra','=','1')->get();
        
        // ->where('traba_empresas.departamento_id','=','1' )->get();

        return [
            'trabajadores' => $trabajadores
        ];
    }

    public function indexTracontra(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        // $filtro = $request->filtro;//guardamos el filtro q viene por request

        $trabajadores=Traba_empre::join('trabajadores','trabajadores.id','=','traba_empresas.trabajador_id')
        ->join('departamentos','departamentos.id','=','traba_empresas.departamento_id')
        ->join('empresas','empresas.id','=','traba_empresas.empresa_id')
        ->join('zona','zona.id','=','traba_empresas.idzona')
        ->select('trabajadores.id','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut','departamentos.nombre_depa',
        'traba_empresas.departamento_id as depa', 'empresas.nombre_em', 'zona.nombre_zona','zona.id as idzona', 'traba_empresas.id as id_traba')
        // ->where('traba_empresas.departamento_id','<','3')
        // ->where('trabajadores.nombre_tra', 'like' , '%' . $filtro . '%' )
        ->where('trabajadores.estado_tra','=','1')->get();
        
        // ->where('traba_empresas.departamento_id','=','1' )->get();

        return [
            'trabajadores' => $trabajadores
        ];
    }

    public function indexTipos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $tipos=Tipo_asi::select('id','nombre_tipo')
            ->orderBy('nombre_tipo', 'asc')->get();

        return [
            'tipos' => $tipos
        ];
    }
    // Asistencias

    // Todo Referente a Horas Extras
    public function indexHoras(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $horas = Hora_extra::join('traba_empresas', 'traba_empresas.id', 'horas_extras.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('users', 'users.id', 'horas_extras.idusuario')
        ->join('zona', 'zona.id', 'horas_extras.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')

        ->select('horas_extras.id as idhora','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha','horas',
        'observa_horas','estado_hora','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_hora', '=', 1)
        ->orderBy('fecha', 'asc')->get();

        return [
            'horas' => $horas
        ];
    }

    public function storeHoras(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            DB::beginTransaction();

            $hora = new Hora_extra();
            $hora->id_traba = $request->id_traba;
            $hora->fecha = $request->fecha;
            $hora->horas = $request->horas;
            $hora->observa_horas = $request->observa_horas;
            $hora->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $hora->idzona = $request->idzona;
            $hora->save();

            DB::commit();

        }catch (Exception $e){
            DB::rollback();
        }
    }

    public function updateHoras(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $hora = Hora_extra::findOrFail($request->id);//ojo
        $hora->fecha = $request->fecha_asi;
        $hora->horas = $request->hora_asi;
        $hora->observa_horas = $request->observa_asi;
        $hora->save();
    }

    public function destroyHoras(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $hora = Hora_extra::findOrFail($request->id);
        $hora->estado_hora = '0';
        $hora->save();
    }


    // Todo Referente a Turnos Extras
    public function indexTurnos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $turnos = Turno_extra::join('traba_empresas', 'traba_empresas.id', 'turnos_extras.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('users', 'users.id', 'turnos_extras.idusuario')
        ->join('zona', 'zona.id', 'turnos_extras.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')
        ->join('tipo_asi', 'tipo_asi.id', 'turnos_extras.tipo_tur')

        ->select('turnos_extras.id as idtur','turnos_extras.tipo_tur','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha_tur','hora_tur',
        'tipo_asi.nombre_tipo','observa_tur','estado_tur','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_tur', '=', 1)
        ->orderBy('fecha_tur', 'asc')->get();

        return [
            'turnos' => $turnos
        ];
    }

    public function storeTurnos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            DB::beginTransaction();

            $turno = new Turno_extra();
            $turno->id_traba = $request->id_traba;
            $turno->fecha_tur = $request->fecha;
            $turno->hora_tur = $request->horas;
            $turno->tipo_tur = $request->tipo;
            $turno->observa_tur = $request->observa_tur;
            $turno->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $turno->idzona = $request->idzona;
            $turno->save();

            DB::commit();

        }catch (Exception $e){
            DB::rollback();
        }
    }

    public function updateTurnos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $turno = Turno_extra::findOrFail($request->id);//ojo
        $turno->fecha_tur = $request->fecha;
        $turno->hora_tur = $request->horas;
        $turno->tipo_tur = $request->tipo;
        $turno->observa_tur = $request->observa_tur;
        $turno->save();
    }

    public function destroyTurnos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $asistencia = Asistencia::findOrFail($request->id);
        $asistencia->estado_asi = '0';
        $asistencia->save();
    }

    public function indexTurnosFecha(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $idem = $request->id;
        $fechaini = $request->fechaini;
        $fechafin = $request->fechafin;

        $asistencias = Asistencia::join('traba_empresas', 'traba_empresas.id', 'asistencias.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('tipo_asi', 'tipo_asi.id', 'asistencias.tipo')
        ->join('users', 'users.id', 'asistencias.idusuario')
        ->join('zona', 'zona.id', 'asistencias.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')

        ->select('asistencias.id as idasi','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha_asi','hora_asi','asistencias.tipo',
        'tipo_asi.nombre_tipo','observa_asi','estado_asi','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_asi', '=', 1)
        ->where('asistencias.id_traba', '=', $idem)
        ->whereBetween('asistencias.fecha_asi',[$fechaini, $fechafin])
        ->orderBy('fecha_asi', 'asc')->get();
        
        //  Entradas y Salidas
        $entradas = Asistencia::select(DB::raw('COUNT(asistencias.tipo) as entrada'))
        ->where('asistencias.tipo','=','1')
        ->where('asistencias.estado_asi','=','1')
        ->where('asistencias.id_traba', '=', $idem)
        ->whereBetween('asistencias.fecha_asi',[$fechaini, $fechafin])
        ->get();

        $salidas = Asistencia::select(DB::raw('COUNT(asistencias.tipo) as salida'))
        ->where('asistencias.tipo','=','2')
        ->where('asistencias.estado_asi','=','1')
        ->where('asistencias.id_traba', '=', $idem)
        ->whereBetween('asistencias.fecha_asi',[$fechaini, $fechafin])
        ->get();

        $entraextras = Turno_extra::select(DB::raw('COUNT(turnos_extras.tipo_tur) as entrada'))
        ->where('turnos_extras.tipo_tur','=','1')
        ->where('turnos_extras.estado_tur','=','1')
        ->where('turnos_extras.id_traba', '=', $idem)
        ->whereBetween('turnos_extras.fecha_tur',[$fechaini, $fechafin])
        ->get();

        $saliextras = Turno_extra::select(DB::raw('COUNT(turnos_extras.tipo_tur) as salida'))
        ->where('turnos_extras.tipo_tur','=','2')
        ->where('turnos_extras.estado_tur','=','1')
        ->where('turnos_extras.id_traba', '=', $idem)
        ->whereBetween('turnos_extras.fecha_tur',[$fechaini, $fechafin])
        ->get();

        $horasextras = Hora_extra::select(DB::raw('SUM(horas_extras.horas) as horas'))
        ->where('horas_extras.estado_hora','=','1')
        ->where('horas_extras.id_traba', '=', $idem)
        ->whereBetween('horas_extras.fecha',[$fechaini, $fechafin])
        ->get();
        // 

        $turnos = Turno_extra::join('traba_empresas', 'traba_empresas.id', 'turnos_extras.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('users', 'users.id', 'turnos_extras.idusuario')
        ->join('zona', 'zona.id', 'turnos_extras.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')
        ->join('tipo_asi', 'tipo_asi.id', 'turnos_extras.tipo_tur')

        ->select('turnos_extras.id as idtur','turnos_extras.tipo_tur','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha_tur','hora_tur',
        'tipo_asi.nombre_tipo','observa_tur','estado_tur','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_tur', '=', 1)
        ->where('turnos_extras.id_traba', '=', $idem)
        ->whereBetween('turnos_extras.fecha_tur',[$fechaini, $fechafin])
        ->orderBy('fecha_tur', 'asc')->get();

        $horas = Hora_extra::join('traba_empresas', 'traba_empresas.id', 'horas_extras.id_traba')
        ->join('trabajadores', 'trabajadores.id', 'traba_empresas.trabajador_id')
        ->join('users', 'users.id', 'horas_extras.idusuario')
        ->join('zona', 'zona.id', 'horas_extras.idzona')
        ->join('empresas', 'empresas.id', 'traba_empresas.empresa_id')
        ->join('departamentos', 'departamentos.id', 'traba_empresas.departamento_id')

        ->select('horas_extras.id as idhora','id_traba','trabajadores.nombre_tra','trabajadores.apellido_tra','fecha','horas',
        'observa_horas','estado_hora','users.usuario', 'zona.nombre_zona','trabajadores.rut','empresas.nombre_em','departamentos.nombre_depa')
        ->where('estado_hora', '=', 1)
        ->where('horas_extras.id_traba', '=', $idem)
        ->whereBetween('horas_extras.fecha',[$fechaini, $fechafin])
        ->orderBy('fecha', 'asc')->get();

        return [
            'turnos' => $turnos,
            'horas' => $horas,
            'asistencias' => $asistencias,
            'entradas' => $entradas,
            'salidas' => $salidas,
            'entraextras' => $entraextras,
            'saliextras' => $saliextras,
            'horasextras' => $horasextras
        ];
    }    
    // Turnos Extras

}

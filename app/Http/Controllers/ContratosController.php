<?php

namespace App\Http\Controllers;
use App\Models\Contrato;
use App\Models\Contra_servi;
use App\Models\Clientempresa;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; //para la hora actual
use Illuminate\Http\Request;

class ContratosController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Contrato::all();

            return [
                'codigo' => $codigo->last()
            ];
    }

    public function selectCli (Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $filtro = $request->filtro;
        $empresa=Clientempresa::select('id','razon','direccion_em','tel_em','nombre_contac','correo_contac','rut')
        ->where('razon', 'like' , '%' . $filtro . '%' )
        ->where('estado_em', '=', '1')
        ->where('tipocli', '=', '1')
        ->orderby('id','asc')->get();

        return ['empresa' =>$empresa];
    }

    public function index(Request $request)
    {
        $contratos=Contrato::join('clientempresas','clientempresas.id','=','contratos.id_cli')
        ->join('empresas','empresas.id','=','contratos.id_empre')
        ->join('zona','zona.id','=','clientempresas.idzona')
        ->select('contratos.id as id_contra','contratos.folio_contra','contratos.nombre_contra','clientempresas.razon','clientempresas.rut','clientempresas.tel_em','clientempresas.correo_contac',
        'clientempresas.direccion_em','contratos.fecha_contra','contratos.id_cli','contratos.id_empre','empresas.nombre_em','zona.nombre_zona')
        ->where('estado_contra', '=', '1')->get();
        
        return [
            'contratos' => $contratos
        ];
    }

    public function verServicios(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id = $request->id;
         $id_contra = $request->id_contra;
        //  ->join('users','idusuario','=','users.id')
            $servi = Contra_servi::join('servicios','contra_servi.id_servi','=','servicios.id')
            ->join('formacostos','contra_servi.id_formac','=','formacostos.id')
            ->select('contra_servi.id','contra_servi.id_servi','contra_servi.precio','servicios.nomb_servi','formacostos.nombre_costo','contra_servi.id_con'
            ,'contra_servi.id_formac')
            ->where('contra_servi.id_cli', '=', $id)
            ->where('contra_servi.id_con', '=', $id_contra)
            ->where('contra_servi.estado_contraservi', '=', 1)->get();
 
             return [
                 'servi' => $servi
             ];

    }

    public function verContratos(Request $request)
    {
         //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id_cli = $request->id_cli;
         $idempre = $request->idempre;
         $contratos=Contrato::join('clientempresas','clientempresas.id','=','contratos.id_cli')
         ->join('empresas','empresas.id','=','contratos.id_empre')
        ->select('contratos.id as id_contra','contratos.folio_contra','contratos.nombre_contra','clientempresas.razon','clientempresas.rut','clientempresas.tel_em','clientempresas.correo_contac',
        'clientempresas.direccion_em','contratos.fecha_contra','contratos.id_cli','contratos.id_empre','empresas.nombre_em')
        ->where('estado_contra', '=', '1')
        ->where('id_cli', '=', $id_cli)->get();
        
        return [
            'contratos' => $contratos
        ];

    }

    public function verContratosEmpresa(Request $request){
            //solo peticiones ajax
         if(!$request->ajax()) return redirect('/');
        
         $id_cli = $request->idcli;
         $id_em = $request->idempre;

         $contratos=Contrato::join('clientempresas','clientempresas.id','=','contratos.id_cli')
         ->join('empresas','empresas.id','=','contratos.id_empre')
        ->select('contratos.id as id_contra','contratos.folio_contra','contratos.nombre_contra','clientempresas.razon','clientempresas.rut','clientempresas.tel_em','clientempresas.correo_contac',
        'clientempresas.direccion_em','contratos.fecha_contra','contratos.id_cli','contratos.id_empre','empresas.nombre_em')
        ->where('estado_contra', '=', '1')
        ->where('contratos.id_cli', '=', $id_cli)
        ->where('contratos.id_empre', '=', $id_em)->get();
        
        return [
            'contratos' => $contratos
        ];

    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $mytime= Carbon::now('America/Santiago');

        try{
            DB::beginTransaction();
        $contrato = new Contrato();
        $contrato->folio_contra = $request->folio_contra;
        $contrato->nombre_contra = $request->nombre_contra;
        $contrato->fecha_contra = $mytime->toDateTimeString(); //Hora Actual
        $contrato->id_cli = $request->id_cli;
        $contrato->id_empre = $request->id_empre;
        $contrato->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
        $contrato->save();
        
        // Cambiar el contrato del clientempresas
        $cliente = Clientempresa::findOrFail($request->id_cli);//ojo
        $cliente->contrato = 1;
        $cliente->save();
        // 

        $servicios = $request->data;//Array de detalles
            foreach($servicios as $ep=>$deta){

                $servi = new Contra_servi();// ojo
                $servi->id_con= $contrato->id;
                $servi->id_cli = $contrato->id_cli;
                $servi->id_servi = $deta['id'];
                $servi->id_formac = $deta['idcosto'];
                $servi->precio = $deta['precio'];
                $servi->save();
            }
        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function store_servi(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $servicios = $request->data;
        foreach($servicios as $ep=>$deta){

            $contra_servi = new Contra_servi();
            $contra_servi->id_cli = $request->id_cli;
            $contra_servi->id_con = $request->id_con;

            $contra_servi->id_servi = $deta['id'];
            $contra_servi->id_formac = $deta['idcosto'];
            $contra_servi->precio = $deta['precio'];
            $contra_servi->save();
        }
    }

    public function eliminarPrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $prenda = Contra_servi::findOrFail($request->id);//ojo
        $prenda->estado_contraservi = 0;
        $prenda->save();
    }

    public function cambiarPrecio(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            //aprovecho de actalizar el nombre del contrato
            $contrato = Contrato::findOrFail($request->id_contra);//ojo
            $contrato->nombre_contra = $request->nombre_contra;
            $contrato->save();
            // 

            $servi = Contra_servi::findOrFail($request->id);//ojo
            $servi->precio = $request->precio;
            $servi->save();

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function validarCli(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');

        

        $registrado = 0;

        $id = $request->id;
       //  ->join('users','idusuario','=','users.id')
           $contra = Contrato::where('contratos.id_cli', '=', $id)
           ->where('contratos.estado_contra', '=', 1)->count();

           if($contra > 0){
                $registrado = 1;
            }

            return  $registrado;
    }

}

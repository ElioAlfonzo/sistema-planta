<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Trabajador;
use App\Models\Rol;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');
        
            $users=User::join('trabajadores', 'users.id_tra', '=', 'trabajadores.id') 
            ->join('roles', 'users.idrol', '=', 'roles.id') 
            ->join('zona', 'users.idzona', '=', 'zona.id') 
            ->join('traba_empresas', 'traba_empresas.trabajador_id', '=', 'trabajadores.id') 
            ->join('departamentos', 'departamentos.id', '=', 'traba_empresas.departamento_id') 

            ->select('users.id as userid','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut','trabajadores.telefono','trabajadores.id as traid',
            'roles.nombre as roles','users.usuario','users.password','users.idrol','users.condicion',
            'zona.nombre_zona as zona','zona.id as idzona','nombre_depa')->get();
            
            // ->where('users.condicion', '=', '1')
        return [
            'users' => $users
        ];
    }

    public function store(Request $request)
    {
        // if (!$request->ajax()) return redirect('/'); 
        $user = new User();
        $user->usuario = $request->usuario;
        $user->password = bcrypt( $request->password);
        $user->idrol = $request->idrol;
        $user->idzona = $request->idzona;
        $user->id_tra = $request->id_tra;
        $user->save();
    }

    public function update(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->usuario = $request->usuario;
        $user->password = bcrypt( $request->password);
        $user->idzona = $request->idzona;
        $user->idrol = $request->idrol;
        $user->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '0';
        $user->save();
    }
 
    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '1';
        $user->save();
    }
   
    public function validar(Request $request){

        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id)->count();

        return $user;
    }
}

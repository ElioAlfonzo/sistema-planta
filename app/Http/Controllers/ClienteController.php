<?php

namespace App\Http\Controllers;
use App\Models\Clientempresa;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(Request $request)
    {
        $clientes=Clientempresa::join('users', 'users.id', '=', 'clientempresas.idusuario')
        ->join('zona', 'zona.id', '=', 'clientempresas.idzona')
        ->select('clientempresas.id','rut','razon','direccion_em','tel_em','nombre_contac','correo_contac','tel_contac','tipocli',
        'clientempresas.contrato','idusuario','clientempresas.idzona','zona.nombre_zona')
        ->where('estado_em', '=', '1')
        ->where('tipocli', '=', '1')->get();
        
        return [
            'clientes' => $clientes
        ];
    }
  
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = new Clientempresa();
        $cliente->rut = $request->rut;
        $cliente->razon = $request->razon;
        $cliente->direccion_em = $request->direccion_em;
        $cliente->tel_em = $request->tel_em;
        $cliente->nombre_contac = $request->nombre_contac;
        $cliente->correo_contac = $request->correo_contac;
        $cliente->tel_contac = $request->tel_contac;
        $cliente->tipocli = 1;
        $cliente->idusuario = \Auth::user()->id; //me guarde el usuario autenticado;
        $cliente->idzona = $request->idzona;
        $cliente->save();
    }
      
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = Clientempresa::findOrFail($request->id);//ojo
        $cliente->rut = $request->rut;
        $cliente->razon = $request->razon;
        $cliente->direccion_em = $request->direccion_em;
        $cliente->tel_em = $request->tel_em;
        $cliente->nombre_contac = $request->nombre_contac;
        $cliente->correo_contac = $request->correo_contac;
        $cliente->tel_contac = $request->tel_contac;
        $cliente->idzona = $request->idzona;
        $cliente->save();
    }


    public function destroy(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = Clientempresa::findOrFail($request->id);
        $cliente->estado_em = '0';
        $cliente->save();
    }

    public function indexParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $clientes=Clientempresa::join('users', 'users.id', '=', 'clientempresas.idusuario')
            ->join('zona', 'zona.id', '=', 'clientempresas.idzona')
            ->select('clientempresas.id','rut','razon','direccion_em','tel_em','nombre_contac','correo_contac','tel_contac','tipocli',
            'clientempresas.contrato','idusuario','clientempresas.idzona','zona.nombre_zona')
            ->where('estado_em', '=', '1')
            ->where('tipocli', '=', '2')->get();

        return [
            'clientes' => $clientes
        ];
    }
  
    public function storeParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = new Clientempresa();
        $cliente->razon = $request->nombre_clip;
        $cliente->direccion_em = $request->direccion_clip;
        $cliente->tel_em = $request->telefono_clip;
        $cliente->correo_contac = $request->correo_clip;
        $cliente->tipocli = 2;
        $cliente->idusuario = \Auth::user()->id; //me guarde el usuario autenticado;
        $cliente->idzona = $request->idzona;
        $cliente->save();
    }
      
    public function updateParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = Clientempresa::findOrFail($request->id);//ojo
        $cliente->razon = $request->nombre_clip;
        $cliente->direccion_em = $request->direccion_clip;
        $cliente->tel_em = $request->telefono_clip;
        $cliente->correo_contac = $request->correo_clip;
        $cliente->idzona = $request->idzona;
        $cliente->save();
    }

    public function destroyParti(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $cliente = Clientempresa::findOrFail($request->id);
        $cliente->estado_em = '0';
        $cliente->save();
    }

    public function todosClientes(Request $request)
    {
        $clientes=Clientempresa::where('estado_em', '=', '1')->get();
        
        return [
            'clientes' => $clientes
        ];
    }
}

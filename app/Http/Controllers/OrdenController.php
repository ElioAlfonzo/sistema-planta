<?php

namespace App\Http\Controllers;
use App\Models\Orden_tra;
use App\Models\Tra_ordenes;
use App\Models\Solicitud;
use App\Models\Presupuesto;
use App\Models\Asignacion;
use App\Models\Cordinacion;
use App\Models\Cuenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use App\User;
use App\Notifications\NotifyAdmin;

class OrdenController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Orden_tra::all();
            return [
                'codigo' => $codigo->last()
            ];
    }

    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $ordenes=Orden_tra::join('clientempresas','clientempresas.id','=','ordenes_tra.id_cli')
            ->join('empresas','empresas.id','=','ordenes_tra.id_empre')
            ->join('estados_pro','estados_pro.id','=','ordenes_tra.respuesta_ot')
            ->join('cordinaciones','cordinaciones.id','=','ordenes_tra.id_coordi')
            ->join('solicitudes','solicitudes.id','=','ordenes_tra.id_soli')
            ->join('zona','zona.id','=','clientempresas.idzona')
            ->select('ordenes_tra.id','folio_ot','fecha_ot','hora_ot','clientempresas.razon','clientempresas.rut','clientempresas.tel_em',
            'clientempresas.tel_contac','clientempresas.correo_contac','empresas.nombre_em','monto_ot','iva_ot','total_ot',
            'estados_pro.nombre_es','ordenes_tra.id_soli','ordenes_tra.id_presu','ordenes_tra.id_coordi','cordinaciones.folio_cordi','ordenes_tra.id_empre','observac_ot','solicitudes.direccion_soli',
            'ordenes_tra.respuesta_ot','zona.nombre_zona')
            ->where('estado_orden', '=', '1')->get();

            $cuentas=Cuenta::join('empresas','empresas.id','=','cuentas.empresa_id')
            ->join('bancos','bancos.id','=','cuentas.banco_id')
            ->select('cuentas.id as idcuenta','nombre_cuenta','empresas.id as id_em','empresas.nombre_em','num_cuenta','total','tipo_cuenta','bancos.id as banco','nom_banco')
            ->get();
        
            return [
                'ordenes' => $ordenes,
                'cuentas' => $cuentas
            ];
    }
    
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;

        try{
            DB::beginTransaction();
            $ordent = new Orden_tra();
            $ordent->folio_ot = $request->folio_ot;
            $ordent->fecha_ot = $request->fecha_ot;
            $ordent->hora_ot = $request->hora_ot;

            $ordent->id_empre = $request->id_empre;
            $ordent->id_cli = $request->id_cli;

            $ordent->id_soli = $request->id_soli;
            
            // Si tipo es igual a 1 vino de presupuesto
            if($request->tipo == 1){
            $ordent->id_presu = $request->id_presu;
            $ordent->tipo = 1;
            }else{
            // Si tipo es igual a 0 vino de asignacion
            $ordent->id_asig = $request->id_presu;
            $ordent->tipo = 0;
            $ordent->id_contrato = $request->clicontratos;
            }

            $ordent->id_coordi = $request->id_coordi;
            $ordent->observac_ot = $request->observac_ot;
            $ordent->estado_orden = 1;
            $ordent->respuesta_ot = 8;
            $ordent->monto_ot = $request->monto_ot;
            $ordent->iva_ot = $request->iva_ot;
            $ordent->total_ot = $request->total_ot;
            $ordent->idusuario = \Auth::user()->id; //me guarde el usuario autenticado
            $ordent->save();

            $servicios = $request->data;//Array de detalles

            foreach($servicios as $ep=>$det){
                $servi = new Tra_ordenes();
                $servi->orden_id =  $ordent->id;
                $servi->servi_id = $det['servi_id'];
                $servi->forma_id = $det['forma_id'];
                $servi->fecha_ot = $ordent->fecha_ot;
                $servi->hora_ot = $ordent->hora_ot;
                $servi->cantidad = $det['cantidad'];
                $servi->valor_uni = $det['valor_uni'];
                $servi->valor_neto = $det['cantidad']* $det['valor_uni'];
                $servi->save();
            }

            $solicitud = Solicitud::findOrFail($request->id_soli);// ojo
            $solicitud->respuesta_soli = 8;
            $solicitud->save();
            

             // Si tipo es igual a 1 vino de presupuesto
            if($request->tipo == 1){
                $presupuesto = Presupuesto::findOrFail($request->id_presu);// ojo
                $presupuesto->respuesta_presu = 8;
                $presupuesto->save();
            }else{
                $asignacion = Asignacion::findOrFail($request->id_presu);// ojo
                $asignacion->respuesta_asi = 8;
                $asignacion->save();

            }
             

            // Aqui cambia de estado la solicitud
            $coordinacion = Cordinacion::findOrFail($request->id_coordi);// ojo
            $coordinacion->respuesta_cordi = 8;
            $coordinacion->save();
            //


            // Todo esto es para lo de las Notiification
            // $fechaActual = date('Y-m-d');
            // $numOrdenes = DB::table('ordenes_tra')->whereDate('created_at', $fechaActual)->count();
            // // $numIngresos = DB::table('ordenes_tra')->whereDate('created_at', $fechaActual)->count();

            // $arregloDatos = [
            //     'ordenes' => [
            //                     'numero' => $numOrdenes,
            //                     'msj' => 'Ordenes'
            //                 ],
            // ];
            // // lista de todos los usuarios
            // // $allUsers = User::all(); 
            // foreach ($allUsers as $notificar) {
            //     User::findOrFail($notificar->id)->notify(new NotifyAdmin($arregloDatos));
            // }

        DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        $id = $request->id;

        $tra_ordenes = Tra_ordenes::join('servicios','servicios.id','=','tra_ordenes.servi_id')
        ->join('formacostos','tra_ordenes.forma_id','=','formacostos.id')
        ->select('tra_ordenes.id as traorden_id','tra_ordenes.servi_id','servicios.nomb_servi','tra_ordenes.forma_id','formacostos.nombre_costo','tra_ordenes.cantidad',
        'tra_ordenes.valor_uni','tra_ordenes.valor_neto')
        ->where('tra_ordenes.orden_id', '=', $id)
        ->orderBy('tra_ordenes.servi_id','desc')->get();

        $orden=Orden_tra::select('monto_ot','iva_ot','total_ot')
        ->where('estado_orden', '=', '1')
        ->where('ordenes_tra.id', '=', $id)
        ->orderBy('ordenes_tra.id', 'desc')->get();

        return [
            'tra_ordenes' => $tra_ordenes,
            'orden' => $orden
        ];
    }

    public function mostrarPdf(Request $request, $id){
        
        $orden=Orden_tra::join('clientempresas','clientempresas.id','=','ordenes_tra.id_cli')
        ->join('empresas','empresas.id','=','ordenes_tra.id_empre')
        ->join('estados_pro','estados_pro.id','=','ordenes_tra.respuesta_ot')
        ->join('solicitudes','solicitudes.id','=','ordenes_tra.id_soli')

        ->select('ordenes_tra.id','folio_ot','fecha_ot','hora_ot','clientempresas.razon','clientempresas.rut','clientempresas.tel_em',
        'clientempresas.tel_contac','clientempresas.correo_contac','clientempresas.direccion_em','monto_ot','iva_ot','total_ot',
        'estados_pro.nombre_es','ordenes_tra.id_soli','ordenes_tra.id_presu','ordenes_tra.id_empre','observac_ot','solicitudes.direccion_soli',
        'solicitudes.ciudad','empresas.nombre_em','empresas.empre_image','empresas.descrip_em','empresas.descrip_dos','empresas.rut_em','empresas.telefono_em','empresas.correo_em')
        ->where('ordenes_tra.id','=', $id)
        ->orderBy('ordenes_tra.id', 'desc')->take(1)->get();

        $detalles = Tra_ordenes::join('servicios','servicios.id','=','tra_ordenes.servi_id')
        ->join('formacostos','tra_ordenes.forma_id','=','formacostos.id')
        ->select('tra_ordenes.servi_id','servicios.nomb_servi','tra_ordenes.forma_id','formacostos.nombre_costo','tra_ordenes.cantidad',
        'tra_ordenes.valor_uni','tra_ordenes.valor_neto')
        ->where('tra_ordenes.orden_id', '=', $id)
        ->orderBy('tra_ordenes.servi_id','desc')->get();

        
        $monto_neto = Orden_tra::select('monto_ot')->where('id',$id)->get();
        $iva = Orden_tra::select('iva_ot')->where('id',$id)->get();
        $total = Orden_tra::select('total_ot')->where('id',$id)->get();

        $numero_orden=Orden_tra::select('folio_ot')->where('id',$id)->get();

        $pdf = \PDF::loadView('pdf.orden_tra',['orden'=>$orden,'detalles'=>$detalles,'monto'=>$monto_neto,'iva'=>$iva,'total'=>$total]);
        return $pdf->stream('Orden de Trabajo N° '.$numero_orden[0]->folio_ot.'.pdf');
    }

    public function cobrar(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;
        try{
            DB::beginTransaction();
            $orden = Orden_tra::findOrFail($request->id);// ojo
            $orden->respuesta_ot = '8';
            $orden->save();

            $solicitud = Solicitud::findOrFail($request->soli);// ojo
            $solicitud->respuesta_soli = '8';
            $solicitud->save();
            
            // Aqui cambia de estado la solicitud
            $presupuesto = Presupuesto::findOrFail($request->presu);// ojo
            $presupuesto->respuesta_presu = '8';
            $presupuesto->save();
            //

            // Aqui cambia de estado la solicitud
            $coordinacion = Cordinacion::findOrFail($request->cordi);// ojo
            $coordinacion->respuesta_cordi = '8';
            $coordinacion->save();

        DB::commit();
            
        } catch (Exception $e){
            DB::rollBack();
        }
    }

}

<?php

namespace App\Http\Controllers;
use App\Models\Solicitud;
use App\Models\Presupuesto;
use App\Models\Presupuesto_Servi;
use App\Models\Asignacion;
use App\Models\Asignacion_Servi;
use App\Models\Cordinacion;
use App\Models\Cordi_Cami;
use App\Models\Cordi_Tra;
use App\Models\Clientempresa;
use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CoordinacionesController extends Controller
{
    public function codigo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $codigo = Cordinacion::all();

            return [
                'codigo' => $codigo->last()
            ];
    }

    public function index(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
            $coordinaciones=Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
            ->join('clientempresas','clientempresas.id','=','solicitudes.idclientempre')
            ->join('zona','zona.id','=','clientempresas.idzona')
            ->join('estados_pro','estados_pro.id','=','cordinaciones.respuesta_cordi')
            ->select('cordinaciones.id','cordinaciones.folio_cordi','cordinaciones.razon','estados_pro.nombre_es','solicitudes.idclientempre',
            'cordinaciones.fecha','cordinaciones.hora','solicitudes.direccion_soli','solicitudes.ciudad','solicitudes.folio_soli','solicitudes.contrato','solicitudes.desplaza',
            'cordinaciones.observacion_cordi','cordinaciones.fecha_fin','cordinaciones.hora_fin','cordinaciones.tipo','cordinaciones.pre_id','cordinaciones.asig_id','cordinaciones.folio_tipo',
            'zona.nombre_zona')
            ->where('estado_cordi', '=', '1')->get();

            $empresa=Empresa::where('estado_em', '=', '1')->get();
            
        return [
            'coordinaciones' => $coordinaciones,
            'empresas' => $empresa
        ];
    
    }

    public function create()
    {        
    }

    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        // $solicitud = $request->soli_id;
        $evalua = $request->pre_id;
        $desplaza = $request->desplaza;
        try{
            DB::beginTransaction();
            $cordinacion = new Cordinacion();
            $cordinacion->folio_cordi = $request->folio_cordi;

            $cordinacion->fecha = $request->fecha;
            $cordinacion->hora = $request->hora;

            $cordinacion->fecha_fin = $request->fecha_fin;
            $cordinacion->hora_fin = $request->hora_fin;

            $cordinacion->razon = $request->razon;

            $cordinacion->observacion_cordi = $request->observacion_cordi;

            $cordinacion->estado_cordi = '1'; //estado para verificr si no esta eliminado

            $cordinacion->soli_id = $request->soli_id;
            
            $cordinacion->pre_id = $request->pre_id;

            $cordinacion->asig_id = $request->id_asig; //id de asignacion

            $cordinacion->folio_tipo = $request->folio_pre; //Aqui el folio de cualquiera de los 2 tipos

            $cordinacion->idusuario = \Auth::user()->id; //me guarde el usuario autenticado            

             // Aqui almacenare si es de tipo 1 o 0
            //tipo 1 es presupuesto, 0 es asignacion
            if($evalua == ""){
                $cordinacion->tipo = 0;
            }
            else{
                $cordinacion->tipo = 1;
            }
            $cordinacion->respuesta_cordi = '7';
            $cordinacion->save();

            // Aqui cambia de estado la solicitud
            $solicitud = Solicitud::findOrFail($request->soli_id);// ojo    
            $solicitud->respuesta_soli = '7';   
            $solicitud->save();
            // 

            // Aqui cambia de estado la solicitud si pre_id esta vacio entonces haz la modificaion en asignacion
            //de lo contrario hazlo en presupuesto
            if($request->pre_id == ""){
                $asignacion = Asignacion::findOrFail($request->id_asig);// ojo
                $asignacion->respuesta_asi = '7';
                $asignacion->save();
            }
            else{
                $presupuesto = Presupuesto::findOrFail($request->pre_id);// ojo
                $presupuesto->respuesta_presu = '7';
                $presupuesto->save();
            }

            $camiones = $request->data;//Array de Camiones

            foreach($camiones as $ep=>$deta){
                $cami = new Cordi_Cami();// ojo
                $cami->ppu = $deta['ppu'];
                $cami->id_cami = $deta['id'];
                $cami->id_cordi = $cordinacion->id;
                $cami->estado_cordi_cami = '1';
                $cami->save();
            }
            
            $trabajadores = $request->data2;//Array de Trabajadores
            $valor = 0;
            foreach($trabajadores as $ep=>$dete){
                if($dete['depa'] == 1 && $valor == 0){    
                    $tra = new Cordi_Tra();
                    $tra->nombre_tra = $dete['nombre_tra'];
                    $tra->id_depa = $dete['depa'];
                    $tra->id_tra = $dete['id'];
                    $tra->id_cordi = $cordinacion->id;
                    $tra->chofer = 1;
                    $tra->save();
                    $valor=1;
                
                }else{
                    $tra = new Cordi_Tra();
                    $tra->nombre_tra = $dete['nombre_tra'];
                    $tra->id_depa = $dete['depa'];
                    $tra->id_tra = $dete['id'];
                    $tra->id_cordi = $cordinacion->id;
                    $tra->save();
                }
            }

        DB::commit();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }
   
    public function obtenerDetalles(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
    
        $id = $request->id;
        $tipo = $request->tipo;
        
        if($tipo == 1){
            $folio_identi = Presupuesto::select('folio_pre')
            ->where('presupuesto.id', '=', $id)->get();
        }else{
            $folio_identi = Asignacion::select('folio_asi')
            ->where('asignaciones.id', '=', $id)->get();
        }

        $camiones = Cordi_Cami::select('id_cami as id','ppu')
        ->where('cordi_cami.id_cordi', '=', $id)
        ->orderBy('cordi_cami.id','desc')->get();

        $personal = Cordi_Tra::join('trabajadores','trabajadores.id','=','cordi_tra.id_tra')
        ->join('departamentos','departamentos.id','=','cordi_tra.id_tra')
        ->select('cordi_tra.id','cordi_tra.id_tra as idtra','trabajadores.nombre_tra','trabajadores.apellido_tra','trabajadores.rut','cordi_tra.id_depa',
        'departamentos.nombre_depa')
        ->where('cordi_tra.id_cordi', '=', $id)
        ->orderBy('cordi_tra.id','desc')->get();

            return [
                'camiones' => $camiones,
                'personal' => $personal,
                'folio_identi' => $folio_identi
            ];

    }

    public function obtenerValidar(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        // Camion
        $cami = $request->cami;
        // Horas
        $ini = $request->ini;
        $fin = $request->fin;
        // Fechas
        $fe = $request->fechasoli;
        $fe2 = $request->fechafin;
        $reservado = 0;
        
        $reserva_inicial = Cordinacion::join('cordi_cami','cordi_cami.id_cordi','=','cordinaciones.id')
        ->select('cordi_cami.id_cami as id','cordi_cami.ppu','cordinaciones.fecha','cordinaciones.hora','cordinaciones.fecha_fin','cordinaciones.hora_fin')
        ->where('cordi_cami.id_cami', '=', $cami)
        ->where('cordinaciones.fecha','<=', $fe)
        ->where('cordinaciones.fecha_fin','>=', $fe)
        ->where('cordinaciones.hora','<=', $ini)
        ->where('cordinaciones.hora_fin','>=', $ini)
        ->count();
        if($reserva_inicial > 0){
            $reservado = 1;
        }

        $reserva_final = Cordinacion::join('cordi_cami','cordi_cami.id_cordi','=','cordinaciones.id')
        ->select('cordi_cami.id_cami as id','cordi_cami.ppu','cordinaciones.fecha','cordinaciones.hora','cordinaciones.fecha_fin','cordinaciones.hora_fin')
        ->where('cordi_cami.id_cami', '=', $cami)
        ->where('cordinaciones.fecha','<=', $fe2)
        ->where('cordinaciones.fecha_fin','>=', $fe2)
        ->where('cordinaciones.hora','<=', $fin)
        ->where('cordinaciones.hora_fin','>=', $fin)
        ->count();
        if($reserva_final > 0){
            $reservado = 1;
        }

        $reserva_inicial_final = Cordinacion::join('cordi_cami','cordi_cami.id_cordi','=','cordinaciones.id')
        ->select('cordi_cami.id_cami as id','cordi_cami.ppu','cordinaciones.fecha','cordinaciones.hora','cordinaciones.fecha_fin','cordinaciones.hora_fin')
        ->where('cordi_cami.id_cami', '=', $cami)
        ->where('cordinaciones.fecha','>=', $fe)
        ->where('cordinaciones.fecha_fin','<=', $fe2)
        ->where('cordinaciones.hora','>=', $ini)
        ->where('cordinaciones.hora_fin','<=', $fin)
        ->count();
        if($reserva_final > 0){
            $reservado = 1;
        }
        

        return  $reservado;

    }

    public function validarTraba(Request $request)
    {
        //solo peticiones ajax
        if(!$request->ajax()) return redirect('/');
        // Trabajador
        $tra = $request->tra;
        // Horas
        $ini = $request->ini;
        $fin = $request->fin;
        // Fechas
        $fe = $request->fechasoli;
        $fe2 = $request->fechafin;
        $reservado = 0;
        
        $reserva_inicial = Cordinacion::join('cordi_tra','cordi_tra.id_cordi','=','cordinaciones.id')
        ->select('cordi_tra.id_tra as id','cordi_tra.nombre_tra','cordinaciones.fecha','cordinaciones.hora',
        'cordinaciones.fecha_fin','cordinaciones.hora_fin')

        ->where('cordi_tra.id_tra', '=', $tra)
        ->where('cordinaciones.fecha','<=', $fe)
        ->where('cordinaciones.fecha_fin','>=', $fe)
        ->where('cordinaciones.hora','<=', $ini)
        ->where('cordinaciones.hora_fin','>=', $ini)
        ->count();
        if($reserva_inicial > 0){
            $reservado = 1;
        }

        $reserva_final = Cordinacion::join('cordi_tra','cordi_tra.id_cordi','=','cordinaciones.id')
        ->select('cordi_tra.id_tra as id','cordi_tra.nombre_tra','cordinaciones.fecha','cordinaciones.hora',
        'cordinaciones.fecha_fin','cordinaciones.hora_fin')

        ->where('cordi_tra.id_tra', '=', $tra)
        ->where('cordinaciones.fecha','<=', $fe2)
        ->where('cordinaciones.fecha_fin','>=', $fe2)
        ->where('cordinaciones.hora','<=', $fin)
        ->where('cordinaciones.hora_fin','>=', $fin)
        ->count();
        if($reserva_final > 0){
            $reservado = 1;
        }

        $reserva_inicial_final = Cordinacion::join('cordi_tra','cordi_tra.id_cordi','=','cordinaciones.id')
        ->select('cordi_tra.id_tra as id','cordi_tra.nombre_tra','cordinaciones.fecha','cordinaciones.hora',
        'cordinaciones.fecha_fin','cordinaciones.hora_fin')
        
        ->where('cordi_tra.id_tra', '=', $tra)
        ->where('cordinaciones.fecha','>=', $fe)
        ->where('cordinaciones.fecha_fin','<=', $fe2)
        ->where('cordinaciones.hora','>=', $ini)
        ->where('cordinaciones.hora_fin','<=', $fin)
        ->count();
        if($reserva_final > 0){
            $reservado = 1;
        }

        return  $reservado;

    }

    public function destroy($id)
    { 
    }

    public function cambiarCamion(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            $camion = Cordi_cami::findOrFail($request->id);//ojo
            $camion->id_cami = $request->camion;
            $camion->ppu = $request->ppu;
            $camion->save();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function cambiarTrabajador(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        try{

            $trabajador = Cordi_Tra::findOrFail($request->idt);//ojo
            $trabajador->id_tra = $request->id_tra;
            $trabajador->nombre_tra = $request->nombre_tra;
            $trabajador->save();
        
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    // Index para ver las coordinaciones de Presupuesto que se llevaron a cabo  1
    public function indexListas(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $coordinaciones=Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
            // ->join('presupuestos','presupuestos.id','=','cordinaciones.pre_id')
            ->join('estados_pro','estados_pro.id','=','cordinaciones.respuesta_cordi')
            ->select('cordinaciones.id','cordinaciones.folio_cordi','cordinaciones.razon','estados_pro.nombre_es','solicitudes.idclientempre','cordinaciones.fecha','cordinaciones.hora',
            'solicitudes.direccion_soli','solicitudes.ciudad','cordinaciones.observacion_cordi','cordinaciones.pre_id','cordinaciones.asig_id','cordinaciones.fecha_fin','cordinaciones.hora_fin','cordinaciones.soli_id',
            'cordinaciones.tipo')
            // 'presupuestos.id_empre'
            ->where('estado_cordi', '=', '1')
            ->where('respuesta_cordi', '=', '7')
            ->where('solicitudes.desplaza', '=', '0')
            ->orderBy('cordinaciones.id','desc')->get();          

        return [
            'coordinaciones' => $coordinaciones
        ];
    }

    // Index para ver las coordinaciones de Asignaciones que se llevaron a cabo 0
    public function indexListas2(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

            $coordinaciones=Cordinacion::join('solicitudes','solicitudes.id','=','cordinaciones.soli_id')
            // ->join('asignaciones','asignaciones.id','=','cordinaciones.asig_id')
            ->join('estados_pro','estados_pro.id','=','cordinaciones.respuesta_cordi')
            ->select('cordinaciones.id','cordinaciones.folio_cordi','cordinaciones.razon','estados_pro.nombre_es','solicitudes.idclientempre','cordinaciones.fecha','cordinaciones.hora',
            'solicitudes.direccion_soli','solicitudes.ciudad','cordinaciones.observacion_cordi','cordinaciones.asig_id','cordinaciones.pre_id','cordinaciones.fecha_fin','cordinaciones.hora_fin',
            'cordinaciones.soli_id','cordinaciones.tipo')
            // 'asignaciones.clicontratos','asignaciones.id_empre'
            ->where('cordinaciones.estado_cordi', '=', '1')
            ->where('cordinaciones.respuesta_cordi', '=', '7')
            ->where('solicitudes.desplaza', '=', '1')
            ->orderBy('cordinaciones.id','desc')->get();          

        return [
            'coordinaciones' => $coordinaciones
        ];
    }
    
}

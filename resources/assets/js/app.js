/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import swal from 'sweetalert2';

window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
window.swal = swal;


Vue.component('spinner', require('./components/Spinner.vue'));
Vue.component('Departamento', require('./components/Departamento.vue'));
Vue.component('Rol', require('./components/Rol.vue'));
Vue.component('Servicio', require('./components/Servicio.vue'));
Vue.component('Empresa', require('./components/Empresa.vue'));
Vue.component('Camion', require('./components/Camion.vue'));
Vue.component('Formac', require('./components/Formac.vue'));
Vue.component('Personal', require('./components/Personal.vue'));
Vue.component('Gestionp', require('./components/Gestionp.vue'));
Vue.component('Finiquito', require('./components/Finiquito.vue'));
Vue.component('Clientem', require('./components/Clientem.vue'));
Vue.component('Parti', require('./components/Parti.vue'));
Vue.component('Solicitud', require('./components/Solicitud.vue'));
Vue.component('Solicitudparti', require('./components/Solicitudparti.vue'));
Vue.component('Presupuesto', require('./components/Presupuesto.vue'));
Vue.component('Coordinacion', require('./components/Coordinacion.vue'));
Vue.component('Orden', require('./components/Orden.vue'));
Vue.component('Guia', require('./components/Guia.vue'));
Vue.component('Contratos', require('./components/Contratos.vue'));
Vue.component('Asignar', require('./components/Asignar.vue'));
Vue.component('Dashboard', require('./components/Dashboard.vue'));
Vue.component('notification', require('./components/Notification.vue'));
Vue.component('Usuario', require('./components/Usuario.vue'));
Vue.component('Estadoguia', require('./components/Estadoguia.vue'));
Vue.component('Estadorden', require('./components/Estadorden.vue'));
Vue.component('Asistencia', require('./components/Asistencia.vue'));
Vue.component('Cuentas', require('./components/Cuentas.vue'));
Vue.component('Cartolas', require('./components/Cartolas.vue'));
Vue.component('Factura', require('./components/Factura.vue'));
Vue.component('Facturaguia', require('./components/Facturaguia.vue'));
Vue.component('Cheques', require('./components/Cheques.vue'));

const app = new Vue({
    el: '#app',
    data: {
        ruta: '',
        menu: 0,
        notifications: [],
    },


});
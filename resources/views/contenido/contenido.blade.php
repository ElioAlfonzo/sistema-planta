@extends('principal')
@section('contenido')

    @if(Auth::check())
    
        @if(Auth::user()->idrol == 1) <!--Administrador-->
            <template v-if="menu==0">
                <Dashboard :ruta="ruta"></Dashboard>
            </template>
        
            <template v-if="menu==1">
                <Departamento :ruta="ruta"></Departamento>
            </template>
        
            <template v-if="menu==2">
                <Servicio :ruta="ruta"></Servicio>
            </template>
        
            <template v-if="menu==3">
                <Empresa :ruta="ruta"></Empresa>
            </template>
        
            <template v-if="menu==4">
                <Camion :ruta="ruta"></Camion>
            </template>
        
            <template v-if="menu==7">
                <Formac :ruta="ruta"></Formac>
            </template>
        
            <template v-if="menu==5">
                <Personal :ruta="ruta"></Personal>
            </template>
        
            <template v-if="menu==6">
                <Gestionp :ruta="ruta"></Gestionp>
            </template>
        
            <template v-if="menu==8">
                <Finiquito :ruta="ruta"></Finiquito>
            </template>
        
            <template v-if="menu==9">
                <Clientem :ruta="ruta"></Clientem>
            </template>
        
            <template v-if="menu==10">
                <Parti :ruta="ruta"></Parti>
            </template>
        
            <template v-if="menu==11">
                <Solicitud :ruta="ruta"></Solicitud>
            </template>
        
            <template v-if="menu==12">
                <Solicitudparti :ruta="ruta"></Solicitudparti>
            </template>
            
            <template v-if="menu==13">
                <Presupuesto :ruta="ruta"></Presupuesto>
            </template>
        
            <template v-if="menu==14">
                <Coordinacion :ruta="ruta"></Coordinacion>
            </template>
        
            <template v-if="menu==15">
                <Orden :ruta="ruta"></Orden>
            </template>
        
            <template v-if="menu==16">
                <Contratos :ruta="ruta"></Contratos>
            </template>
        
            <template v-if="menu==17">
                <Guia :ruta="ruta"></Guia>
            </template>
        
            <template v-if="menu==18">
                <Asignar :ruta="ruta"></Asignar>
            </template>
        
            <template v-if="menu==19">
                <Rol :ruta="ruta"></Rol>
            </template>
        
            <template v-if="menu==20">
                <Usuario :ruta="ruta"></Usuario>
            </template>

            <template v-if="menu==21">
                <Estadoguia :ruta="ruta"></Estadoguia>
            </template>

            <template v-if="menu==22">
                <Estadorden :ruta="ruta"></Estadorden>
            </template>

            <template v-if="menu==23">
                <Asistencia :ruta="ruta"></Asistencia>
            </template>

            <template v-if="menu==24">
                <Cuentas :ruta="ruta"></Cuentas>
            </template>

            <template v-if="menu==25">
                <Cartolas :ruta="ruta"></Cartolas>
            </template>

            <template v-if="menu==26">
                <Factura :ruta="ruta"></Factura>
            </template>

            <template v-if="menu==27">
                <Facturaguia :ruta="ruta"></Facturaguia>
            </template>

            <template v-if="menu==28">
                <Cheques :ruta="ruta"></Cheques>
            </template>
        
        @elseif(Auth::user()->idrol == 2) <!-- Recursos -->
                <template v-if="menu==0">
                    <Dashboard :ruta="ruta"></Dashboard>
                </template>

                <template v-if="menu==5">
                    <Personal :ruta="ruta"></Personal>
                </template>
            
                <template v-if="menu==6">
                    <Gestionp :ruta="ruta"></Gestionp>
                </template>
            
                <template v-if="menu==8">
                    <Finiquito :ruta="ruta"></Finiquito>
                </template>

                <template v-if="menu==23">
                    <Asistencia :ruta="ruta"></Asistencia>
                </template>
            
        @elseif(Auth::user()->idrol == 3) <!--Operador-->
                <template v-if="menu==0">
                    <Dashboard :ruta="ruta"></Dashboard>
                </template>
                <template v-if="menu==9">
                    <Clientem :ruta="ruta"></Clientem>
                </template>
            
                <template v-if="menu==10">
                    <Parti :ruta="ruta"></Parti>
                </template>

    
                <template v-if="menu==11">
                    <Solicitud :ruta="ruta"></Solicitud>
                </template>
            
                <template v-if="menu==12">
                    <Solicitudparti :ruta="ruta"></Solicitudparti>
                </template>

                <template v-if="menu==13">
                    <Presupuesto :ruta="ruta"></Presupuesto>
                </template>

                <template v-if="menu==14">
                    <Coordinacion :ruta="ruta"></Coordinacion>
                </template>
            
                <template v-if="menu==15">
                    <Orden :ruta="ruta"></Orden>
                </template>
            
                <template v-if="menu==16">
                    <Contratos :ruta="ruta"></Contratos>
                </template>
            
                <template v-if="menu==17">
                    <Guia :ruta="ruta"></Guia>
                </template>
            
                <template v-if="menu==18">
                    <Asignar :ruta="ruta"></Asignar>
                </template>
        

        @elseif(Auth::user()->idrol == 4) <!--Secretaria-->
                <template v-if="menu==0">
                    <Dashboard :ruta="ruta"></Dashboard>
                </template>
                <template v-if="menu==9">
                    <Clientem :ruta="ruta"></Clientem>
                </template>
            
                <template v-if="menu==10">
                    <Parti :ruta="ruta"></Parti>
                </template>
    
                <template v-if="menu==11">
                    <Solicitud :ruta="ruta"></Solicitud>
                </template>
            
                <template v-if="menu==12">
                    <Solicitudparti :ruta="ruta"></Solicitudparti>
                </template>

                <template v-if="menu==16">
                    <Contratos :ruta="ruta"></Contratos>
                </template>
        @endif
        
    @endif



    
    
@endsection
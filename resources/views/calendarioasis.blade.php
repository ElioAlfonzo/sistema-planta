<html>
  <head>
    <title>Calendario de Asistencia</title>
    <meta content="">
    <link rel="shortcut icon" href="../../img/favi.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
    body{
      font-family: sans-serif;
    }
    .header-col{
      background: #E3E9E5;
      color:#536170;
      text-align: center;
      font-size: 20px;
      font-weight: bold;
    }
    .header-calendar{
      background: #5C8CAF;
      color:white;
    }
    .col {
      padding-right: 10px !important;
      padding-left: 10px !important;
    }
    .box-day{
      border:1px solid #E3E9E5;
      height:150px;
    }
    .box-dayoff{
      border:1px solid #E3E9E5;
      height:150px;
      background-color: #ccd1ce;
    }
    #div1 {
    overflow: scroll;
      max-height: 145px;
      max-width: 150px;
    }
    </style>

  </head>
  <body>

    <div class="container">
      <div style="height:50px"></div>
      <h1>Calendario de Asistencias</h1>
      <hr>
      
      
      <div class="row header-calendar"  >

        <div class="col" style="display: flex; justify-content: space-between; padding: 10px;">
          @if ($tipo == 0)
            <a  href="{{ asset('/Asistencia/indexAsis/') }}/<?= $data['last']; ?>" style="margin:10px;">
              <i class="fas fa-chevron-circle-left" style="font-size:30px;color:white;"></i>
            </a>

            <h2 style="font-weight:bold;margin:10px;"><?= $mespanish; ?> <small><?= $data['year']; ?></small></h2>

            <a  href="{{ asset('/Asistencia/indexAsis/') }}/<?= $data['next']; ?>" style="margin:10px;">
              <i class="fas fa-chevron-circle-right" style="font-size:30px;color:white;"></i>
            </a>
          @else
            <a  href="{{ asset('/Asistencia/indexAsis/personal/') }}/<?= $data['last'];?>/<?= $id ?>" style="margin:10px;">
              <i class="fas fa-chevron-circle-left" style="font-size:30px;color:white;"></i>
            </a>

            <h2 style="font-weight:bold;margin:10px;"><?= $mespanish; ?> <small><?= $data['year']; ?></small></h2>

            <a  href="{{ asset('/Asistencia/indexAsis/personal/') }}/<?= $data['next']; ?>/<?= $id ?>" style="margin:10px;">
              <i class="fas fa-chevron-circle-right" style="font-size:30px;color:white;"></i>
            </a>
          @endif
        </div>
        
      </div>
      

      <div class="row">
        <div class="col header-col">Lunes</div>
        <div class="col header-col">Martes</div>
        <div class="col header-col">Miercoles</div>
        <div class="col header-col">Jueves</div>
        <div class="col header-col">Viernes</div>
        <div class="col header-col">Sabado</div>
        <div class="col header-col">Domingo</div>
      </div>
      <!-- inicio de semana -->
      @foreach ($data['calendar'] as $weekdata)

        <div class="row">
          <!-- ciclo de dia por semana -->
          @foreach  ($weekdata['datos'] as $dayweek)

          @if  ($dayweek['mes']==$mes)
            <div class="col box-day">
              <div id="div1">
                {{ $dayweek['dia']  }}
                {{-- Carga  lista los Eventos --}}
                {{-- Eventos --}}
                @foreach ($dayweek['evento'] as $event)
                  @if ($event->tipo == 1)
                    <a class="badge badge-success text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Entrada">
                        {{ $event->nombre_tra. ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif

                  @if($event->tipo == 2)
                  
                    <a class="badge badge-secondary text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Salida">
                        {{ $event->nombre_tra . ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif

                  @if($event->tipo == 3)
                    <a class="badge badge-danger text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Ausente">
                        {{ $event->nombre_tra . ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif

                  @if($event->tipo == 4)
                    <a class="badge badge-primary text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Vacaciones">
                        {{ $event->nombre_tra . ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif

                  @if($event->tipo == 5)
                    <a class="badge badge-info text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Licencia Medica">
                        {{ $event->nombre_tra . ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif
                  
                  @if($event->tipo == 6)
                    <a class="badge badge-dark text-white" href="#" data-toggle="tooltip" data-placement="bottom" title="Permiso">
                        {{ $event->nombre_tra . ' ' .$event->apellido_tra. ' - '.$event->hora_asi}}
                    </a>
                  @endif

                @endforeach

              </div>
            </div>
          @else
          <div class="col box-dayoff">
          </div>
          @endif


          @endforeach
        </div>

      @endforeach

    </div> <!-- /container -->

    <!-- Footer -->
<footer class="page-footer font-small blue pt-4">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a href="" target="_blank">Elio Alfonzo</a>. All rights reserved.</span>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

  </body>
</html>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema Gestion de Flota de Camiones">
    <meta name="author" content="Elio Alfonzo">
    <meta name="keyword" content="Sistema Gestion de Flota de Camiones">
    <link rel="shortcut icon" href="img/favi.png">
    <!--Tomar el id del usuario logueado-->
    <meta name="userId" content="{{ Auth::check() ? Auth::user()->id : ''}}">
    <title>Sistema de Gestion Ecofosa</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- Graficos Chart--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js">

    {{-- Icons --}}
    <link href="css/plantilla.css" rel="stylesheet">
    <link rel="stylesheet" src="vue-multiselect/dist/vue-multiselect.min.css">

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    {{-- identificador para Vuejs --}}
    <div id="app">

        <header class="app-header navbar">
            <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand"></a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="nav navbar-nav d-md-down-none">
                <li class="nav-item px-3">
                    <a class="nav-link" href="#">Escritorio</a>
                </li>
                <li class="nav-item px-3">
                    <a class="nav-link" href="#">Configuraciones</a>
                </li>
            </ul>
            <ul class="nav navbar-nav ml-auto">
                {{-- Aqui ivan las notificaciones --}}
                <notification :notifications="notifications"></notification>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="img/avatars/6.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
                        <span class="d-md-down-none">{{Auth::user()->usuario}}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header text-center">
                            <strong>Cuenta</strong>
                        </div>
                        {{-- <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Perfil</a> --}}
                        <a class="dropdown-item" href="{{ route('logout') }}" 
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-lock"></i> Cerrar sesión
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>
            </ul>
        </header>

        <div class="app-body">
            <!--Sidebar-->
                <!--Si el usuario actual esta autenticado-->
                @if(Auth::check())
                    @if(Auth::user()->idrol == 1) <!--Administrador-->
                        @include('plantilla.sidebaradministrador')

                    @elseif(Auth::user()->idrol == 2) <!--RRHH-->
                        @include('plantilla.sidebarecursos')
                     
                    @elseif(Auth::user()->idrol == 3)  <!--Operaciones-->
                        @include('plantilla.sidebaroperaciones')
                    
                    @elseif(Auth::user()->idrol == 4)  <!--Operaciones-->
                        @include('plantilla.sidebarsecretaria')
                    @else

                    @endif
                @endif
            <!--/Sidebar-->

            <!-- Contenido Principal -->
                @yield('contenido')
            <!-- /Fin del contenido principal -->
        </div>

    </div>
    <footer class="app-footer">
        
        <span>Copyright &copy; 2020  All rights reserved. <a target="_blank" href="https://api.whatsapp.com/send?phone=+56956259823&text=Hola Elio Como estas? Sabes, Necesito Ayuda en el Sistema de Gestion Ecofosa ATT: {{Auth::user()->usuario}}">Necesito Ayuda Elio Alfonzo</a></span>
        {{-- <span class="ml-auto">Desarrollado por <a href="">EA-IT</a></span> --}}
    </footer>

    <script src="js/app.js"></script>
    <script src="js/plantilla.js"></script>
</body>

</html>
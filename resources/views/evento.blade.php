<html>
  <head>
    <title>Coordinacion N° {{$event->folio_cordi}}</title>
    <meta content="">
    <link rel="shortcut icon" href="../../img/favi.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
    body{
      font-family:  sans-serif;
    }
    .main-panel{
    background: #f3f3f9;
    }
    .header-col{
      background: #E3E9E5;
      color:#536170;
      text-align: center;
      font-size: 20px;
      font-weight: bold;
    }
    .header-calendar{
      background: #EE192D;color:white;
    }
    .box-day{
      border:1px solid #E3E9E5;
      height:150px;
    }
    .box-dayoff{
      border:1px solid #E3E9E5;
      height:150px;
      background-color: #ccd1ce;
    }
    p{
        font-size: 0.875rem;
    }
    .bg-fondo{
        background-color: #29363d !important;
        font-size: 0.9rem;
    }

    .table td{
        font-size: 0.875rem;
    }
    .h4, h4 {
        font-size: 1.13rem;
    }
    #imagen{
        width: 230px;
        height: 75px;
    }
    </style>

  </head>
  <body>
<div class="main-panel">
    <div class="container">
      <div style="height:50px"></div>
      <h1 class="text-center">Detalles de Coordinacion de OT</h1>
      <hr>
    
        <div class="col-lg-12">
        
            <div class="card px-2" style="opacity: 1;">
                <div class="card-body">

                    <div class="container-fluid d-flex justify-content-between">
                        <div class="col-lg-6 pl-0">
                            <div id="logo">
                                <div>
                                    @if ($empre->id == "1")
                                        {{-- <p>{{$event->id}}</p> --}}
                                        <img src="/../img/ecofosa.png" id="imagen">
                                        <p class="text-uppercase mt-2 mb-0 text-justify"><b>Ingeniería mantención y servicios de limpieza</b></p>
                                        <p class="text-uppercase mb-0"><b>Industrial ecofosa spa</b></p>
                                        <p class="text-uppercase mb-0"><b>Rut: 76.281.644-k</b></p>
                                        <p class="text-uppercase mb-0"><b>Giro: Evacuacion de Riles</b></p>
                                        
                                    @elseif ($empre->id == "2")
                                        <img src="/../img/induclean.jpg" id="imagen">
                                        <p class="text-uppercase mb-0 ml-2"><b>Induclean spa</b></p>
                                        <p class="text-uppercase mb-0 ml-2"><b>Rut: 76.857.367-0</b></p>
                                        <p class="text-uppercase mb-0 ml-2"><b>Giro: Transporte</b></p>
                                    @else
                                        <img src="/../img/induclean.jpg" id="imagen">
                                        <p class="text-uppercase mb-0 ml-2"><b>Induclean EIRL</b></p>
                                        <p class="text-uppercase mb-0 ml-2"><b>Rut: 76.280.893-5</b></p>
                                        <p class="text-uppercase mb-0 ml-2"><b>Giro: Transporte</b></p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <p class="text-right"><b>COORDINACION</b></p>
                            {{-- <p>{{$empre->folio_pre}}</p> --}}
                            <h3 class="text-right my-2 "><b> N° {{$event->folio_cordi}} </b></h3>
                        </div>
                    </div>
                    
                    <div class="container-fluid d-flex justify-content-between">

                        <div class="col-lg-6 pl-0">
                            <p class="mt-4 mb-2 text-uppercase"><b>{{$event->razon }}</b></p>
                            <p class="mb-1">Rut : {{ $cli->rut }}</p>
                            <p class="text-capitalize mb-1">Telefono : {{ $cli->tel_em }}</p>
                            <p class="text-capitalize">Direccion: {{ $cli->direccion_em }}</p>
                            
                        </div>

                        <div class="col-lg-6 pl-0">
                            <p class="mt-4 mb-2 text-uppercase text-right"><b>Contactos</b></p>
                            <p class="text-right mb-1">Tlf: {{ $cli->tel_contac }}</p>
                            <p class="text-right mb-1">Correo: {{ $cli->correo_contac}}</p>
                            <p class="text-right mb-1">Dir. Solicitud: {{ $soli->direccion_soli}}</p>
                            <br>
                            
                        </div>
                        
                    </div>

                    <div class="container-fluid d-flex justify-content-center">
                        <div class="col-lg-12 px-0">
                            <p class="mb-1 text-center text-uppercase"><b>Observaciones:</b></p>
                            <p class="mb-1 text-capitalize">{{$event->observacion_cordi}}</p>
                        </div>
                    </div>
                    <hr>
                    <div>
                        
                        <div class="container-fluid mt-1 w-100">
                            <h4 class="text-right mb-2">SERVICIOS</h4>
                            <div class="table-responsive w-100">
                                <table class="table">
                                    <thead>
                                        <tr class="bg-fondo text-white">
                                            <th>#</th>
                                            <th class="text-left">Servicios</th>
                                            <th>Cantidad</th>
                                            <th>Unidad</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($servi as $ser)
                                        <tr>
                                            <td>{{$ser->id}}</td>
                                            <td class="text-left">{{$ser->nomb_servi}}</td>
                                            <td class="text-left">{{$ser->cantidad}}</td>
                                            <td class="text-left">{{$ser->nombre_costo}}</td>
                                        </tr>
                                        
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="container-fluid mt-1 w-100">
                        <h4 class="text-right mb-2">TRABAJADORES</h4>
                        <div class="table-responsive w-100">
                            <table class="table">
                                <thead>
                                    <tr class="bg-fondo text-white">
                                        <th>#</th>
                                        <th class="text-left">Nombre</th>
                                        <th>Rut</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @foreach ($traba as $tra)
                                    <tr>
                                        <td>{{$tra->id}}</td>
                                        <td class="text-left">{{$tra->nombre_tra}}</td>
                                        <td class="text-left">{{$tra->rut}}</td>
                                    </tr>
                                    
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <hr>

                    <div class="container-fluid mt-1 w-100">
                        <h4 class="text-right mb-2">CAMIONES</h4>
                        <div class="table-responsive w-100">
                            <table class="table">
                                <thead>
                                    <tr class="bg-fondo text-white">
                                        <th>#</th>
                                        <th class="text-left">PPU</th>
                                        <th>Marca</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @foreach ($cami as $ca)
                                    <tr>
                                        <td>{{$ca->id}}</td>
                                        <td class="text-left">{{$ca->ppu}}</td>
                                        <td class="text-left">{{$ca->marca}}</td>
                                    </tr>
                                    
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <hr>

                        <div class="container-fluid w-100">
                            <a class="btn btn-secondary float-left mt-4 ml-2"  href="{{ asset('/Evento/index') }}">Atras</a>
                        </div>

                        {{-- <div class="container-fluid w-100">
                            <a class="btn btn-primary float-right mt-4 ml-2" href="{{ asset('/coordinacion/pdf/3') }}"> <i class="icon-printer mr-1"></i>Imprimir</a>
                        </div> --}}

                </div>
            </div>

        </div>


      <!-- inicio de semana -->


    </div> <!-- /container -->

        <!-- Footer -->
    <footer class="page-footer font-small blue pt-4">
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019 <a href="" target="_blank">Elio Alfonzo</a>. All rights reserved.</span>
    </div>
    <!-- Copyright -->
    </footer>
    <!-- Footer -->
</div>
  </body>
</html>
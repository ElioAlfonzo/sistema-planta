<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li @click="menu=0" class="nav-item">
                <a class="nav-link active" href="#"><i class="icon-speedometer"></i> Escritorio</a>
            </li>
            <li class="nav-title">
                Mantenimiento
            </li>
            
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-people"></i> RRHH</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=5" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-user"></i> Trabajadores</a>
                    </li>

                    <li @click="menu=6" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-doc"></i> Contratos</a>
                    </li>
                    
                    <li @click="menu=8" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-doc"></i> Finiquitos</a>
                    </li>
                    <li @click="menu=23" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-user"></i> Asistencias</a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li @click="menu=0" class="nav-item">
                <a class="nav-link active" href="#"><i class="icon-speedometer"></i> Escritorio</a>
            </li>
            <li class="nav-title">
                Mantenimiento
            </li>

            {{-- Clientes y Contratos --}}
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-people"></i> CLIENTES</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=10" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-user"></i> Particular</a>
                    </li>

                    <li @click="menu=9" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-home"></i> Empresas</a>
                    </li>
                    
                    <li @click="menu=16" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-folder"></i> Contratos</a>
                    </li>

                </ul>
            </li>
            {{-- Clientes y Contratos --}}

            {{-- Ordenes y Gestiones --}}
            <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-basket"></i> PROYECTOS</a>
                <ul class="nav-dropdown-items">
                    
                    <li @click="menu=11" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-basket-loaded"></i> Solicitudes Empresas</a>
                    </li>

                    <li @click="menu=12" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-notebook"></i> Solicitudes Particular</a>
                    </li>

                    <li @click="menu=13" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-wallet"></i> Cotizaciones</a>
                    </li>

                    <li @click="menu=18" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-wallet"></i> Asignaciones</a>
                    </li>

                    <li @click="menu=14" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-wallet"></i> Coordinaciones</a>
                    </li>

                    <li @click="menu=15" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-wallet"></i> Orden Trabajo</a>
                    </li>

                    <li @click="menu=17" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-wallet"></i> Guias Despacho</a>
                    </li>
                </ul>
            </li>
            {{-- Ordenes y Gestiones --}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
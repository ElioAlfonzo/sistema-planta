<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li @click="menu=0" class="nav-item">
                <a class="nav-link active" href="#"><i class="fa fa-tachometer"></i> Escritorio</a>
            </li>
            <li class="nav-title">
                Mantenimiento
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-database"></i> ADMINISTRADOR</a>
                <ul class="nav-dropdown-items">
                    <li @click="menu=1" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-book"></i> Cargos</a>
                    </li>

                    <li @click="menu=4" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-truck"></i> Camiones</a>
                    </li>

                    <li @click="menu=3" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-building"></i> Empresas</a>
                    </li>

                    <li @click="menu=7" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-book"></i> Formas de Costos</a>
                    </li>

                    <li @click="menu=2" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-book"></i> Servicios</a>
                    </li>
                   

                    {{-- <li @click="menu=5" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-bag"></i> Personal</a>
                    </li> --}}
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-users"></i> RRHH</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=23" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Asistencias</a>
                    </li>

                    <li @click="menu=6" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Contratos</a>
                    </li>

                    <li @click="menu=8" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Finiquitos</a>
                    </li>

                    <li @click="menu=5" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-users"></i> Trabajadores</a>
                    </li>
                    
                </ul>
            </li>

            {{-- Clientes y Contratos --}}
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-users"></i> CLIENTES</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=16" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Contratos</a>
                    </li>

                    <li @click="menu=9" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-building"></i> Empresas</a>
                    </li>

                    <li @click="menu=10" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-user"></i> Particular</a>
                    </li>
                   

                </ul>
            </li>
            {{-- Clientes y Contratos --}}
            
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-check-square"></i> SOLICITUDES</a>
                <ul class="nav-dropdown-items">
                    
                    <li @click="menu=11" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-building"></i> Solicitudes Empresas</a>
                    </li>

                    <li @click="menu=12" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-user"></i> Solicitudes Particular</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-table"></i> ASIGNAR-COTIZAR</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=18" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-check-square"></i> Asignaciones</a>
                    </li>
                    
                    <li @click="menu=13" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-money"></i> Cotizaciones</a>
                    </li>
                    
                </ul>
            </li>

            {{-- Ordenes y Gestiones --}}
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-briefcase"></i> PROYECTOS</a>
                <ul class="nav-dropdown-items">
                    
                    <li @click="menu=14" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-calendar"></i> Coordinaciones</a>
                    </li>

                    <li @click="menu=17" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Guias Despacho</a>
                    </li>

                    <li @click="menu=15" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Orden Trabajo</a>
                    </li>

                    
                </ul>
            </li>
            {{-- Ordenes y Gestiones --}}

            <!--Estado de Pagos-->
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-list"></i> ESTADOS DE PAGO</a>
                <ul class="nav-dropdown-items">
                    
                    <li @click="menu=21" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Estado P de Guias</a>
                    </li>

                    <li @click="menu=22" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i> Estado P de OT</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-file-text"></i> FACTURAS</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=26" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-dot-circle-o"></i>Facturas OT</a>
                    </li>

                    <li @click="menu=27" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-dot-circle-o"></i>Facturas GUIA</a>
                    </li>

                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-university"></i> BANCOS</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=24" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i>Cuentas</a>
                    </li>

                    <li @click="menu=25" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i>Cartolas</a>
                    </li>

                    <li @click="menu=28" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-file-text"></i>Cheques
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-users"></i> Acceso</a>
                <ul class="nav-dropdown-items">

                    <li @click="menu=20" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-user"></i>Usuarios</a>
                    </li>
                    <li @click="menu=19" class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-user-plus"></i> Roles</a>
                    </li>
                </ul>
            </li>

            {{-- <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-pie-chart"></i> Reportes</a>
                <ul class="nav-dropdown-items">
                    <li @click="menu=0" class="nav-item">
                        <a class="nav-link" href="i#"><i class="icon-chart"></i> Reporte Ingresos</a>
                    </li>
                    <li @click="menu=0" class="nav-item">
                        <a class="nav-link" href="#"><i class="icon-chart"></i> Reporte Ventas</a>
                    </li>
                </ul>
            </li>
            <li @click="menu=0" class="nav-item">
                <a class="nav-link" href="#"><i class="icon-book-open"></i> Ayuda <span class="badge badge-danger">PDF</span></a>
            </li>
            <li @click="menu=0" class="nav-item">
                <a class="nav-link" href="#"><i class="icon-info"></i> Acerca de...<span class="badge badge-info">IT</span></a>
            </li> --}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<!DOCTYPE html>
<html lang="en">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ASIGNACION</title>

<style>
    
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    p, label, span, table{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 9pt;
    }
    .h2{
        font-family: 'BrixSansBlack';
        font-size: 16pt;
    }
    .h3{
        /* font-family: Arial, Helvetica, sans-serif; */
        font-size: 12pt;
        display: block;
        text-align: center;
        padding: 3px;
        margin-bottom: 3px;
    }
    .titulo{
        /* font-family: Arial, Helvetica, sans-serif; */
        display: block;
        text-align: center;
        padding: 3px;
        margin-bottom: 3px;
    }
    #page_pdf{
        width: 95%;
        margin: 40px auto 10px auto;
    }

    #factura_head, #factura_cliente, #factura_detalle{
        width: 100%;
        margin-bottom: 10px;
    }
    .logo_factura{
        width: 25%;
    }
    .logo_factura img{
        width: 200px;
        height: 90px;
    }
    .info_empresa{
        width: 50%;
        text-align: center;
    }
    .info_factura{
        width: 25%;
    }
    .info_cliente{
        width: 100%;
    }
    .datos_cliente{
        width: 100%;
    }
    .datos_cliente tr td{
        width: 50%;
    }
    .datos_cliente{
        padding: 10px 10px 0 10px;
    }
    .datos_cliente label{
        width: 75px;
        display: inline-block;
    }
    .datos_cliente p{
        display: inline-block;
    }

    .textright{
        text-align: right;
    }
    .textleft{
        text-align: left;
    }
    .textcenter{
        text-align: center;
    }
    .round{
        border-radius: 10px;
        border: 1px solid #111;
        overflow: hidden;
        padding-bottom: 15px;
    }
    .round p{
        padding: 0 15px;
    }

    #factura_detalle{
        border-collapse: collapse;
    }
    #factura_detalle thead th{
        background: #0b0b0bed;
        color: #FFF;
        padding: 5px;
    }
    #detalle_productos tr:nth-child(even) {
        background: #ededed;
    }
    #detalle_totales span{
        font-family: 'BrixSansBlack';
    }
    .nota{
        font-size: 8pt;
    }
    .label_gracias{
        font-family: verdana;
        font-weight: bold;
        font-style: italic;
        text-align: center;
        margin-top: 20px;
    }
    .anulada{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
    }
    .textpadi{
        padding-left: 5px;
    }
        
</style>
<head>
</head>

<body>
<div id="page_pdf">

    <table id="factura_head">
        @foreach ($empre as $presu)
            <tr>
                <td class="logo_factura">
                    <div>
                        @if($presu->id == 1)
                        <img src="img/ecofosa.png">
                        @else
                        <img src="img/induclean.jpg">
                        @endif
                    </div>                    
                </td>
                
                <td class="info_empresa">
                    @if($presu->id == 1)
                        <div>
                            
                            <p>Ingeniería mantención y servicios de limpieza</p>
                            <p>Industrial ecofosa spa</p>
                            <p>Rut: 76.281.644-k</p>
                            <p>Teléfono: 332411547</p>
                            <p>Email: info@ecofosa.cl</p>
                        </div>
                    @elseif($presu->id == 2)
                        <div>
                            <span class="h2">Induclean Spa</span>
                            <p>Rut: 76.280.893-5</p>
                            <p>Giro: Transporte</p>
                            <p>Teléfono: 332411547</p>
                            <p>Email: info@ecofosa.cl</p>
                        </div>
                    @else
                        <div>
                            <span class="h2">Induclean EIRL</span>
                            <p>Rut: 76.857.367-0</p>
                            <p>Giro: Transporte</p>
                            <p>Teléfono: 332411547</p>
                            <p>Email: info@ecofosa.cl</p>
                        </div>
                    @endif
                </td>
                
                <td class="info_factura">
                    <div class="round textcenter">
                        <span><b>COODINACION.</b></span>
                        <p><b>N° {{$event->folio_cordi}}</b></p>
                    </div>
                </td>
            </tr>
        @endforeach
	</table>

    <table id="factura_cliente">
        @foreach ($asignacion as $presu)
            <tr>
                <td class="info_cliente">
                    <div class="round">
                        <span class="h3"><b>{{$presu->razon}}</b></span>
                        <table class="datos_cliente">
                            <tr>
                                <td><label>Rut:</label><p>{{$presu->rut}}</p></td>
                                <td><label>Email:</label><p>{{$presu->correo_contac}}</p></td>
                            </tr>
                            <tr>
                                <td><label>Teléfono:</label><p>{{$presu->tel_em}}</p></td>
                                <td><label>Ciudad Servi:</label><p>{{$presu->ciudad}}</p></td>
                            </tr>
                            <tr>
                                <td><label>Dirección:</label><p>{{$presu->direccion_em}}</p></td>
                                <td><label>Direc Servi:</label><p>{{$presu->direccion_soli}}</p></td>
                            </tr>
                        </table>
                    </div>
                </td>

            </tr>
        @endforeach
    </table>
    
    <table id="factura_cliente">
        @foreach ($asignacion as $presu)
            <tr>
                <td class="info_cliente">
                    <div class="round">
                        <span class="titulo"><b>Observaciones:</b></span>
                        <table class="datos_cliente">
                            <tr>
                                <td><p>{{$presu->observacion_asi}}</p></td>
                            </tr>
                        </table>
                    </div>
                </td>

            </tr>
        @endforeach
    </table>
    

    <table id="factura_detalle">
        <thead>
            <tr>
                <th class="textleft">Descripción</th>
                <th width="50px">Cant.</th>
                <th width="60px">U. Medida</th>
                
            </tr>
        </thead>
        <tbody id="detalle_productos">
            @foreach ($detalles as $deta)
                <tr>
                    <td class="textpadi">{{$deta->nomb_servi}}</td>
                    <td class="textcenter">{{$deta->cantidad}}</td>
                    <td class="textcenter">{{$deta->nombre_costo}}</td>
                </tr>
            @endforeach
        </tbody>
        
        {{-- <tfoot id="detalle_totales">
            @foreach ($asignacion as $presu)
                <tr>
                    <td></td>
                    <td colspan="3" class="textright"><span>Total Neto</span></td>
                    <td class="textright"><span>$ {{number_format($presu->monto_pre,2)}}</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3" class="textright"><span>IVA(%)</span></td>
                    <td class="textright"><span>$ {{number_format($presu->iva_pre,2)}}</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3" class="textright"><span>TOTAL</span></td>
                    <td class="textright">$ {{number_format($presu->total_pre,2)}}<</td>
                </tr>
            @endforeach
        </tfoot> --}}
    </table>

    <div>
		{{-- <p class="nota">Cualquiera de estos precios son ajustables, <br>pongase en contacto con nombre, teléfono y Email</p> --}}
		{{-- <h4 class="label_gracias">¡Gracias por su confiaza!</h4> --}}
	</div>
</div>

    
</body>
</html>


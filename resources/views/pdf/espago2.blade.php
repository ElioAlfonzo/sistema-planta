<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Estado de Pagos</title>
</head>
<style>
    
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    p, label, span, table{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 7pt;
    }
    .h2{
        font-family: 'BrixSansBlack';
        font-size: 16pt;
    }
    .h3{
        /* font-family: Arial, Helvetica, sans-serif; */
        font-size: 12pt;
        display: block;
        text-align: center;
        padding: 3px;
        margin-bottom: 3px;
    }
    .titulo{
        /* font-family: Arial, Helvetica, sans-serif; */
        display: block;
        text-align: center;
        padding: 3px;
        margin-bottom: 3px;
    }
    #page_pdf{
        width: 95%;
        margin: 40px auto 10px 25px;
    }

    #factura_head, #factura_cliente, #factura_detalle{
        width: 100%;
        margin-bottom: 10px;
    }
    .logo_factura{
        width: 25%;
    }
    .logo_factura img{
        width: 200px;
        height: 90px;
    }
    .info_empresa{
        width: 50%;
        text-align: center;
    }
    .info_factura{
        width: 25%;
    }
    .info_cliente{
        width: 100%;
    }
    .datos_cliente{
        width: 100%;
    }
    .datos_cliente tr td{
        width: 50%;
    }
    .datos_cliente{
        padding: 10px 10px 0 10px;
    }
    .datos_cliente label{
        width: 75px;
        display: inline-block;
    }
    .datos_cliente p{
        display: inline-block;
    }

    .textright{
        text-align: right;
    }
    .textleft{
        text-align: left;
    }
    .textcenter{
        text-align: center;
    }
    .round{
        border-radius: 10px;
        border: 1px solid #111;
        overflow: hidden;
        padding-bottom: 15px;
    }
    .round p{
        padding: 0 15px;
    }

    #factura_detalle{
        border-collapse: collapse;
    }
    #factura_detalle thead th{
        /* background: #0b0b0bed; */
        /* color: #000; */
        padding: 5px;
        border: #000 solid 1.5px;
    }
    #factura_detalle tbody td{
        /* background: #0b0b0bed; */
        /* color: #000; */
        padding: 1px; /*aqui modifique*/
        border: #000 solid 1.5px;
    }

    #factura_detalle tbody th{
        /* background: #0b0b0bed; */
        /* color: #000; */
        padding: 2px; /*aqui modifique*/
        border: #000 solid 1.5px;
    }

    #detalle_productos tr:nth-child(even) {
        background: #ededed;
    }
    #detalle_totales span{
        font-family: 'BrixSansBlack';
    }
    .nota{
        font-size: 8pt;
    }
    .label_gracias{
        font-family: verdana;
        font-weight: bold;
        font-style: italic;
        text-align: center;
        margin-top: 20px;
    }
    .anulada{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
    }
    .textpadi{
        padding-left: 5px;
    }
        
</style>


<body>
    <div id="page_pdf">
        <!--Logo y nombre de documento-->
        
        <table id="factura_head">
            @foreach ($empre as $em)
            <tr>
                <td class="logo_factura">
                    <div>
                        <img src="storage/{{$em->empre_image}}">
                    </div> 
                </td>

                <td>

                </td>
                
                <td class="info_factura">
                    <div class="round textcenter">
                        <span><b>ESTADO DE PAGO</b></span>
                        <p><b>RUT: {{$em->rut_em}}</b></p>
                        <p><b>ES-PG-{{$folio}}</b></p>
                    </div>
                </td>
            </tr>
        @endforeach
        </table>
        <!---->

        <!-- Info de Estado de Pago -->
        {{-- <table id="factura_detalle">
            <thead>
                <tr>
                    <th colspan="4" class="textcenter">ESTADO DE PAGO</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="textcenter">Codigo</th>
                    <th class="textcenter">Revision</th>
                    <th class="textcenter">Fecha</th>
                    <th class="textcenter">Pagina</th>
                </tr>
                <tr>
                    <td class="textcenter">RE-AFF-03/03</td>
                    <td class="textcenter">0</td>
                    <td class="textcenter">05-12-2013</td>
                    <td class="textcenter">1 de 1</td>
                </tr>
            </tbody>
        </table> --}}
        <!---->

        <!--Informacion de rangos de fechas del contrato -->
        <table id="factura_detalle">
            <thead>
                <tr>
                    <th colspan="4" class="textcenter">ESTADO DE PAGO (Guias)</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th class="textcenter">Mes</th>
                    <td class="textcenter">{{$mes}}</td>
                    <th class="textcenter">Cliente</th>
                    <td class="textcenter">{{$razon[0]->razon}}</td>
                </tr>
                <tr>
                    <th class="textcenter">Desde</th>
                    <td class="textcenter">{{$fechaini}}</td>
                    <th class="textcenter">Total N.</th>
                    <td class="textcenter">$ {{number_format($subtota,2)}}</td>
                </tr>
                <tr>
                    <th class="textcenter">Hasta</th>
                    <td class="textcenter">{{$fechafin}}</td>
                    <th class="textcenter">19% IVA</th>
                    <td class="textcenter">$ {{number_format($ivatota,2)}}</td>
                </tr>
                <tr>
                    <th class="textcenter">Fecha de R.</th>
                    <td class="textcenter">{{$fechao}}</td>
                    <th class="textcenter">TOTAL</th>
                    <td class="textcenter">$ {{ number_format($tota,2) }} </td>
                    
                </tr>
            </tbody>
        </table>
        <!--  -->
    
        <!-- Info de Servicios Realizado -->
        <table id="factura_detalle">
            <thead>
                <tr>
                    <th class="textcenter">Fecha</th>
                    <th class="textcenter">GUIA</th>
                    <th class="textcenter">Descripcion</th>
                    <th class="textcenter">Lugar</th>
                    <th class="textcenter">Medida</th>
                    <th class="textcenter">Cant.</th>
                    <th class="textcenter" >Patente</th>
                    <th class="textcenter" >Chofer</th>
                    <th class="textcenter" >Valor U</th>
                    <th class="textcenter" >Valor Neto</th>
                </tr>
            </thead>

            <tbody id="detalle_productos">
                @foreach ($resumen as $resu)
                    <tr>
                        <td class="textcenter">{{$resu->fecha_guia}}</td>
                        <td class="textcenter">{{$resu->folio_guia}}</td>
                        <td class="textcenter">{{$resu->nomb_servi}}</td>
                        <td class="textcenter">{{$resu->direccion_soli}}</td>
                        <td class="textcenter">{{$resu->nombre_costo}}</td>
                        <td class="textcenter">{{$resu->cantidad}}</td>
                        <td class="textcenter">{{$resu->ppu}}</td>
                        <td class="textcenter">{{$resu->nombre_tra}} {{$resu->apellido_tra}}</td>
                        <td class="textcenter"><span> $ {{number_format($resu->valor_uni,2)}}</span></td>
                        <td class="textcenter"><span>$ {{number_format($resu->valor_neto,2)}}</span></td>
                    </tr>
                @endforeach
            </tbody>
            
            <tfoot id="detalle_totales">
                {{-- @foreach ($orden as $or) --}}
                    {{-- <tr>
                        <td></td>
                        <td colspan="3" class="textright"><span>Total Neto</span></td>
                        <td class="textright"><span>$ {{number_format($or->monto_guia,2)}}</span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3" class="textright"><span>IVA(%)</span></td>
                        <td class="textright"><span>$ {{number_format($or->iva_guia,2)}}</span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3" class="textright"><span>TOTAL</span></td>
                        <td class="textright">$ {{number_format($or->total_guia,2)}}<</td>
                    </tr> --}}
                {{-- @endforeach --}}
            </tfoot>
        </table>
        <!---->

        <div>
            {{-- <p class="nota">Cualquiera de estos precios son ajustables, <br>pongase en contacto con nombre, teléfono y Email</p> --}}
            {{-- <h4 class="label_gracias">¡Gracias por su confiaza!</h4> --}}
        </div>
    </div>
</body>


</html>
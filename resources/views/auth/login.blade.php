@extends('auth.contenido')

@section('login')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card-group mb-0">

        <div class="card p-4">
        <form class="form-horizontal was-validated" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="card-body">
            <h1><img src="img/favi.png" class="mr-2"> Bienvenido</h1>
            <h6>ECOFOSA</h6>
            <p class="text-muted">Acceso al sistema</p>
            <div class="form-group mb-3{{$errors->has('usuario' ? 'is-invalid' : '')}}">
                <span class="input-group-addon"><i class="icon-user"></i></span>
                <input type="text" value="{{old('usuario')}}" name="usuario" id="usuario" class="form-control" placeholder="Usuario">

                {!!$errors->first('usuario','<span class="invalid-feedback">:message</span>')!!}

            </div>
            <div class="form-group mb-4 {{$errors->has('password' ? 'is-invalid' : '')}}">
                <span class="input-group-addon"><i class="icon-lock"></i></span>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">

                {!!$errors->first('password', '<span class="invalid-feedback">:message</span>')!!}

            </div>
            <div class="row">
                <div class="col-6">
                <button type="submit" class="btn btn-primary px-4">Acceder</button>
                </div>
            </div>
            </div>
        </form>
        </div>

        <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
            <div class="card-body text-center">
            <div>
                <h2>Sistema Gestion Ecofosa</h2>
                <p>Sistema para la gestion de solicitudes, trabajos, personal, coordiaciones, entre otras.</p>
                <a href="https://api.whatsapp.com/send?phone=+56956259823&text=Hola Elio Como estas? Sabes, Necesito Ayuda en el Sistema de Gestion Ecofosa" target="_blank" class="btn btn-primary active mt-3">¡Mas Informacion!</a>
            </div>
            </div>
            <div>
                <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright © 2020  All rights reserved.</p>
            </div>
        </div>
        </div>
    </div>
</div>

<style>

    .card{
        border: 0;
        border-radius: .3rem;
        box-shadow: 0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,0.38);
    }
    .card-header{
        border: 0;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
    }
    .card-footer{
        border: 0;
        border-radius: .3rem;
    }

    .form-control,.btn{
        border-radius: 3px;
    }
    .bg-primary {
        background: linear-gradient(to right, #244985, #2a4571) !important;
    }
</style>
@endsection
